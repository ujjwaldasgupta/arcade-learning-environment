#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cassert>
#include <limits>
#include <ale_interface.hpp>
#include <environment/ale_screen.hpp>

#include "extended_basic_features2.hpp"

using namespace std;

const int numBitsPerByte = 8;

typedef unsigned char bool_t;

float getQ(std::unordered_set<unsigned int> &f, vector<float> &w)
{
	float Q = 0;
	
	for(std::unordered_set<unsigned int>::iterator it = f.begin(); it != f.end(); it++)
	{
		assert(*it < w.size());
		Q += w[*it];
	}
	
	return Q;
}

int bestAction(std::unordered_set<unsigned int> &f, vector<vector<float> > &w) 
{
	int bestAction = rand()%w.size();
	float bestQ = getQ(f, w[bestAction]);
	
	for(int i=0; i<w.size(); i++)
	{
		float Q = getQ(f, w[i]);
		
		if(Q > bestQ)
		{
			bestQ = Q;
			bestAction = i;
		}
	}
	
	return bestAction;
}

int main(int argc, char** argv) 
{
	if(argc < 7)
	{
		std::cerr << "Usage: " << argv[0] << " rom_file display alpha gamma actionCount runs" << std::endl;
		return 1;
	}

	int display = atoi(argv[2]);
	float alpha = atof(argv[3]);
	float gamma = atof(argv[4]);
	int actionCount = atoi(argv[5]);
	int runs = atoi(argv[6]);
	ALEInterface ale(display);

	ale.loadROM(argv[1]);

	ActionVect legal_actions = ale.getMinimalActionSet();
	
	ALEScreen screen = ale.getScreen();
	int numColors = 32;
	int numColumns = 16;
	int numRows = 21;
	int proximityLimit = 5;
	ExtendedBASICFeatures2 features(numColors, numRows, numColumns, proximityLimit);
	int numFeatures = features.numFeatures();
	vector<vector<float> > w(legal_actions.size(), vector<float>(numFeatures, 0));
	alpha = 2*alpha/numFeatures;
	
	float sumReward = 0;
	for(int episode=0; episode<runs; episode++) 
	{
		float totalReward = 0;
		
		int currAction;
		currAction = rand()%legal_actions.size();
		
		ALEScreen screen = ale.getScreen();
		std::unordered_set<unsigned int> currF = features.getFeatures(screen);
		
		while (!ale.game_over()) 
		{
			float currReward = 0;
			
			for(int i=0; i<actionCount; i++)
			{
				currReward += ale.act(legal_actions[currAction]);
			}
			
			totalReward += currReward;
			int nextAction;
			
			ALEScreen screen = ale.getScreen();
			std::unordered_set<unsigned int> nextF =  features.getFeatures(screen);
			
			if(rand()%100 >= 90)
			{
				nextAction = rand()%legal_actions.size();
			}
			else
			{
				nextAction = bestAction(nextF, w);
			}
			
			float delta = currReward + gamma*getQ(nextF, w[nextAction])-getQ(currF, w[currAction]);
			
			for(auto it = currF.begin(); it != currF.end(); it++)
			{
				//cout<<"w["<<currAction<<"]["<<i<<"] += "<<alpha*currF[i]*delta<<"\n";
				assert(*it < w[currAction].size());
				w[currAction][*it] += alpha*delta;
				assert(!isnan(w[currAction][*it]));
				assert(!isinf(w[currAction][*it]));
			}
				
			currAction = nextAction;
			currF = nextF;
		}
		
		cout << "Episode " << episode << " ended with score: " << totalReward << endl;
		sumReward += totalReward;
		ale.reset_game();
	}
	
	cout << "Average score: " << (float)sumReward/runs << endl;
};


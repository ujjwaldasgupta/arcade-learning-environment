#include <iostream>
#include <vector>
#include <ale_interface.hpp>
#include <environment/ale_screen.hpp>

#include "policy_tree.hpp"

std::vector<unsigned int> getBASICFeaturesFromScreen(ALEScreen &screen)
{
	    int numColumns = 16;
        int numRows = 21;
        int numColors = 8;
		int screenWidth = screen.width();
		int screenHeight = screen.height();
        int blockWidth = screenWidth / numColumns;
        int blockHeight = screenHeight / numRows;

        int featuresPerBlock = numColors;
        std::vector<unsigned int> features(numColumns*numRows*numColors, 0);

        int blockIndex = 0;
        
        // For each pixel block
        for (int by = 0; by < numRows; by++) 
		{
            for (int bx = 0; bx < numColumns; bx++) 
			{
                vector<bool> hasColor(numColors, false);
                int xo = bx * blockWidth;
                int yo = by * blockHeight;

                // Determine which colors are present
                for (int x = xo; x < xo + blockWidth; x++)
				{
                    for (int y = yo; y < yo + blockHeight; y++)
					{
                        unsigned char pixel = screen.get(y,x);
                        hasColor[(pixel & 0xF) >> 1] = true;
                    }
				}
                // Add all colors present to our feature set
                for (int c = 0; c < numColors; c++)
				{
                    if (hasColor[c])
					{
                        features[c + blockIndex] = 1;
					}
				}

                // Increment the feature offset in the big feature vector
                blockIndex += featuresPerBlock;
            }
        }

        return features;
}

int main(int argc, char** argv)
{
	if(argc < 15)
	{
		std::cerr<<"Usage: "<<argv[0]<<" rom_file display runs timeStepsPerAction sizeHistory minActionProbability learningRate maxLeafNodes gradientAverageWindowForConvergence convergenceThreshold useGPOMDP"<<'\n';
		return 1;
	}

	int display = atoi(argv[2]);
	int runs = atoi(argv[3]);
	int timeStepsPerAction = atoi(argv[4]);
	int sizeHistory = (bool)atoi(argv[5]);
	double minActionProbability = atof(argv[6]);
	double learningRate = atof(argv[7]);
	int maxLeafNodes = atoi(argv[8]);
	int gradientAverageWindowForConvergence = atoi(argv[9]);
	double convergenceThreshold = atof(argv[10]);
	bool useGPOMDP = atoi(argv[11]) ? true : false;
	bool useTotalReward = atoi(argv[12]) ? true : false;
	bool useBaseLine = atoi(argv[13]) ? true : false;
	bool useSoftMax = atoi(argv[14]) ? true : false;
	
	ALEInterface ale(display);
	ale.loadROM(argv[1]);
	//ActionVect valid_actions = ale.getLegalActionSet();
	ActionVect valid_actions = ale.getMinimalActionSet();
	ALEScreen screen = ale.getScreen();
	
	unsigned int featureSize = getBASICFeaturesFromScreen(screen).size();
	
	/* PolicyTree(unsigned int sizeHistory, unsigned int sizeObservations, unsigned int numObservations, unsigned int numActions, double minActionProbability, double learningRate, unsigned int maxLeafNodes, unsigned int gradientAverageWindowForConvergence, double gradientRatioThreshold, double convergenceThreshold, bool useGPOMDP, bool useTotalReward, useBaseLine) */
	PolicyTree policyTree(sizeHistory, featureSize, 2, valid_actions.size(), minActionProbability, learningRate, maxLeafNodes, gradientAverageWindowForConvergence, convergenceThreshold, useGPOMDP, useTotalReward, useBaseLine, useSoftMax);
	double sumReward = 0;
	
	for(int episode=0; episode < runs; episode++)
	{
		double totalReward = 0;

		while(!ale.game_over())
		{	
			screen = ale.getScreen();
			std::vector<unsigned int> observations = getBASICFeaturesFromScreen(screen);
			unsigned int action = policyTree.storeObservationsAndGetAction(observations);
			double reward = 0;
			for(unsigned int i=0; i < timeStepsPerAction; i++)
			{
				reward += ale.act(valid_actions[action]);
			}
			policyTree.processAction(action, reward);
			totalReward += reward;
		}

		policyTree.processEpisode();
		std::cout<<"Episode "<<episode<<" ended with score: "<< totalReward <<'\n';
		sumReward += totalReward;
		ale.reset_game();
	}

	std::cout << "Average score: " << sumReward/runs << '\n';
}


#include <cmath>
#include <cassert>

#include "basic_features.hpp"

using namespace std;

class ExtendedBASICFeatures2 : public BASICFeatures
{
    public:
    unsigned int proximityLimit;
    
    ExtendedBASICFeatures2(unsigned int numColors, unsigned int numRows, unsigned int numColumns, unsigned int proximityLimit) : BASICFeatures(numColors, numRows, numColumns), proximityLimit(proximityLimit)
    {
    }
    
    unsigned int numFeatures()
    {
        return (2*proximityLimit-1)*(2*proximityLimit-1)*((numColors*(numColors+1))/2);
    }
    unordered_set<unsigned int> getFeatures(ALEScreen& screen)
    {
        unordered_set<unsigned int> originalFeatures = BASICFeatures::getFeatures(screen);
        unordered_set<unsigned int> features;
        //cout<<"Original Feature Size: "<<originalFeatures.size()<<"\n";
        //cout<<"Features: ";
        
        for(auto it1 = originalFeatures.begin(); it1 != originalFeatures.end(); it1++)
        {
            auto it2 = it1;
            for(; it2 != originalFeatures.end(); it2++)
            {
                bool flag = false;
                unsigned int color1 = (*it1)%numColors;
                unsigned int color2 = (*it2)%numColors;
                unsigned int r1 = ((*it1)%(numColors*numRows))/numColors;
                unsigned int r2 = ((*it2)%(numColors*numRows))/numColors;
                unsigned int c1 = (*it1)/(numColors*numRows);
                unsigned int c2 = (*it2)/(numColors*numRows);
                if(color1 > color2)
                {
                    flag = true;
                }
                unsigned int minColor, maxColor;
                int i, j;
                if(flag)
                {
                    minColor = color2;
                    maxColor = color1;
                    i = r1-r2;
                    j = c1-c2;
                }
                else
                {
                    minColor = color1;
                    maxColor = color2;
                    i = r2-r1;
                    j = c2-c1;
                }
                unsigned int posIndex = (i + proximityLimit - 1)*(2*proximityLimit - 1) + j + proximityLimit - 1;
                if(i < (int)proximityLimit && (-i) < (int)proximityLimit && j < (int)proximityLimit && (-j) < (int)proximityLimit)
                {
                    unsigned int index = (posIndex*numColors*(numColors+1))/2 + minColor*numColors - ((minColor-1)*minColor)/2 + maxColor - minColor;
                    if(features.count(index) == 0)
                    {
                        assert(index < numFeatures());
                        //cout<<"("<<posIndex<<","<<minColor<<","<<maxColor<<") ";
                        features.insert(index);
                    }
                }
            }
        }
        //cout<<"\n";
        
        return features;
    }
};
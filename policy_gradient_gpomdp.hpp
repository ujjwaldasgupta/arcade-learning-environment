#include <vector>
#include <deque>
#include <cassert>
#include <cmath>
#include <utility>
#include <cstdlib>
#include <set>
#include <unordered_set>
#include "sample.hpp"

class PolicyGradientGPOMDP
{
	public:
	unsigned int sizeHistory;
	unsigned int sizeObservations;
	unsigned int numActions;
	std::vector<std::pair<unsigned int, unsigned int> > observationsHistory;
	std::vector<std::vector<std::vector<double> > > weights;
	std::vector<std::vector<std::vector<double> > > weightsGradient;
	std::vector<double> biasWeights;
	std::vector<double> biasWeightsGradient;
	
	std::vector<std::vector<std::vector<double> > > Z;
	std::vector<double> biasZ;
	std::set<std::pair<unsigned int, unsigned int> > activeZIndices;
	
	std::vector<double> cachedActionProbabilities;
	bool isActionProbabilitiesCacheClean;
	
	double learningRate;
	double temperature;
	double baseLine;
	unsigned long long int baseLineCount;
	
	PolicyGradientGPOMDP(unsigned int sizeHistory, unsigned int sizeObservations, unsigned int numActions, double learningRate, double temperature) : sizeHistory(sizeHistory), sizeObservations(sizeObservations), numActions(numActions), weights(numActions, std::vector<std::vector<double> >(sizeHistory, std::vector<double>(sizeObservations, 0))), weightsGradient(numActions, std::vector<std::vector<double> >(sizeHistory, std::vector<double>(sizeObservations, 0))), Z(numActions, std::vector<std::vector<double> >(sizeHistory, std::vector<double>(sizeObservations, 0))), learningRate(learningRate), temperature(temperature), isActionProbabilitiesCacheClean(false), baseLine(0.0), baseLineCount(0), biasWeights(numActions, 0.0),  biasWeightsGradient(numActions, 0.0), biasZ(numActions, 0.0)
	{
	}
	
	unsigned int storeObservationsAndGetAction(std::unordered_set<unsigned int> observations)
	{
		storeObservations(observations);
		return getAction();
	}
	
	void updateStoredGradient(unsigned int action, double reward)
	{
		std::vector<double> actionProbabilities = getActionProbabilities();
		
		for(unsigned int i=0 ; i < numActions; i++)
		{
			assert(actionProbabilities[i] >= 0);
			assert(actionProbabilities[i] <= 1);
			
			for(int j=0; j < observationsHistory.size(); j++)
			{
				assert(observationsHistory[j].first < sizeHistory);
				assert(observationsHistory[j].second < sizeObservations);
				
				activeZIndices.insert(observationsHistory[j]);
				
				if(i == action)
				{
					Z[i][observationsHistory[j].first][observationsHistory[j].second] += (1 - actionProbabilities[i]) / temperature;
				}
				else
				{
					Z[i][observationsHistory[j].first][observationsHistory[j].second] += (-1) * actionProbabilities[i] / temperature;
				}
			}
			
			/*for(unsigned int j=0 ; j < sizeHistory; j++)
			{
				for(unsigned int k=0; k < sizeObservations; k++)
				{
					weightsGradient[i][j][k] += (reward - baseLine) * Z[i][j][k];
				}
			}*/
			
			for(std::set<std::pair<unsigned int, unsigned int> >::iterator it = activeZIndices.begin(); it != activeZIndices.end(); it++)
			{
				weightsGradient[i][it->first][it->second] += (reward - baseLine) * Z[i][it->first][it->second];
			}
					
			if(i == action)
			{
				biasZ[i] += (1 - actionProbabilities[i]) / temperature;
				biasWeightsGradient[i] += (reward - baseLine) * biasZ[i];
			}
			else
			{
				biasZ[i] += (-1) * actionProbabilities[i] / temperature;
				biasWeightsGradient[i] += (reward - baseLine) * biasZ[i];
			}
		}
		
		baseLineCount++;
		baseLine += ((double)1/baseLineCount)*(reward - baseLine);
	}
	
	std::vector<double> getActionProbabilities()
	{
		if(isActionProbabilitiesCacheClean)
		{
			return cachedActionProbabilities;
		}
		
		std::vector<double> actionProbabilities(numActions);
		double sumActionProbabilities = 0;
		
		for(unsigned int i=0 ; i < numActions; i++)
		{
			double sum = 0;
			
			for(int j=0; j < observationsHistory.size(); j++)
			{
				assert(observationsHistory[j].first < sizeHistory);
				assert(observationsHistory[j].second < sizeObservations);
				
				sum += weights[i][observationsHistory[j].first][observationsHistory[j].second];
			}
			
			sum += biasWeights[i];
			
			actionProbabilities[i] = exp(sum/temperature);
			sumActionProbabilities += actionProbabilities[i];
		}
		
		if(!std::isinf(sumActionProbabilities))
		{
			for(unsigned int i=0 ; i < numActions; i++)
			{
				actionProbabilities[i] /= sumActionProbabilities;
			}
		}
		else
		{
			std::set<unsigned int> nanIndexes;
			
			for(unsigned int i=0 ; i < numActions; i++)
			{
				if(std::isinf(actionProbabilities[i]))
				{
					nanIndexes.insert(i);
				}
			}
			
			assert(nanIndexes.size() > 0);
			
			for(unsigned int i=0 ; i < numActions; i++)
			{
				if(nanIndexes.find(i) == nanIndexes.end())
				{
					actionProbabilities[i] = 0;
				}
				else
				{
					actionProbabilities[i] = (double)1/nanIndexes.size();
				}
			}
		}
		
		cachedActionProbabilities = actionProbabilities;
		isActionProbabilitiesCacheClean = true;
		
		return actionProbabilities;
	}
	
	unsigned int getAction()
	{
		std::vector<double> actionProbabilities = getActionProbabilities();
		unsigned int action = sample(actionProbabilities);
		return action;
	}
	
	void storeObservations(std::unordered_set<unsigned int> observations)
	{
		std::vector<std::pair<unsigned int, unsigned int> > newObservationsHistory;
		newObservationsHistory.reserve(observations.size() + observationsHistory.size());
		
		for(int i=0; i < observationsHistory.size(); i++)
		{
			if(observationsHistory[i].first + 1< sizeHistory)
			{
				assert(observationsHistory[i].second < sizeObservations);
				newObservationsHistory.push_back(std::make_pair(observationsHistory[i].first+1, observationsHistory[i].second));
			}
		}

		for(auto it = observations.begin(); it != observations.end(); it++)
		{
			assert(*it < sizeObservations);
			newObservationsHistory.push_back(std::make_pair(0, *it));
		}
		
		observationsHistory = newObservationsHistory;
		isActionProbabilitiesCacheClean = false;
	}
	
	void processEpisode()
	{
		for(unsigned int i=0 ; i < numActions; i++)
		{
			for(unsigned int j=0 ; j < sizeHistory; j++)
			{
				for(unsigned int k=0; k < sizeObservations; k++)
				{
					weights[i][j][k] += learningRate * weightsGradient[i][j][k];
					weightsGradient[i][j][k] = 0;
					Z[i][j][k] = 0;
				}
			}
			
			biasWeights[i] += learningRate * biasWeightsGradient[i];
			biasWeightsGradient[i] = 0;
			biasZ[i] = 0;
		}
		
		activeZIndices.clear();
		isActionProbabilitiesCacheClean = false;
		observationsHistory.clear();
	}
};
	
 

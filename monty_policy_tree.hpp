#include <vector>
#include <cstdio>
#include <cassert>
#include <cmath>
#include <deque>
#include <set>
#include <numeric>
#include <functional>
#include <algorithm>

#include "sample.hpp"

const double tolerance = 1e-6;

class PolicyTree
{
public:
	class PolicyTreeNode
	{
	public:
		PolicyTree& policyTree;
		bool isFringe;
		std::vector<PolicyTreeNode* > children;
		std::vector<std::vector<std::vector<PolicyTreeNode* > > > observationsFringeChildren;
		std::vector<std::vector<PolicyTreeNode*> > actionFringeChildren;
		std::vector<double> Q;
		std::vector<unsigned int> Z;
		std::vector<double> currSumQ;
		std::vector<unsigned int> currZ;
		unsigned int policyAction;
		int hIndex;
		int oIndex;
		PolicyTreeNode* parent;
		
		PolicyTreeNode(PolicyTree& policyTree, bool isFringe) : policyTree(policyTree), isFringe(isFringe), Q(policyTree.numActions, 0), Z(policyTree.numActions, 0), currSumQ(policyTree.numActions, 0), currZ(policyTree.numActions, 0), hIndex(-1), oIndex(-1)
		{
			policyAction = rand() % policyTree.numActions;
			
			if(!isFringe)
			{
				createFringe();
			}
		}
		
		~PolicyTreeNode()
		{
			if(!isFringe)
			{
				deleteFringe();
			}
		}
		
		void createFringe()
		{
			assert(!isFringe);
			
			observationsFringeChildren = std::vector<std::vector<std::vector<PolicyTreeNode* > > >(policyTree.sizeHistory, std::vector<std::vector<PolicyTreeNode* > >(policyTree.sizeObservations, std::vector<PolicyTreeNode*>(policyTree.numObservations) ) );
			
			for(unsigned int i=0; i < policyTree.sizeHistory; i++)
			{
				for(unsigned int j=0; j < policyTree.sizeObservations; j++)
				{
					for(unsigned int k=0; k < policyTree.numObservations; k++)
					{
						observationsFringeChildren[i][j][k] = new PolicyTreeNode(policyTree, true);
						observationsFringeChildren[i][j][k]->parent = this;
						observationsFringeChildren[i][j][k]->Q = Q;
					}
				}
			}
			
			actionFringeChildren = std::vector<std::vector<PolicyTreeNode*> >(policyTree.sizeHistory, std::vector<PolicyTreeNode*>(policyTree.numActions));
			
			for(unsigned int i=0; i < policyTree.sizeHistory; i++)
			{
				for(unsigned int j=0; j < policyTree.numActions; j++)
				{
					actionFringeChildren[i][j] = new PolicyTreeNode(policyTree, true);
					actionFringeChildren[i][j]->parent = this;
					actionFringeChildren[i][j]->Q = Q;
				}
			}
		}
		
		void deleteFringe()
		{
			assert(!isFringe);
			
			for(unsigned int i=0; i < policyTree.sizeHistory; i++)
			{
				for(unsigned int j=0; j < policyTree.sizeObservations; j++)
				{
					for(unsigned int k=0; k < policyTree.numObservations; k++)
					{
						if(observationsFringeChildren[i][j][k])
						{
							delete observationsFringeChildren[i][j][k];
						}
					}
				}
			}
			
			for(unsigned int i=0; i < policyTree.sizeHistory; i++)
			{
				for(unsigned int j=0; j < policyTree.numActions; j++)
				{
					delete actionFringeChildren[i][j];
				}
			}
			
			observationsFringeChildren.clear();
			actionFringeChildren.clear();
		}
		
		unsigned int bestAction()
		{
			unsigned int action = rand() % Q.size();
			double bestQ = Q[action];
			
			for(int i=0; i < Q.size(); i++)
			{
				if(Q[i] > bestQ)
				{
					bestQ = Q[i];
					action = i;
				}
			}
			
			return action;
		}
		
		unsigned int getAction()
		{
			unsigned int action;
			
			if(rand()%100 >= policyTree.epsilon * 100)
			{
				action = policyAction;
			}
			else
			{
				action = rand() % policyTree.numActions;
			}
			
			return action;
		}
		
		void updateCurrSumQ(double reward)
		{
            for(unsigned int i=0; i < policyTree.numActions; i++)
            {
                currSumQ[i] += currZ[i] * reward;
            }
			
			if(!isFringe)
			{
				for(unsigned int i=0; i < policyTree.sizeHistory; i++)
				{
					for(unsigned int j=0; j < policyTree.sizeObservations; j++)
					{
						for(unsigned int k=0; k < policyTree.numObservations; k++)
						{
							if(observationsFringeChildren[i][j][k])
							{
								observationsFringeChildren[i][j][k]->updateCurrSumQ(reward);
							}
						}
					}
				}
                
				for(unsigned int i=0; i < policyTree.sizeHistory; i++)
				{
					for(unsigned int j=0; j < policyTree.numActions; j++)
					{
						actionFringeChildren[i][j]->updateCurrSumQ(reward);
					}
				}
			}
		}
        
		void updateZ(unsigned int action)
		{
			currZ[action]++;
            
			if(!isFringe)
			{
				for(unsigned int i=0; i < policyTree.sizeHistory; i++)
				{
					assert(i < policyTree.observationsHistory.size());
					for(unsigned int j=0; j < policyTree.sizeObservations; j++)
					{
						unsigned int observation = policyTree.observationsHistory.at(i)[j];
						assert(observationsFringeChildren[i][j][observation]);
						observationsFringeChildren[i][j][observation]->updateZ(action);
					}
					
					assert(i < policyTree.actionHistory.size());
					unsigned int action = policyTree.actionHistory.at(i);
					assert(actionFringeChildren[i][action]);
					actionFringeChildren[i][action]->updateZ(action);
				}
			}
		}
		
		void updateQ()
		{
			for(unsigned int i=0; i < policyTree.numActions; i++)
			{
				Z[i] += currZ[i];
				if(Z[i] > 0)
				{
					Q[i] += (currSumQ[i] - currZ[i]*Q[i])/Z[i];
				}
			}
			
			currZ = std::vector<unsigned int>(policyTree.numActions, 0);
			currSumQ = std::vector<double>(policyTree.numActions, 0);
			
			if(!isFringe)
			{
				for(unsigned int i=0; i < policyTree.sizeHistory; i++)
				{
					for(unsigned int j=0; j < policyTree.sizeObservations; j++)
					{
						for(unsigned int k=0; k < policyTree.numObservations; k++)
						{
							assert(observationsFringeChildren[i][j][k]);
							observationsFringeChildren[i][j][k]->updateQ();
						}
					}
				}
				
				for(unsigned int i=0; i < policyTree.sizeHistory; i++)
				{
					for(unsigned int j=0; j < policyTree.numActions; j++)
					{
						assert(actionFringeChildren[i][j]);
						actionFringeChildren[i][j]->updateQ();
					}
				}
			}
		}
		
		bool improvePolicy()
		{
			unsigned int oldPolicyAction = policyAction;
			policyAction = bestAction();
			
			if(oldPolicyAction != policyAction)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		void clearQ()
		{
			Z = std::vector<unsigned int>(policyTree.numActions, 0);
			Q = std::vector<double>(policyTree.numActions, 0);
			
			if(!isFringe)
			{
				for(unsigned int i=0; i < policyTree.sizeHistory; i++)
				{
					for(unsigned int j=0; j < policyTree.sizeObservations; j++)
					{
						for(unsigned int k=0; k < policyTree.numObservations; k++)
						{
							assert(observationsFringeChildren[i][j][k]);
							observationsFringeChildren[i][j][k]->clearQ();
						}
					}
				}
				
				for(unsigned int i=0; i < policyTree.sizeHistory; i++)
				{
					for(unsigned int j=0; j < policyTree.numActions; j++)
					{
						assert(actionFringeChildren[i][j]);
						actionFringeChildren[i][j]->clearQ();
					}
				}
			}
		}
		
		bool checkForDivergingNodes()
		{
			assert(!isFringe);
			int bestHIndex = -1;
			int bestOIndex = -1;
			double maxGradient = 0;
			unsigned int leafAction = bestAction();
			
			if((policyTree.numLeafNodes > policyTree.maxLeafNodes - policyTree.numObservations + 1) && (policyTree.numLeafNodes > policyTree.maxLeafNodes - policyTree.numActions + 1))
			{
				return false;
			}
			
			for(unsigned int i=0; i < policyTree.sizeHistory; i++)
			{
				if(policyTree.numLeafNodes <= policyTree.maxLeafNodes - policyTree.numObservations + 1)
				{
					for(unsigned int j=0; j < policyTree.sizeObservations; j++)
					{
						for(unsigned int k=0; k < policyTree.numObservations; k++)
						{
							unsigned int fringeAction = observationsFringeChildren[i][j][k]->bestAction();
							double sumFringeQ = 0;
							unsigned int sumFringeZ = 0;
							
							for(unsigned int l=0; l < policyTree.numActions; l++)
							{
								sumFringeQ += observationsFringeChildren[i][j][k]->Q[l];
								sumFringeZ += observationsFringeChildren[i][j][k]->Z[l];
							}
							
							double gradient = sumFringeZ * (((double)(policyTree.numActions)/(policyTree.numActions-1))*observationsFringeChildren[i][j][k]->Q[fringeAction] - (sumFringeQ)/(policyTree.numActions-1) );
							
							if(leafAction != fringeAction && gradient > maxGradient)
							{
								bestHIndex = i;
								bestOIndex = j;
								maxGradient = gradient;
							}
						}
					}
				}
				
				if(policyTree.numLeafNodes > policyTree.maxLeafNodes - policyTree.numActions + 1)
				{
					for(unsigned int j=0; j < policyTree.numActions; j++)
					{
						unsigned int fringeAction = actionFringeChildren[i][j]->bestAction();
						double sumFringeQ = 0;
						unsigned int sumFringeZ = 0;
						
						for(unsigned int l=0; l < policyTree.numActions; l++)
						{
							sumFringeQ += actionFringeChildren[i][j]->Q[l];
							sumFringeZ += actionFringeChildren[i][j]->Z[l];
						}
						
						double gradient = sumFringeZ * (((double)(policyTree.numActions)/(policyTree.numActions-1))*actionFringeChildren[i][j]->Q[fringeAction] - (sumFringeQ)/(policyTree.numActions-1) );
						
						if(leafAction != fringeAction && gradient > maxGradient)
						{
							bestHIndex = i;
							bestOIndex = j;
							maxGradient = gradient;
						}
					}
				}
			}
			
			if(bestHIndex != -1)
			{
				if(bestOIndex != -1)
				{
					promoteFringeNode(bestHIndex, bestOIndex);
				}
				else
				{
					promoteActionFringeNode(bestHIndex);
				}
				
				return true;
			}
			
			return false;
		}
		
		void promoteFringeNode(int hIndex, int oIndex)
		{
			this->hIndex = hIndex;
			this->oIndex = oIndex;
			children = std::vector<PolicyTreeNode* >(policyTree.numObservations, NULL);
			
			for(unsigned int i=0; i < policyTree.numObservations; i++)
			{
				assert(observationsFringeChildren[hIndex][oIndex][i]);
				observationsFringeChildren[hIndex][oIndex][i]->isFringe = false;
				observationsFringeChildren[hIndex][oIndex][i]->parent = this;
				observationsFringeChildren[hIndex][oIndex][i]->createFringe();
                observationsFringeChildren[hIndex][oIndex][i]->policyAction = observationsFringeChildren[hIndex][oIndex][i]->bestAction();
				children[i] = observationsFringeChildren[hIndex][oIndex][i];
				observationsFringeChildren[hIndex][oIndex][i] = NULL;
			}
			
			deleteFringe();
			policyTree.numLeafNodes += policyTree.numObservations - 1;
		}
		
		void promoteActionFringeNode(int hIndex)
		{
			this->hIndex = hIndex;
			this->oIndex = -1;
			children = std::vector<PolicyTreeNode* >(policyTree.numActions, NULL);
			
			for(unsigned int i=0; i < policyTree.numActions; i++)
			{
				assert(actionFringeChildren[hIndex][i]);
				actionFringeChildren[hIndex][i]->isFringe = false;
				actionFringeChildren[hIndex][i]->parent = this;
				actionFringeChildren[hIndex][i]->createFringe();
                actionFringeChildren[hIndex][i]->policyAction = actionFringeChildren[hIndex][i]->bestAction();
				children[i] = actionFringeChildren[hIndex][i];
				actionFringeChildren[hIndex][i] = NULL;
			}
			
			deleteFringe();
			policyTree.numLeafNodes += policyTree.numActions - 1;
		}
	};
	
	PolicyTreeNode *root;
	unsigned int sizeHistory;
	unsigned int sizeObservations;
	unsigned int numObservations;
	unsigned int numActions;
	double epsilon;
	unsigned int maxLeafNodes;
	unsigned int numLeafNodes;
	unsigned int numPolicyEvaluationSteps;
	unsigned int numStepsSinceLastPolicyChange;
    unsigned int numIterationsSinceDivergenceCheck;
    unsigned int maxIterationsBeforeDivergenceCheck;
	std::deque<std::pair<PolicyTreeNode*, unsigned int> > currentStateActionPairs;
	std::deque<std::deque<std::pair<PolicyTreeNode*, unsigned int> > > stateActionPairsHistory;
	std::deque<std::vector<unsigned int> > observationsHistory;
	std::deque<unsigned int> actionHistory;
	std::deque<double> rewardHistory;
	
	PolicyTree(unsigned int sizeHistory, unsigned int sizeObservations, unsigned int numObservations, unsigned int numActions, double epsilon, unsigned int maxLeafNodes, unsigned int numPolicyEvaluationSteps, unsigned int maxIterationsBeforeDivergenceCheck) : sizeHistory(sizeHistory), sizeObservations(sizeObservations), numObservations(numObservations), numActions(numActions), numLeafNodes(1), epsilon(epsilon), maxLeafNodes(maxLeafNodes), numPolicyEvaluationSteps(numPolicyEvaluationSteps), numStepsSinceLastPolicyChange(0), maxIterationsBeforeDivergenceCheck(maxIterationsBeforeDivergenceCheck), numIterationsSinceDivergenceCheck(0)
	{
		root = new PolicyTreeNode(*this, false);
		assert(maxLeafNodes > 0);
        resetHistory();
	}
	
	~PolicyTree()
	{
		freePolicyTreeNode(root);
	}
	
	void freePolicyTreeNode(PolicyTreeNode *node)
	{
		for(unsigned int i=0; i < node->children.size(); i++)
		{
			freePolicyTreeNode(node->children[i]);
		}
		
		delete node;
	}
	
	PolicyTreeNode* getCurrentNode()
	{
		return getCurrentNode(root);
	}
	
	PolicyTreeNode* getCurrentNode(PolicyTreeNode* node)
	{
		if(node->children.empty())
		{
			return node;
		}
		else
		{
			assert(node->hIndex >= 0);
			
			if(node->oIndex == -1)
			{
				if((unsigned int)node->hIndex < actionHistory.size())
				{
					unsigned int action = actionHistory.at(node->hIndex);
					assert(action < node->children.size());
					return getCurrentNode(node->children[action]);
				}
				else
				{
					return getCurrentNode(node->children[0]);
				}
			}
			else
			{
				if((unsigned int)node->hIndex < observationsHistory.size())
				{
					std::vector<unsigned int> observations = observationsHistory.at(node->hIndex);
					assert(node->oIndex >= 0);
					assert((unsigned int)node->oIndex < observations.size());
					unsigned int observation = observations[node->oIndex];
					assert(observation < node->children.size());
					return getCurrentNode(node->children[observation]);
				}
				else
				{
					return getCurrentNode(node->children[0]);
				}
			}
		}
	}
	
	void storeObservations(std::vector<unsigned int> observations)
	{
		observationsHistory.push_front(observations);
		
		if(observationsHistory.size() > sizeHistory)
		{
			observationsHistory.pop_back();
		}
	}
	
	void storeAction(unsigned int action)
	{
		actionHistory.push_front(action);
		
		if(actionHistory.size() > sizeHistory)
		{
			actionHistory.pop_back();
		}
	}
	
	unsigned int storeObservationsAndGetAction(std::vector<unsigned int> observations)
	{
		storeObservations(observations);
		unsigned int action;
		action = getAction();
		storeAction(action);
		currentStateActionPairs.clear();
		return action;
	}
	
	unsigned int getAction()
	{
		PolicyTreeNode *currentNode = getCurrentNode();
		assert(currentNode);
		assert(!currentNode->isFringe);
		return currentNode->getAction();
	}
	
	void processActionAndReward(unsigned int action, double reward)
	{
        updateZ(action);
		updateCurrSumQ(reward);
	}

    void updateZ(unsigned int action)
	{
		updateZ(root, action);
	}
	
	void updateZ(PolicyTreeNode *node, unsigned int action)
	{
		if(node->children.size() == 0)
		{
			node->updateZ(action);
		}
		else
		{
			for(unsigned int i=0; i < node->children.size(); i++)
			{
				updateZ(node->children[i], action);
			}
		}
	}
    
	void updateCurrSumQ(double reward)
	{
		updateCurrSumQ(root, reward);
	}
	
	void updateCurrSumQ(PolicyTreeNode *node, double reward)
	{
		if(node->children.size() == 0)
		{
			node->updateCurrSumQ(reward);
		}
		else
		{
			for(unsigned int i=0; i < node->children.size(); i++)
			{
				updateCurrSumQ(node->children[i], reward);
			}
		}
	}
	
	void updateQ()
	{
		updateQ(root);
	}
	
	void updateQ(PolicyTreeNode *node)
	{
		if(node->children.size() == 0)
		{
			node->updateQ();
		}
		else
		{
			for(unsigned int i=0; i < node->children.size(); i++)
			{
				updateQ(node->children[i]);
			}
		}
	}

	void clearQ()
	{
		clearQ(root);
	}
	
	void clearQ(PolicyTreeNode *node)
	{
		if(node->children.size() == 0)
		{
			node->clearQ();
		}
		else
		{
			for(unsigned int i=0; i < node->children.size(); i++)
			{
				clearQ(node->children[i]);
			}
		}
	}
    
	bool improvePolicy()
	{
		return improvePolicy(root);
	}
	
	bool improvePolicy(PolicyTreeNode *node)
	{
		bool ret = false;
		
		if(node->children.size() == 0)
		{
			if(node->improvePolicy())
			{
				ret = true;
			}
		}
		else
		{
			for(unsigned int i=0; i < node->children.size(); i++)
			{
				if(improvePolicy(node->children[i]))
				{
					ret = true;
				}
			}
		}
		
		return ret;
	}
	
	void processEpisode()
	{
		updateQ();
		numStepsSinceLastPolicyChange++;
		
		if(numStepsSinceLastPolicyChange == numPolicyEvaluationSteps)
		{
            numIterationsSinceDivergenceCheck++;
			bool policyChanged = improvePolicy();

            if(!policyChanged || numIterationsSinceDivergenceCheck > maxIterationsBeforeDivergenceCheck)
            {
				checkForDivergingNodes();
                numIterationsSinceDivergenceCheck = 0;
			}
            else
            {
                clearQ();
            }
            
            numStepsSinceLastPolicyChange = 0;
		}
		
		resetHistory();
	}
	
	bool checkForDivergingNodes()
	{
		return checkForDivergingNodes(root);
	}
	
	bool checkForDivergingNodes(PolicyTreeNode *node)
	{
		bool ret = false;
		
		if(node->children.size() == 0)
		{
			if(node->checkForDivergingNodes())
			{
				ret = true;
			}
		}
		else
		{
			for(unsigned int i=0; i < node->children.size(); i++)
			{
				if(checkForDivergingNodes(node->children[i]))
				{
					ret = true;
				}
			}
		}
		
		return ret;
	}
	
	void resetHistory()
	{
		observationsHistory.clear();
		actionHistory.clear();
		
		for(int i=0; i < sizeHistory; i++)
		{
			storeObservations(std::vector<unsigned int>(sizeObservations, 0));
			storeAction(0);
		}
	}
};

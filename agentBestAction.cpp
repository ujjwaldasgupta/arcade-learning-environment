#include <iostream>
#include <ale_interface.hpp>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <cassert>

using namespace std;


int main(int argc, char** argv) 
{
	if(argc < 6) 
	{
		std::cerr << "Usage: " << argv[0] << " rom_file display action epsilon runs" << std::endl;
		return 1;
	}

	// Initialize parameters
	int display = atoi(argv[2]);
	int action = atoi(argv[3]);
	float epsilon = atof(argv[4]);
	int runs = atoi(argv[5]);
	
	assert(epsilon >= 0);
	assert(epsilon <= 1);
	
	int epsilonPercentage = epsilon * 100;

	ALEInterface ale(display);
	
	// Load the ROM file
	ale.loadROM(argv[1]);

	// Get the vector of legal actions
	ActionVect legal_actions = ale.getMinimalActionSet();
	assert(action < legal_actions.size());
	
	// Initialize expected reward for each action
	vector<float> Q(legal_actions.size(),0);

	float sumReward = 0;
	// Play 10 episodes
	for(int episode=0; episode<runs; episode++) 
	{
		float totalReward = 0;
		while (!ale.game_over()) 
		{
			int a = 0;
			
			if(rand()%100 >= epsilonPercentage)
			{
				// Choose a greedy action
				a = action;
			}
			else
			{
				// Choose a random action
				a = rand()%legal_actions.size();
			}
			
			// Apply the action and get the resulting reward
			float reward = ale.act(legal_actions[a]);
			totalReward += reward;
		}
		cout << "Episode " << episode << " ended with score: " << totalReward << endl;
		sumReward += totalReward;
		ale.reset_game();
	}
	
	cout << "Average score: " << (float)sumReward/runs << endl;
};


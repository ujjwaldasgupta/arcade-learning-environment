#ifndef __SAMPLE_INCLUDED__
#define __SAMPLE_INCLUDED__

#include <vector>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <iostream>

using namespace std;

int sample(vector<double>& probabilities)
{
    const double tolerence = 1e-6;
    
    double randNumber = (double)rand()/RAND_MAX;
    double sum = 0;
    int ans = -1;
    for(unsigned int i=0; i < probabilities.size(); i++)
    {
        sum += probabilities[i];
        if(sum > randNumber && ans == -1)
        {
            ans = i;
        }
    }
    assert(abs(sum-1.0) < tolerence);
    assert(ans != -1);
    return ans;
}

#endif
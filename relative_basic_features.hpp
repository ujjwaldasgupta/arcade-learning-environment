#include "features.hpp"

class RelativeBASICFeatures : public Features
{
	public:
	
	/* There will be numRows-1 rows above and below the agent, and similiarly numColumns-1 columns left and right of the agent. One additional row and column is located where the agent is. 
	 * Effectively, the total number of rows is 2*numRows-1, while the total number of columns is 2*numColumns-1.
	 */
	
	unsigned int numColors;
	unsigned int numRows;
	unsigned int numColumns;
	unsigned int colorsAssigned;
	std::vector<int> colorMap;
	unsigned int agentPixel;
	unsigned int minX;
	unsigned int maxX;
	unsigned int minY;
	unsigned int maxY;
	
	RelativeBASICFeatures(unsigned int numColors, unsigned int numRows, unsigned int numColumns, unsigned char agentPixel, unsigned int minX, unsigned int maxX, unsigned int minY, unsigned int maxY) : numColors(numColors), numRows(numRows), numColumns(numColumns), colorsAssigned(0), agentPixel(agentPixel), minX(minX), maxX(maxX), minY(minY), maxY(maxY), colorMap(128, -1) 
	{
		assert(numColors > 0);
		assert(numRows > 0);
		assert(numColumns > 0);
		assert(agentPixel >= 0);
		assert(minX >= 0);
		assert(maxX >= minX);
		assert(minY >= 0);
		assert(maxY >= minY);
	}
	
	std::unordered_set<unsigned int> getFeatures(ALEScreen& screen)
	{
		int screenWidth = screen.width();
		int screenHeight = screen.height();
		int blockWidth = screenWidth / numColumns;
		int blockHeight = screenHeight / numRows;
		int featuresPerBlock = numColors;
		std::unordered_set<unsigned int> features;
		int agentX = -1;
		int agentY = -1;
		/* Search for agent in the game. We assume that it is the bottom-right most pixel, which matches agentPixel and lies between the specified constraints. */
		for(int y = maxY; y >= (int)minY; y--)
		{
			for(int x = maxX; x >= (int)minX; x--)
			{
				assert(x < screenWidth);
				assert(y < screenHeight);
				unsigned char pixel = screen.get(y,x);
				if(pixel == agentPixel)
				{
					agentX = x;
					agentY = y;
					goto done;
				}
			}
		}
		done: if(agentX == -1) /* Cannot find agent. Let's return null features, because the game state is one which apparently does not contain the agent. */
		{
			return features;
		}
		int agentBlockX = agentX / blockWidth;
		int agentBlockY = agentY / blockHeight;
		/* For each pixel block */
		for (int bx = 0; bx < numColumns; bx++) 
		{
			for (int by = 0; by < numRows; by++) 
			{
				vector<bool> hasColor(numColors, false);
				int xo = bx * blockWidth;
				int yo = by * blockHeight;
				/* Determine which colors are present */
				for (int x = xo; x < xo + blockWidth; x++)
				{
					for (int y = yo; y < yo + blockHeight; y++)
					{
						unsigned char pixel = screen.get(y,x);
						if(colorMap[pixel >> 1] == -1)
						{
							if(colorsAssigned < numColors)
							{
								colorMap[pixel >> 1] = colorsAssigned;
								hasColor[colorsAssigned] = true;
								colorsAssigned++;
							}
						}
						else hasColor[colorMap[pixel >> 1]] = true;
					}
				}
				int relativeBlockX = bx - agentBlockX + numColumns - 1;
				int relativeBlockY = by - agentBlockY + numRows - 1;
				assert(relativeBlockX >= 0);
				assert(relativeBlockY >= 0);
				int blockIndex = (relativeBlockX*(2*numRows-1) + relativeBlockY)*numColors;
				/* Add all colors present to our feature set */
				for (int c = 0; c < numColors; c++)
				{
					if (hasColor[c])
					{
						assert(c + blockIndex >= 0);
						features.insert(c + blockIndex);
					}
				}
			}
		}
		return features;
	}
};
#include <iostream>
#include <vector>
#include <unordered_set>
#include <ale_interface.hpp>
#include <environment/ale_screen.hpp>
#include "policy_tree_linear.hpp"
#include "basic_features.hpp"
#include "extended_basic_features.hpp"

using namespace std;

int main(int argc, char** argv)
{
        if(argc < 17)
        {
                cerr<<"Usage: "<<argv[0]<<" rom_file display runs timeStepsPerAction featureType learningRate minActionProbability maxLeafNodes gradientAverageWindow optimizationSteps useBaseLine useMultiSplit cosineThreshold onlyUseFringeBias useDotProductScore discountFactor parametersNormConstraint"<<'\n';
                return 1;
        }

        int display = atoi(argv[2]);
        int runs = atoi(argv[3]);
        int timeStepsPerAction = atoi(argv[4]);
        int featureType = atoi(argv[5]);
        double learningRate = atof(argv[6]);
        double minActionProbability = atof(argv[7]);
        int maxLeafNodes = atoi(argv[8]);
        int gradientAverageWindow = atoi(argv[9]);
        int optimizationSteps = atoi(argv[10]);
        bool useBaseLine = atoi(argv[11]) ? true : false;
        bool useMultiSplit = atoi(argv[12]) ? true : false;
        double cosineThreshold = atof(argv[13]);
        bool onlyUseFringeBias = atoi(argv[14]) ? true : false;
        bool useDotProductScore = atoi(argv[15]) ? true : false;
        double discountFactor = atof(argv[16]);
        const int numColumns = 16;
        const int numRows = 21;
        const int numColors = 8;
        Features *features;
        int featureSize;
        
        if(featureType == 1)
        {
                features = new ExtendedBASICFeatures(numColors, numRows, numColumns);
                featureSize = features->numFeatures();
        }
        else
        {
                features = new BASICFeatures(numColors, numRows, numColumns);
                featureSize = features->numFeatures();
        }

        ALEInterface ale(display);
        ale.loadROM(argv[1]);
        ActionVect valid_actions = ale.getMinimalActionSet();
        ALEScreen screen = ale.getScreen();

        PolicyTreeLinear policyTreeLinear(featureSize, valid_actions.size(), learningRate, minActionProbability, maxLeafNodes, gradientAverageWindow, optimizationSteps, useBaseLine, useMultiSplit, cosineThreshold, onlyUseFringeBias, useDotProductScore, discountFactor);
        
        double sumReward = 0;
        
        for(int episode=0; episode < runs; episode++)
        {
                double totalReward = 0;
                int episodeLength = 0;

                while(!ale.game_over())
                {
                        screen = ale.getScreen();
                        unordered_set<int> observations = features->getFeatures(screen);
                        int action = policyTreeLinear.storeObservationsAndGetAction(observations);
                        double reward = 0;
                        for(int i=0; i < timeStepsPerAction; i++)
                        {
                                reward += ale.act(valid_actions[action]);
                        }
                        policyTreeLinear.processAction(action, reward);
                        totalReward += reward;
                        episodeLength++;
                }
                
                cout<<"Episode "<<episode<<" ended with score: "<< totalReward <<'\n';
                policyTreeLinear.processEpisode();
                sumReward += totalReward;
                ale.reset_game();
        }

        delete features;
        cout << "Average score: " << sumReward/runs << '\n';
}
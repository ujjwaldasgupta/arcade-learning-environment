#include <iostream>
#include <vector>
#include <ale_interface.hpp>

#include "sample.hpp"

int main(int argc, char** argv)
{
	if(argc < 7)
	{
		std::cerr<<"Usage: "<<argv[0]<<" rom_file display runs timeStepsPerAction action probability"<<'\n';
		return 1;
	}

	int display = atoi(argv[2]);
	int runs = atoi(argv[3]);
	int timeStepsPerAction = atoi(argv[4]);
	int action = atoi(argv[5]);
	double probability = atof(argv[6]);
	assert(probability > 0 && probability <= 1);
	ALEInterface ale(display);
	ale.loadROM(argv[1]);
	//ActionVect valid_actions = ale.getLegalActionSet();
	ActionVect valid_actions = ale.getMinimalActionSet();
	vector<double> actionProbabilities(valid_actions.size());
	
	for(int i=0; i < actionProbabilities.size(); i++)
	{
		if(i == action)
		{
			actionProbabilities[i] = probability;
		}
		else
		{
			actionProbabilities[i] = (1-probability)/(actionProbabilities.size()-1);
		}
	}
	
	double sumReward = 0;
	
	for(int episode=0; episode < runs; episode++)
	{
		double totalReward = 0;

		while(!ale.game_over())
		{
			unsigned int action = sample(actionProbabilities);
			double reward = 0;
			for(unsigned int i=0; i < timeStepsPerAction; i++)
			{
				reward += ale.act(valid_actions[action]);
			}
			totalReward += reward;
		}

		std::cout<<"Episode "<<episode<<" ended with score: "<< totalReward <<'\n';
		sumReward += totalReward;
		ale.reset_game();
	}

	std::cout << "Average score: " << sumReward/runs << '\n';
}


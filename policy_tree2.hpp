#include <vector>
#include <cstdio>
#include <cassert>
#include <cmath>
#include <deque>
#include <set>
#include <numeric>
#include <functional>
#include <algorithm>
#include <iostream>
#include "sample.hpp"

const double tolerance = 1e-6;

class PolicyTree
{
public:
	class PolicyTreeNode
	{
	public:
		PolicyTree& policyTree;
		bool isFringe;
		std::vector<PolicyTreeNode* > children;
		std::vector<std::vector<std::vector<PolicyTreeNode* > > > observationsFringeChildren;
		std::vector<std::vector<PolicyTreeNode*> > actionFringeChildren;
		std::vector<double> actionProbabilities;
		std::vector<double> actionParameters;
		std::vector<double> Z;
		std::vector<double> actionProbabilitiesGradient;
		std::vector<double> sumActionProbabilitiesGradient;
		std::deque<std::vector<double> > actionProbabilitiesChangeHistory;
		std::vector<double> sumActionProbabilitiesChange;
		
		int hIndex;
		int oIndex;
		PolicyTreeNode* parent;
		
		PolicyTreeNode(PolicyTree& policyTree, bool isFringe) : policyTree(policyTree), isFringe(isFringe), Z(policyTree.numActions, 0), actionParameters(policyTree.numActions, 0), actionProbabilitiesGradient(policyTree.numActions, 0), hIndex(-1), oIndex(-1), parent(NULL), sumActionProbabilitiesGradient(policyTree.numActions, 0), sumActionProbabilitiesChange(policyTree.numActions, 0)
		{
			if(!isFringe)
			{
				createFringe();
				actionProbabilities = calculateActionProbabilitiesFromParameters(actionParameters);
			}
		}
		
		~PolicyTreeNode()
		{
			if(!isFringe)
			{
				deleteFringe();
			}
		}
		
		void createFringe()
		{
			assert(!isFringe);
			
			observationsFringeChildren = std::vector<std::vector<std::vector<PolicyTreeNode* > > >(policyTree.sizeHistory, std::vector<std::vector<PolicyTreeNode* > >(policyTree.sizeObservations, std::vector<PolicyTreeNode*>(policyTree.numObservations) ) );
			
			for(unsigned int i=0; i < policyTree.sizeHistory; i++)
			{
				for(unsigned int j=0; j < policyTree.sizeObservations; j++)
				{
					for(unsigned int k=0; k < policyTree.numObservations; k++)
					{
						observationsFringeChildren[i][j][k] = new PolicyTreeNode(policyTree, true);
						observationsFringeChildren[i][j][k]->parent = this;
					}
				}
			}
			
			actionFringeChildren = std::vector<std::vector<PolicyTreeNode*> >(policyTree.sizeHistory, std::vector<PolicyTreeNode*>(policyTree.numActions));
			
			for(unsigned int i=0; i < policyTree.sizeHistory; i++)
			{
				for(unsigned int j=0; j < policyTree.numActions; j++)
				{
					actionFringeChildren[i][j] = new PolicyTreeNode(policyTree, true);
					actionFringeChildren[i][j]->parent = this;
				}
			}
		}
		
		void deleteFringe()
		{
			assert(!isFringe);
			
			for(unsigned int i=0; i < policyTree.sizeHistory; i++)
			{
				for(unsigned int j=0; j < policyTree.sizeObservations; j++)
				{
					for(unsigned int k=0; k < policyTree.numObservations; k++)
					{
						if(observationsFringeChildren[i][j][k])
						{
							delete observationsFringeChildren[i][j][k];
						}
					}
				}
			}
			
			for(unsigned int i=0; i < policyTree.sizeHistory; i++)
			{
				for(unsigned int j=0; j < policyTree.numActions; j++)
				{
					delete actionFringeChildren[i][j];
				}
			}
			
			observationsFringeChildren.clear();
			actionFringeChildren.clear();
		}
		
		void updateZ(unsigned int nextAction)
		{
			if(isFringe)
			{
				assert(parent);
				if(policyTree.useSoftMax)
				{
					for(int i=0; i < policyTree.numActions; i++)
					{
						if(i == nextAction)
						{
							Z[i] += 1 - parent->actionProbabilities[i];
						}
						else
						{
							Z[i] += -1 * parent->actionProbabilities[i];
						}
					}
				}
				else
				{
					Z[nextAction] += 1.0/parent->actionProbabilities[nextAction];
				}
			}
			else
			{
				if(policyTree.useSoftMax)
				{
					for(int i=0; i < policyTree.numActions; i++)
					{
						if(i == nextAction)
						{
							Z[i] += 1 - actionProbabilities[i];
						}
						else
						{
							Z[i] += -1 * actionProbabilities[i];
						}
					}
				}
				else
				{
					Z[nextAction] += 1.0/actionProbabilities[nextAction];
				}

				for(unsigned int i=0; i < policyTree.sizeHistory; i++)
				{					
					assert(i < policyTree.observationsHistory.size());
					for(unsigned int j=0; j < policyTree.sizeObservations; j++)
					{
						unsigned int observation = policyTree.observationsHistory.at(i)[j];
						assert(observationsFringeChildren[i][j][observation]);
						observationsFringeChildren[i][j][observation]->updateZ(nextAction);
					}
					
					assert(i < policyTree.actionHistory.size());
					unsigned int action = policyTree.actionHistory.at(i);
					assert(actionFringeChildren[i][action]);
					actionFringeChildren[i][action]->updateZ(nextAction);
				}
			}
		}
		
		std::vector<double> calculateActionProbabilitiesFromParameters(std::vector<double> actionParameters)
		{
			std::vector<double> actionProbabilities(policyTree.numActions, 0);
			assert(actionParameters.size() == policyTree.numActions);
			double sum = 0;
			
			for(int i=0; i < policyTree.numActions; i++)
			{
				actionProbabilities[i] = exp(actionParameters[i]/policyTree.temperature);
				sum += actionProbabilities[i];
			}
			
			for(int i=0; i < policyTree.numActions; i++)
			{
				actionProbabilities[i] /= sum;
			}
			
			return actionProbabilities;
		}

		void updateGPOMDPGradient(double reward)
		{
			if(policyTree.useTotalReward)
			{
				std::vector<double> delta(policyTree.numActions);
				std::transform(Z.begin(), Z.end(), delta.begin(), std::bind2nd(std::multiplies<double>(), reward));
				std::transform(actionProbabilitiesGradient.begin(), actionProbabilitiesGradient.end(), delta.begin(), actionProbabilitiesGradient.begin(), std::plus<double>());
			}
			else
			{
				std::vector<double> delta(policyTree.numActions);
				std::transform(Z.begin(), Z.end(), delta.begin(), std::bind2nd(std::multiplies<double>(), (double)reward));
				std::transform(delta.begin(), delta.end(), actionProbabilitiesGradient.begin(), delta.begin(), std::minus<double>());
				std::transform(delta.begin(), delta.end(), delta.begin(), std::bind2nd(std::divides<double>(), (double)policyTree.episodeLength));
				std::transform(actionProbabilitiesGradient.begin(), actionProbabilitiesGradient.end(), delta.begin(), actionProbabilitiesGradient.begin(), std::plus<double>());
			}
			
			if(!isFringe)
			{   
				for(unsigned int i=0; i < policyTree.sizeHistory; i++)
				{					
					for(unsigned int j=0; j < policyTree.sizeObservations; j++)
					{
						for(unsigned int k=0; k < policyTree.numObservations; k++)
						{
							observationsFringeChildren[i][j][k]->updateGPOMDPGradient(reward);
						}
					}

					for(unsigned int j=0 ; j < policyTree.numActions; j++)
					{
						actionFringeChildren[i][j]->updateGPOMDPGradient(reward);
					}
				}
			}
		}
		
		void updateREINFORCEGradient(double totalReward)
		{
			if(policyTree.useTotalReward)
			{
				std::transform(Z.begin(), Z.end(), actionProbabilitiesGradient.begin(), std::bind2nd(std::multiplies<double>(), totalReward));
			}
			else
			{
				std::transform(Z.begin(), Z.end(), actionProbabilitiesGradient.begin(), std::bind2nd(std::multiplies<double>(), totalReward/policyTree.episodeLength));
			}
			
			if(!isFringe)
			{   
				for(unsigned int i=0; i < policyTree.sizeHistory; i++)
				{					
					for(unsigned int j=0; j < policyTree.sizeObservations; j++)
					{
						for(unsigned int k=0; k < policyTree.numObservations; k++)
						{
							observationsFringeChildren[i][j][k]->updateREINFORCEGradient(totalReward);
						}
					}

					for(unsigned int j=0 ; j < policyTree.numActions; j++)
					{
						actionFringeChildren[i][j]->updateREINFORCEGradient(totalReward);
					}
				}
			}
		}
		
		std::vector<double> calculateActionProbabilitiesChange(std::vector<double> actionProbabilitiesGradient)
		{
			assert(!isFringe);
			
			if(policyTree.useSoftMax)
			{
				std::vector<double> newActionParameters(policyTree.numActions, 0);
				std::vector<double> delta(policyTree.numActions, 0);
				std::transform(actionProbabilitiesGradient.begin(), actionProbabilitiesGradient.end(), delta.begin(), std::bind2nd(std::multiplies<double>(), policyTree.learningRate));
				std::transform(actionParameters.begin(), actionParameters.end(), delta.begin(), newActionParameters.begin(), std::plus<double>());
				
				std::vector<double> newActionProbabilities = calculateActionProbabilitiesFromParameters(newActionParameters);
				std::transform(newActionProbabilities.begin(), newActionProbabilities.end(), actionProbabilities.begin(), delta.begin(), std::minus<double>());
				
				return delta;
			}
			else
			{
				double sum = std::accumulate(actionProbabilitiesGradient.begin(), actionProbabilitiesGradient.end(), 0.0, std::plus<double>());
				
				for(unsigned int i=0; i < policyTree.numActions; i++)
				{
					actionProbabilitiesGradient[i] -= sum/policyTree.numActions;
				}
				
				std::vector<double> actionProbabilitiesChange(policyTree.numActions);
				
				for(unsigned int i=0; i < policyTree.numActions; i++)
				{
					actionProbabilitiesChange[i] = policyTree.learningRate * actionProbabilitiesGradient[i];
				}
				
				bool valid;
				std::set<unsigned int> dontTouch;
				
				do
				{
					valid = true;
					sum = 0;

					for(unsigned int i=0; i < policyTree.numActions; i++)
					{
						if((dontTouch.find(i) == dontTouch.end()) && (actionProbabilities[i] + actionProbabilitiesChange[i] < policyTree.minActionProbability))
						{
							dontTouch.insert(i);
							sum += actionProbabilities[i] + actionProbabilitiesChange[i] - policyTree.minActionProbability;
							actionProbabilitiesChange[i] = policyTree.minActionProbability - actionProbabilities[i];
						}
					}
					
					for(unsigned int i=0; i < policyTree.numActions; i++)
					{
						if(dontTouch.find(i) == dontTouch.end())
						{
							actionProbabilitiesChange[i] += sum/(policyTree.numActions - dontTouch.size());
							if(actionProbabilitiesChange[i] + actionProbabilities[i] < policyTree.minActionProbability)
							{
								valid = false;
							}
						}
					}
				}
				
				while(!valid);
				
				sum = std::accumulate(actionProbabilitiesChange.begin(), actionProbabilitiesChange.end(), 0.0, std::plus<double>());
				assert(std::abs(sum) < tolerance);
				
				return actionProbabilitiesChange;
			}
		}
		
		bool checkForDivergingNodes()
		{
			int bestHIndex = -1;
			int bestOIndex = -1;
			
			double minProjection = 0;
			
			std::vector<double> averageActionProbabilitiesGradient(policyTree.numActions);
			std::transform(sumActionProbabilitiesGradient.begin(), sumActionProbabilitiesGradient.end(), averageActionProbabilitiesGradient.begin(), std::bind2nd(std::divides<double>(), (double)policyTree.countSteps));
			std::vector<double> averageActionProbabilitiesChange = calculateActionProbabilitiesChange(averageActionProbabilitiesGradient);
			
			for(unsigned int i=0; i < policyTree.sizeHistory; i++)
			{
				for(unsigned int j=0; j < policyTree.sizeObservations; j++)
				{
					for(unsigned int k=0; k < policyTree.numObservations; k++)
					{
						std::vector<double> averageFringeActionProbabilitiesGradient(policyTree.numActions);
						std::transform(observationsFringeChildren[i][j][k]->sumActionProbabilitiesGradient.begin(), observationsFringeChildren[i][j][k]->sumActionProbabilitiesGradient.end(), averageFringeActionProbabilitiesGradient.begin(), std::bind2nd(std::divides<double>(), (double)policyTree.countSteps));
						std::vector<double> averageFringeActionProbabilitiesChange = calculateActionProbabilitiesChange(averageFringeActionProbabilitiesGradient);
						double cos = cosine(averageActionProbabilitiesChange, averageFringeActionProbabilitiesChange);
						double fringeNorm = l2Norm(averageFringeActionProbabilitiesChange);
                        
						if(cos*fringeNorm < minProjection)
						{
							minProjection = cos*fringeNorm;
							bestHIndex = i;
							bestOIndex = j;
						}
					}
				}
				
				for(unsigned int j=0; j < policyTree.numActions; j++)
				{
					std::vector<double> averageFringeActionProbabilitiesGradient(policyTree.numActions);
					std::transform(actionFringeChildren[i][j]->sumActionProbabilitiesGradient.begin(), actionFringeChildren[i][j]->sumActionProbabilitiesGradient.end(), averageFringeActionProbabilitiesGradient.begin(), std::bind2nd(std::divides<double>(), (double)policyTree.countSteps));
					std::vector<double> averageFringeActionProbabilitiesChange = calculateActionProbabilitiesChange(averageFringeActionProbabilitiesGradient);
					double cos = cosine(averageActionProbabilitiesChange, averageFringeActionProbabilitiesChange);
					double fringeNorm = l2Norm(averageFringeActionProbabilitiesChange);
                    
					if(cos*fringeNorm < minProjection)
					{
						minProjection = cos*fringeNorm;
						bestHIndex = i;
						bestOIndex = j;
					}
				}
			}
			
			if(bestHIndex != -1)
			{
				std::cout<<"Adding split on: "<<bestHIndex<<", "<<bestOIndex<<"\n";

				if(bestOIndex != -1)
				{
					promoteFringeNode(bestHIndex, bestOIndex);
				}
				else
				{
					promoteActionFringeNode(bestHIndex);
				}

				std::cout<<"Number of leaf nodes: "<<policyTree.numLeafNodes<<"\n";
				return true;
			}
			
			return false;
		}
		
		void addActionProbabilitiesChangeToHistory(std::vector<double> actionProbabilitiesChange)
		{
			actionProbabilitiesChangeHistory.push_front(actionProbabilitiesChange);
            std::transform(sumActionProbabilitiesChange.begin(), sumActionProbabilitiesChange.end(), actionProbabilitiesChange.begin(), sumActionProbabilitiesChange.begin(), std::plus<double>());
			if(actionProbabilitiesChangeHistory.size() > policyTree.gradientAverageWindowForConvergence)
			{
				std::transform(sumActionProbabilitiesChange.begin(), sumActionProbabilitiesChange.end(), actionProbabilitiesChangeHistory.back().begin(), sumActionProbabilitiesChange.begin(), std::minus<double>());
				actionProbabilitiesChangeHistory.pop_back();
			}
		}
		
		void performGradientStep()
		{
			assert(!isFringe);
			
			std::vector<double> actionProbabilitiesChange = calculateActionProbabilitiesChange(actionProbabilitiesGradient);	
			
			if(policyTree.useSoftMax)
			{
				std::vector<double> delta(policyTree.numActions, 0);
				std::transform(actionProbabilitiesGradient.begin(), actionProbabilitiesGradient.end(), delta.begin(), std::bind2nd(std::multiplies<double>(), policyTree.learningRate));
				std::transform(actionParameters.begin(), actionParameters.end(), delta.begin(), actionParameters.begin(), std::plus<double>());
				actionProbabilities = calculateActionProbabilitiesFromParameters(actionParameters);
			}
			else
			{
				std::transform(actionProbabilities.begin(), actionProbabilities.end(), actionProbabilitiesChange.begin(), actionProbabilities.begin(), std::plus<double>());
			}
			
			actionProbabilitiesGradient = std::vector<double>(policyTree.numActions, 0);
			Z = std::vector<double>(policyTree.numActions, 0);
			
			double sum = std::accumulate(actionProbabilities.begin(), actionProbabilities.end(), 0.0, std::plus<double>());
			assert(abs(sum - 1.0) < tolerance);
		}
		
		void storeGradient()
		{
			std::transform(sumActionProbabilitiesGradient.begin(), sumActionProbabilitiesGradient.end(), actionProbabilitiesGradient.begin(), sumActionProbabilitiesGradient.begin(), std::plus<double>());
			actionProbabilitiesGradient = std::vector<double>(policyTree.numActions, 0);
			Z = std::vector<double>(policyTree.numActions, 0);
			
			if(!isFringe)
			{
				for(unsigned int i=0; i < policyTree.sizeHistory; i++)
				{
					for(unsigned int j=0; j < policyTree.sizeObservations; j++)
					{
						for(unsigned int k=0; k < policyTree.numObservations; k++)
						{
							observationsFringeChildren[i][j][k]->storeGradient();
						}
					}
					
					for(unsigned int j=0 ; j < policyTree.numActions; j++)
					{
						actionFringeChildren[i][j]->storeGradient();
					}
				}
			}
		}
		
		void resetGradients() /* This should be called only after a "converged" tree is ready to be optimized again (i.e. a node has been added or removed, or the tree has been found to be divergent). */
		{
			actionProbabilitiesGradient = std::vector<double>(policyTree.numActions, 0);
			sumActionProbabilitiesGradient = std::vector<double>(policyTree.numActions, 0);
			policyTree.countSteps = 0;
			actionProbabilitiesChangeHistory.clear();
			sumActionProbabilitiesChange = std::vector<double>(policyTree.numActions, 0);
			
			if(!isFringe)
			{
				for(unsigned int i=0; i < policyTree.sizeHistory; i++)
				{
					for(unsigned int j=0; j < policyTree.sizeObservations; j++)
					{
						for(unsigned int k=0; k < policyTree.numObservations; k++)
						{
							observationsFringeChildren[i][j][k]->resetGradients();
						}
					}
					
					for(unsigned int j=0 ; j < policyTree.numActions; j++)
					{
						actionFringeChildren[i][j]->resetGradients();
					}
				}
			}
		}
		
		void promoteFringeNode(int hIndex, int oIndex)
		{
			this->hIndex = hIndex;
			this->oIndex = oIndex;
			children = std::vector<PolicyTreeNode* >(policyTree.numObservations, NULL);
			
			for(unsigned int i=0; i < policyTree.numObservations; i++)
			{
				assert(observationsFringeChildren[hIndex][oIndex][i]);
				observationsFringeChildren[hIndex][oIndex][i]->isFringe = false;
				observationsFringeChildren[hIndex][oIndex][i]->parent = this;
				observationsFringeChildren[hIndex][oIndex][i]->createFringe();
				children[i] = observationsFringeChildren[hIndex][oIndex][i];
				observationsFringeChildren[hIndex][oIndex][i]->actionProbabilities = actionProbabilities;
				observationsFringeChildren[hIndex][oIndex][i]->actionParameters = actionParameters;
				observationsFringeChildren[hIndex][oIndex][i] = NULL;
			}
			
			deleteFringe();
			policyTree.numLeafNodes += policyTree.numObservations - 1;
		}
		
		void promoteActionFringeNode(int hIndex)
		{
			this->hIndex = hIndex;
			this->oIndex = -1;
			children = std::vector<PolicyTreeNode* >(policyTree.numActions, NULL);
			
			for(unsigned int i=0; i < policyTree.numActions; i++)
			{
				assert(actionFringeChildren[hIndex][i]);
				actionFringeChildren[hIndex][i]->isFringe = false;
				actionFringeChildren[hIndex][i]->parent = this;
				actionFringeChildren[hIndex][i]->createFringe();
				children[i] = actionFringeChildren[hIndex][i];
				actionFringeChildren[hIndex][i]->actionProbabilities = actionProbabilities;
				actionFringeChildren[hIndex][i]->actionParameters = actionParameters;
				actionFringeChildren[hIndex][i] = NULL;
			}
			
			deleteFringe();
			policyTree.numLeafNodes += policyTree.numActions - 1;
		}
		
		double infinityNorm(std::vector<double>& v)
		{
			double max = *std::max_element(v.begin(), v.end());
			double min = *std::min_element(v.begin(), v.end());
			return (std::abs(max) > std::abs(min)) ? std::abs(max) : std::abs(min);
		}
		
        double l2Norm(std::vector<double> v)
        {
            std::transform(v.begin(), v.end(), v.begin(), v.begin(), std::multiplies<double>());
            double ans = std::accumulate(v.begin(), v.end(), 0.0, std::plus<double>());
            return std::sqrt(ans);
        }
        
		double dotProduct(std::vector<double> v1, std::vector<double> v2)
		{
			assert(v1.size() == v2.size());
			std::vector<double> temp(v1.size());
			std::transform(v1.begin(), v1.end(), v2.begin(), temp.begin(), std::multiplies<double>());
			double ans = std::accumulate(temp.begin(), temp.end(), 0.0, std::plus<double>());
			
			return ans;
		}
		
		double cosine(std::vector<double> v1, std::vector<double> v2)
		{
			assert(v1.size() == v2.size());
			double v1DotV2 = dotProduct(v1, v2);
			double v1DotV1 = dotProduct(v1, v1);
			double v2DotV2 = dotProduct(v1, v1);
			
			return v1DotV2 / (sqrt(v1DotV1) * sqrt(v2DotV2));
		}
	};
	
	PolicyTreeNode *root;
	unsigned int sizeHistory;
	unsigned int sizeObservations;
	unsigned int numObservations;
	unsigned int numActions;
	unsigned int numLeafNodes;
	double minActionProbability;
	double learningRate;
	unsigned int maxLeafNodes;
	unsigned int gradientAverageWindowForConvergence;
	double convergenceThreshold;
	std::deque<std::vector<unsigned int> > observationsHistory;
	std::deque<unsigned int > actionHistory;
	unsigned int countSteps;
	unsigned int episodeLength;
	bool useGPOMDP;
	double totalReward;
	double averageReward;
	unsigned long long averageRewardCount;
	vector<double> averageRewardAtTime;
	vector<unsigned long long> averageRewardAtTimeCount;
	bool useTotalReward;
	bool useBaseLine;
	bool useSoftMax;
	double temperature;
	
	PolicyTree(unsigned int sizeHistory, unsigned int sizeObservations, unsigned int numObservations, unsigned int numActions, double minActionProbability, double learningRate, unsigned int maxLeafNodes, unsigned int gradientAverageWindowForConvergence, double convergenceThreshold, bool useGPOMDP, bool useTotalReward, bool useBaseLine, bool useSoftMax) : sizeHistory(sizeHistory), sizeObservations(sizeObservations), numObservations(numObservations), numActions(numActions), numLeafNodes(1), minActionProbability(minActionProbability), learningRate(learningRate), maxLeafNodes(maxLeafNodes), gradientAverageWindowForConvergence(gradientAverageWindowForConvergence), convergenceThreshold(convergenceThreshold), countSteps(0), episodeLength(0), useGPOMDP(useGPOMDP), averageReward(0), averageRewardCount(0), useTotalReward(useTotalReward), useBaseLine(useBaseLine), useSoftMax(useSoftMax), temperature(1.0)
	{
		root = new PolicyTreeNode(*this, false);
		assert(numActions*minActionProbability < 1.0);
		assert(gradientAverageWindowForConvergence > 0);
		assert(maxLeafNodes > 0);
		resetHistory();
	}
	
	~PolicyTree()
	{
		freePolicyTreeNode(root);
	}
	
	void freePolicyTreeNode(PolicyTreeNode *node)
	{
		for(unsigned int i=0; i < node->children.size(); i++)
		{
			freePolicyTreeNode(node->children[i]);
		}
		
		delete node;
	}
	
	PolicyTreeNode* getCurrentNode()
	{
		return getCurrentNode(root);
	}
	
	PolicyTreeNode* getCurrentNode(PolicyTreeNode* node)
	{
		if(node->children.empty())
		{
			return node;
		}
		else
		{
			assert(node->hIndex >= 0);
			
			if(node->oIndex == -1)
			{
				if((unsigned int)node->hIndex < actionHistory.size())
				{
					unsigned int action = actionHistory.at(node->hIndex);
					assert(action < node->children.size());
					return getCurrentNode(node->children[action]);
				}
				else
				{
					return getCurrentNode(node->children[0]);
				}
			}
			else
			{
				if((unsigned int)node->hIndex < observationsHistory.size())
				{
					std::vector<unsigned int> observations = observationsHistory.at(node->hIndex);
					assert(node->oIndex >= 0);
					assert((unsigned int)node->oIndex < observations.size());
					unsigned int observation = observations[node->oIndex];
					assert(observation < node->children.size());
					return getCurrentNode(node->children[observation]);
				}
				else
				{
					return getCurrentNode(node->children[0]);
				}
			}
		}
	}
	
	void storeObservations(std::vector<unsigned int> observations)
	{
		observationsHistory.push_front(observations);
		
		if(observationsHistory.size() > sizeHistory)
		{
			observationsHistory.pop_back();
		}
	}
	
	void storeAction(unsigned int action)
	{
		actionHistory.push_front(action);
		
		if(actionHistory.size() > sizeHistory)
		{
			actionHistory.pop_back();
		}
	}
	
	unsigned int storeObservationsAndGetAction(std::vector<unsigned int> observations)
	{
		storeObservations(observations);
		return getAction();
	}
	
	unsigned int getAction()
	{
		PolicyTreeNode *currentNode = getCurrentNode();
		assert(currentNode);
		assert(!currentNode->isFringe);
		assert(currentNode->actionProbabilities.size() == numActions);
		unsigned int action = sample(currentNode->actionProbabilities);
		return action;
	}
	
	void processAction(unsigned int action, double reward)
	{
		episodeLength++;
		PolicyTreeNode *currentNode = getCurrentNode();
		assert(currentNode);
		assert(!currentNode->isFringe);
		assert(currentNode->actionProbabilities.size() == numActions);
		currentNode->updateZ(action);
		
		if(averageRewardAtTime.size() < episodeLength)
		{
			assert(averageRewardAtTime.size() == episodeLength - 1);
			averageRewardAtTime.push_back(0);
			averageRewardAtTimeCount.push_back(1);
		}
		
		assert(averageRewardAtTime.size() >= episodeLength);
		assert(averageRewardAtTimeCount.size() >= episodeLength);
		
		if(useGPOMDP)
		{
			if(useBaseLine)
			{
				updateGPOMDPGradient(reward - averageRewardAtTime[episodeLength - 1]);
			}
			else
			{
				updateGPOMDPGradient(reward);
			}
		}
		
		averageRewardAtTimeCount[episodeLength - 1]++;
		averageRewardAtTime[episodeLength - 1] += ((double)1/averageRewardAtTimeCount[episodeLength - 1]) * (reward - averageRewardAtTime[episodeLength - 1]);
				
		averageRewardCount++;
		averageReward += (reward - averageReward)/averageRewardCount;
		totalReward += reward;
		storeAction(action);
	}
	
	void processEpisode()
	{
		if(!useGPOMDP)
		{
			if(useBaseLine)
			{
				updateREINFORCEGradient(totalReward - episodeLength*averageReward);
			}
			else
			{
				updateREINFORCEGradient(totalReward);
			}
		}
		
		countSteps++;
		
		storeGradient();
		performGradientStep();
		
		if(countSteps % gradientAverageWindowForConvergence == 0)
		{
			checkForDivergingNodes();
			resetGradients();
			countSteps = 0;
		}
		
		episodeLength = 0;
		totalReward = 0;
		resetHistory();
	}
	
	
	bool checkForDivergingNodes()
	{
		return checkForDivergingNodes(root);
	}
	
	bool checkForDivergingNodes(PolicyTreeNode *node)
	{
		bool ret = false;
		
		if(node->children.size() == 0)
		{
			if(node->checkForDivergingNodes())
			{
				ret = true;
			}
		}
		else
		{
			for(unsigned int i=0; i < node->children.size(); i++)
			{
				if(checkForDivergingNodes(node->children[i]))
				{
					ret = true;
				}
			}
		}
		
		return ret;
	}
	
	void updateGPOMDPGradient(double reward)
	{
		updateGPOMDPGradient(root, reward);
	}
	
	void updateGPOMDPGradient(PolicyTreeNode *node, double reward)
	{
		if(node->children.size() == 0)
		{
			node->updateGPOMDPGradient(reward);
		}
		else
		{
			for(unsigned int i=0; i < node->children.size(); i++)
			{
				updateGPOMDPGradient(node->children[i], reward);
			}
		}
	}
	
	void updateREINFORCEGradient(double totalReward)
	{
		updateREINFORCEGradient(root, totalReward);
	}
	
	void updateREINFORCEGradient(PolicyTreeNode *node, double totalReward)
	{
		if(node->children.size() == 0)
		{
			node->updateREINFORCEGradient(totalReward);
		}
		else
		{
			for(unsigned int i=0; i < node->children.size(); i++)
			{
				updateREINFORCEGradient(node->children[i], totalReward);
			}
		}
	}
	
	void storeGradient()
	{
		storeGradient(root);
	}
	
	void storeGradient(PolicyTreeNode *node)
	{
		if(node->children.size() == 0)
		{
			node->storeGradient();
		}
		else
		{
			for(unsigned int i=0; i < node->children.size(); i++)
			{
				storeGradient(node->children[i]);
			}
		}
	}
	
	void resetGradients()
	{
		resetGradients(root);
	}
	
	void resetGradients(PolicyTreeNode *node)
	{
		if(node->children.size() == 0)
		{
			node->resetGradients();
		}
		else
		{
			for(unsigned int i=0; i < node->children.size(); i++)
			{
				resetGradients(node->children[i]);
			}
		}
	}
	
	void performGradientStep()
	{
		performGradientStep(root);
	}
	
	void performGradientStep(PolicyTreeNode *node)
	{	
		if(node->children.size() == 0)
		{
			node->performGradientStep();
		}
		else
		{
			for(unsigned int i=0; i < node->children.size(); i++)
			{
				performGradientStep(node->children[i]);
			}
		}
	}
	
	void resetHistory()
	{
		observationsHistory.clear();
		actionHistory.clear();
		
		for(int i=0; i < sizeHistory; i++)
		{
			storeObservations(std::vector<unsigned int>(sizeObservations, 0));
			storeAction(0);
		}
	}
};


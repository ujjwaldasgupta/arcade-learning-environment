#ifndef __POLICY_TREE_LINEAR_INCLUDED__
#define __POLICY_TREE_LINEAR_INCLUDED__

#include <vector>
#include <cstdio>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <deque>
#include <set>
#include <numeric>
#include <functional>
#include <algorithm>
#include <iostream>
#include <unordered_set>
#include "sample.hpp"

using namespace std;

const double tolerance = 1e-6;

class PolicyTreeLinear
{
public:
    class PolicyTreeLinearNode
    {
    public:
        PolicyTreeLinear &policyTree;
        bool isFringe;
        vector<PolicyTreeLinearNode* > children;
        vector<PolicyTreeLinearNode* > fringeChildren;
        vector<vector<double> > actionParameters;
        vector<vector<double> > Z;
        vector<vector<double> > actionParametersGradient;
        vector<vector<double> > sumActionParametersGradient;
        unordered_set<int> observationsToUpdateCache;
        vector<int> splitIndices;
        double learningRate;
        
        PolicyTreeLinearNode *parent;

        PolicyTreeLinearNode(PolicyTreeLinear &policyTree, bool isFringe) : policyTree(policyTree), isFringe(isFringe), Z(policyTree.numActions, vector<double>(policyTree.numObservations, 0)), actionParameters(policyTree.numActions, vector<double>(policyTree.numObservations, 0)), actionParametersGradient(policyTree.numActions, vector<double>(policyTree.numObservations, 0)), parent(NULL), sumActionParametersGradient(policyTree.numActions, vector<double>(policyTree.numObservations, 0)), learningRate(policyTree.learningRate)
        {
            if(!isFringe)
            {
                createFringe();
            }
        }

        ~PolicyTreeLinearNode()
        {
            if(!isFringe)
            {
                deleteFringe();
            }
        }

        void createFringe()
        {
            assert(!isFringe);

            fringeChildren = vector<PolicyTreeLinearNode * >(policyTree.numObservations);
            
            for(int i = 0; i < policyTree.numObservations; i++)
            {
                fringeChildren[i] = new PolicyTreeLinearNode(policyTree, true);
                fringeChildren[i]->parent = this;
            }
        }

        void deleteFringe()
        {
            assert(!isFringe);

            for(int i = 0; i < policyTree.numObservations; i++)
            {
                if(fringeChildren[i])
                {
                    delete fringeChildren[i];
                }
            }

            fringeChildren.clear();
        }

        void updateZ(int nextAction)
        {
            if(isFringe && policyTree.onlyUseFringeBias)
            {
                observationsToUpdateCache.insert(0);
                for(int i = 0; i < policyTree.numActions; i++)
                {
                    double correctiveTerm = 1 + policyTree.minActionProbability/((1-policyTree.numActions*policyTree.minActionProbability)*policyTree.actionProbabilities[i]);
                    Z[i][0] += ((i==nextAction)-policyTree.actionProbabilities[i])/(policyTree.temperature*correctiveTerm);
                }
            }
            else
            {
                for(auto it = policyTree.observations.begin(); it != policyTree.observations.end() ; it++)
                {
                    if(observationsToUpdateCache.find(*it) == observationsToUpdateCache.end())
                    {
                        observationsToUpdateCache.insert(*it);
                    }
                    
                    for(int i = 0; i < policyTree.numActions; i++)
                    {    
                        assert(*it >= 0);
                        assert(*it < policyTree.numObservations);
                        
                        double correctiveTerm = 1 + policyTree.minActionProbability/((1-policyTree.numActions*policyTree.minActionProbability)*policyTree.actionProbabilities[i]);
                        
                        Z[i][*it] += ((i==nextAction)-policyTree.actionProbabilities[i])/(policyTree.temperature*correctiveTerm);
                    }
                }
            }
            
            if(!isFringe && !policyTree.optimizing)
            {
                for(auto it = policyTree.observations.begin(); it != policyTree.observations.end(); it++)
                {
                    fringeChildren[*it]->updateZ(nextAction);
                }
            }
        }
        
        void updateGradient(double reward)
        {
            if(isFringe && policyTree.onlyUseFringeBias)
            {
                for(int i = 0; i < policyTree.numActions; i++)
                {
                    actionParametersGradient[i][0] += reward * Z[i][0];
                    Z[i][0] *= policyTree.discountFactor;
                }
            }
            else
            {
                for(int i=0; i < policyTree.numActions; i++)
                {
                    for(auto it = observationsToUpdateCache.begin(); it != observationsToUpdateCache.end(); it++)
                    {
                        assert(*it >= 0);
                        assert(*it < policyTree.numObservations);
                        actionParametersGradient[i][*it] += reward * Z[i][*it];
                        Z[i][*it] *= policyTree.discountFactor;
                    }
                }
            }

            if(!isFringe && !policyTree.optimizing)
            {
                for(auto it = observationsToUpdateCache.begin(); it != observationsToUpdateCache.end(); it++)
                {
                    fringeChildren[*it]->updateGradient(reward);
                }
            }
        }
        
        void performGradientStep()
        {
            assert(!isFringe);
                        
            for(int i=0; i < policyTree.numActions; i++)
            {
                for(auto it = observationsToUpdateCache.begin(); it != observationsToUpdateCache.end(); it++)
                {
                    assert(*it >= 0);
                    assert(*it < policyTree.numObservations);
                    actionParameters[i][*it] += (policyTree.learningRate * actionParametersGradient[i][*it]);
                }
            }
            
            normalizeParameters();
        }

        void storeGradient()
        {
            assert(!policyTree.optimizing);
            
            for(int i=0; i < policyTree.numActions; i++)
            {
                for(auto it = observationsToUpdateCache.begin(); it != observationsToUpdateCache.end(); it++)
                {
                    assert(*it >= 0);
                    assert(*it < policyTree.numObservations);
                    sumActionParametersGradient[i][*it] += actionParametersGradient[i][*it];
                }
            }
            
            if(!isFringe)
            {
                for(auto it = observationsToUpdateCache.begin(); it != observationsToUpdateCache.end(); it++)
                {
                    assert(*it >= 0);
                    assert(*it < policyTree.numObservations);
                    fringeChildren[*it]->storeGradient();
                }
            }
        }
        
        void normalizeParameters()
        {
            for(int j=0; j < policyTree.numObservations; j++)            
            {
                double average = 0;
                
                for(int i=0; i < policyTree.numActions; i++)
                {
                    average += actionParameters[i][j];
                }
                
                average /= policyTree.numActions;
                
                for(int i=0; i < policyTree.numActions; i++)
                {
                    actionParameters[i][j] -= average;
                }             
                
            }
        }
        
        void cleanUpAfterEpisode()
        {
            actionParametersGradient = vector<vector<double> >(policyTree.numActions, vector<double>(policyTree.numObservations, 0));
            Z = vector<vector<double> >(policyTree.numActions, vector<double>(policyTree.numObservations, 0));

            if(!policyTree.optimizing)
            {
                for(auto it = observationsToUpdateCache.begin(); it != observationsToUpdateCache.end(); it++)
                {
                    fringeChildren[*it]->Z = vector<vector<double> >(policyTree.numActions, vector<double>(policyTree.numObservations, 0));
                    fringeChildren[*it]->actionParametersGradient = vector<vector<double> >(policyTree.numActions, vector<double>(policyTree.numObservations, 0));
                }
            }

            observationsToUpdateCache.clear();
        }

        pair<double, int> checkForDivergingNodes(double treeMaxNorm)
        {
            int bestSplitIndex = -1;
            double bestScore = 0;
            vector<double> scores(policyTree.numObservations, 0);
            vector<double> otherScores(policyTree.numObservations, 0);
            
            for(int i = 0; i < policyTree.numObservations; i++)
            {
                vector<vector<double> > complementFringeSumActionParametersGradient(policyTree.numActions, vector<double>(policyTree.numObservations));
                
                for(int j = 0; j < policyTree.numActions; j++)
                {
                    for(int k = 0; k < policyTree.numObservations; k++)
                    {
                        complementFringeSumActionParametersGradient[j][k] = sumActionParametersGradient[j][k] - fringeChildren[i]->sumActionParametersGradient[j][k];
                    }
                }
            
                double score = max(maxNorm(fringeChildren[i]->sumActionParametersGradient), maxNorm(complementFringeSumActionParametersGradient));
                scores[i] = score - treeMaxNorm;
                otherScores[i] = l1Norm(fringeChildren[i]->sumActionParametersGradient) + l1Norm(complementFringeSumActionParametersGradient) - l1Norm(sumActionParametersGradient);

                if(policyTree.useDotProductScore)
                {
                    double temp = (double)rand()/RAND_MAX;
                    
                    if(temp > bestScore)
                    {
                        bestScore = temp;
                        bestSplitIndex = i;
                    }
                }
                else
                {
                    if(policyTree.onlyUseFringeBias)
                    {
                        if(scores[i] > bestScore)
                        {
                            bestScore = scores[i];
                            bestSplitIndex = i;
                        }
                    }
                    else
                    {
                        if(otherScores[i] > bestScore)
                        {
                            bestScore = otherScores[i];
                            bestSplitIndex = i;
                        }
                    }
                }
            }

            return make_pair(bestScore, bestSplitIndex);
        }

        /* This should be called only after a "converged" tree is ready to be optimized again(i.e. a node has been added or removed, or the tree has been found to be divergent). */
        void resetGradients()
        {
            actionParametersGradient = vector<vector<double> >(policyTree.numActions, vector<double>(policyTree.numObservations, 0));
            sumActionParametersGradient = vector<vector<double> >(policyTree.numActions, vector<double>(policyTree.numObservations, 0));

            if(!isFringe)
            {
                for(int i = 0; i < policyTree.numObservations; i++)
                {
                    fringeChildren[i]->resetGradients();
                }
            }
        }

        void promoteObservationFringeNode(int splitIndex)
        {
            splitIndices.push_back(splitIndex);

            cout << "Adding split on " << splitIndex << "\n";
            
            if(policyTree.useMultiSplit)
            {
                for(int i = 0; i < policyTree.numObservations; i++)
                {
                    if(i != splitIndex && cosine(fringeChildren[i]->sumActionParametersGradient, fringeChildren[splitIndex]->sumActionParametersGradient) > policyTree.cosineThreshold)
                    {
                        cout << ", "<<i;
                        splitIndices.push_back(i);
                    }
                }
            }
            
            cout<<"\n";
            
            children = vector<PolicyTreeLinearNode * >(2, NULL);
            
            bool fringeDivergent = dotProduct(sumActionParametersGradient, fringeChildren[splitIndex]->sumActionParametersGradient) < 0;
            
            children[0] = new PolicyTreeLinearNode(policyTree, false);    
            children[0]->parent = this;
            children[1] = new PolicyTreeLinearNode(policyTree, false);    
            children[1]->parent = this;
            children[0]->actionParameters = actionParameters;
            children[1]->actionParameters = actionParameters;
            deleteFringe();
            policyTree.numLeafNodes += 1;
        }

        double l2NormSquared(vector<vector<double> >& v)
        {
            double ans = 0;
            
            for(int i=0; i < v.size(); i++)
            {
                for(int j=0; j < v[i].size(); j++)
                {
                    ans += v[i][j] * v[i][j];
                }
            }
            
            return ans;
        }
        
        double l2Norm(vector<vector<double> >& v)
        {
            return sqrt(l2NormSquared(v));
        }
        
        double l1Norm(vector<vector<double> >& v)
        {
            double ans = 0;
            
            for(int i=0; i < v.size(); i++)
            {
                for(int j=0; j < v[i].size(); j++)
                {
                    ans += abs(v[i][j]);
                }
            }
            
            return ans;
        }
        
        double maxNorm(vector<vector<double> >& v)
        {
            double ans = 0;
            
            for(int i=0; i < v.size(); i++)
            {
                if(policyTree.onlyUseFringeBias)
                {
                    assert(v[i].size() > 0);
                    
                    if(abs(v[i][0]) > ans)
                    {
                        ans = abs(v[i][0]);
                    }
                }
                else
                {
                    for(int j=0; j < v[i].size(); j++)
                    {
                        if(abs(v[i][j]) > ans)
                        {
                            ans = abs(v[i][j]);
                        }
                    }
                }
            }
            
            return ans;
        }
        
        double mostDivergingParameter(vector<vector<double> >& v1, vector<vector<double> >& v2)
        {
            double ans = 0;
            assert(v1.size() == v2.size());
            
            for(int i=0; i < v1.size(); i++)
            {
                assert(v1[i].size() == v2[i].size());
                
                for(int j=0; j < v1[i].size(); j++)
                {
                    if(v2[i][j]*v1[i][j] < 0)
                    {
                        if(min(fabs(v1[i][j]), fabs(v2[i][j])) > ans)
                        {
                            ans = min(fabs(v1[i][j]), fabs(v2[i][j]));
                        }
                    }
                }
            }
            
            return ans;  
        }
        
        double dotProduct(vector<vector<double> >& v1, vector<vector<double> >& v2)
        {
            double ans = 0;
            assert(v1.size() == v2.size());
            
            for(int i=0; i < v1.size(); i++)
            {
                assert(v1[i].size() == v2[i].size());
                
                for(int j=0; j < v1[i].size(); j++)
                {
                    ans += v1[i][j] * v2[i][j];
                }
            }
            
            return ans;
        }
        
        double cosine(vector<vector<double> >& v1, vector<vector<double> >& v2)
        {
            if(l2Norm(v1) == 0 || l2Norm(v2) == 0)
            {
                return 0;
            }
            else
            {
                return dotProduct(v1, v2)/(l2Norm(v1)*l2Norm(v2));
            }
        }
    };

    PolicyTreeLinearNode *root;
    int numObservations;
    int numActions;
    int numLeafNodes;
    double learningRate;
    double minActionProbability;
    int maxLeafNodes;
    int gradientAverageWindow;
    int optimizationSteps;
    unordered_set<int> observations;
    deque<int > actionHistory;
    bool optimizing;
    int countSteps;
    int episodeLength;
    unsigned long long episodeCount;
    vector<double> averageRewardAtTime;
    bool useBaseLine;
    bool useMultiSplit;
    bool onlyUseFringeBias;
    bool useDotProductScore;
    double cosineThreshold;
    double discountFactor;
    double temperature;
    vector<double> actionProbabilities;
    
    PolicyTreeLinear(int numObservations, int numActions, double learningRate, double minActionProbability, int maxLeafNodes, int gradientAverageWindow, int optimizationSteps, bool useBaseLine, bool useMultiSplit, double cosineThreshold, bool onlyUseFringeBias, bool useDotProductScore, double discountFactor) : numObservations(numObservations), numActions(numActions), numLeafNodes(1), learningRate(learningRate), minActionProbability(minActionProbability), maxLeafNodes(maxLeafNodes), gradientAverageWindow(gradientAverageWindow), optimizationSteps(optimizationSteps), countSteps(0), episodeLength(0), optimizing(true), episodeCount(0), useBaseLine(useBaseLine), useMultiSplit(useMultiSplit), cosineThreshold(cosineThreshold), onlyUseFringeBias(onlyUseFringeBias), useDotProductScore(useDotProductScore), discountFactor(discountFactor), temperature(1.0)
    {
        root = new PolicyTreeLinearNode(*this, false);
        assert(gradientAverageWindow > 0);
        assert(optimizationSteps >= 0);
        assert(maxLeafNodes > 0);
        actionProbabilities = vector<double>(numActions);
        updateActionProbabilities();
        
        if(optimizationSteps == 0)
        {
            optimizing = false;
        }
    }

    ~PolicyTreeLinear()
    {
        freePolicyTreeLinearNode(root);
    }

    void freePolicyTreeLinearNode(PolicyTreeLinearNode *node)
    {
        for(int i = 0; i < node->children.size(); i++)
        {
            freePolicyTreeLinearNode(node->children[i]);
        }

        delete node;
    }

    PolicyTreeLinearNode *getCurrentNode()
    {
        return getCurrentNode(root);
    }

    PolicyTreeLinearNode *getCurrentNode(PolicyTreeLinearNode *node)
    {
        if(node->children.empty())
        {
            return node;
        }
        else
        {
            bool flag = false;

            for(int i = 0; i < node->splitIndices.size(); i++)
            {
                assert(node->splitIndices[i] < numObservations);
                
                if(observations.find(node->splitIndices[i]) != observations.end())
                {
                    flag = true;
                    break;
                }
            }

            if(flag)
            {
                return getCurrentNode(node->children[1]);
            }
            else
            {
                return getCurrentNode(node->children[0]);
            }
        }
    }

    void storeObservations(unordered_set<int>& observations)
    {
        this->observations = observations;
        updateActionProbabilities();
    }

    int storeObservationsAndGetAction(unordered_set<int>& observations)
    {
        storeObservations(observations);
        return getAction();
    }

    int getAction()
    {
        PolicyTreeLinearNode *currentNode = getCurrentNode();
        int action = sample(actionProbabilities);
        double r =(double) rand() / RAND_MAX;
        
        if(r < minActionProbability*numActions)
        {
            action = rand() % numActions;
        }
        
        return action;
    }

    void processAction(int action, double reward)
    {
        PolicyTreeLinearNode *currentNode = getCurrentNode();
        assert(currentNode);
        assert(!currentNode->isFringe);
        currentNode->updateZ(action);
        double baseLine = 0;

        if(episodeLength < averageRewardAtTime.size())
        {
            baseLine = averageRewardAtTime[episodeLength];
        }

        if(useBaseLine)
        {
            updateGradient(reward - baseLine);
        }
        else
        {
            updateGradient(reward);
        }

        if(episodeLength < averageRewardAtTime.size())
        {
            averageRewardAtTime[episodeLength] +=((double) 1 /(episodeCount + 1)) *(reward - averageRewardAtTime[episodeLength]);
        }
        else
        {
            assert(episodeLength == averageRewardAtTime.size());
            averageRewardAtTime.push_back(reward /(episodeCount + 1));
        }

        episodeLength++;
    }

    void processEpisode()
    {
        episodeCount++;
        
        if(optimizing)
        {
            countSteps++;
            performGradientStep();
            updateActionProbabilities();
            
            if(countSteps == optimizationSteps)
            {
                optimizing = false;
                countSteps = 0;
            }
        }
        else
        {       
            countSteps++;
            storeGradient();
            if(countSteps % gradientAverageWindow == 0)
            {
                double treeMaxNorm = getTreeMaxNorm();
                pair<PolicyTreeLinearNode *, pair<double, int> > ret = checkForDivergingNodes(treeMaxNorm);
                if(ret.first && ret.second.second >= 0)
                {
                    ret.first->promoteObservationFringeNode(ret.second.second);
                    resetGradients();
                    updateActionProbabilities();
                }
                countSteps = 0;
                optimizing = true;
            }
        }

        observations.clear();
        cleanUpAfterEpisode();
        episodeLength = 0;
    }

    double getTreeMaxNorm()
    {
        return getTreeMaxNorm(root);
    }

    double getTreeMaxNorm(PolicyTreeLinearNode *node)
    {
        double treeMaxNorm = 0;
        
        if(node->children.size() == 0)
        {
            treeMaxNorm = node->maxNorm(node->sumActionParametersGradient);
        }
        else
        {
            for(int i = 0; i < node->children.size(); i++)
            {
                double temp = getTreeMaxNorm(node->children[i]);
                if(temp > treeMaxNorm)
                {
                    treeMaxNorm = temp;
                }
            }
        }

        return treeMaxNorm;
    }

    pair<PolicyTreeLinearNode *, pair<double, int > > checkForDivergingNodes(double treeMaxNorm)
    {
        return checkForDivergingNodes(root, treeMaxNorm);
    }

    pair<PolicyTreeLinearNode *, pair<double, int > > checkForDivergingNodes(PolicyTreeLinearNode *node, double treeMaxNorm)
    {
        pair<PolicyTreeLinearNode *, pair<double, int > > ret;
        ret.first = NULL;
        ret.second.first = 0;
        ret.second.second = -1;

        if(node->children.size() == 0)
        {
            ret.first = node;
            ret.second = node->checkForDivergingNodes(treeMaxNorm);
        }
        else
        {
            for(int i = 0; i < node->children.size(); i++)
            {
                pair<PolicyTreeLinearNode *, pair<double, int > > childRet = checkForDivergingNodes(node->children[i], treeMaxNorm);
                if(childRet.second.first > ret.second.first)
                {
                    ret = childRet;
                }
            }
        }

        return ret;
    }

    void updateGradient(double reward)
    {
        updateGradient(root, reward);
    }

    void updateGradient(PolicyTreeLinearNode *node, double reward)
    {
        if(node->children.size() == 0)
        {
            node->updateGradient(reward);
        }
        else
        {
            for(int i = 0; i < node->children.size(); i++)
            {
                updateGradient(node->children[i], reward);
            }
        }
    }

    void storeGradient()
    {
        storeGradient(root);
    }

    void storeGradient(PolicyTreeLinearNode *node)
    {
        if(node->children.size() == 0)
        {
            node->storeGradient();
        }
        else
        {
            for(int i = 0; i < node->children.size(); i++)
            {
                storeGradient(node->children[i]);
            }
        }
    }

    void resetGradients()
    {
        resetGradients(root);
    }

    void resetGradients(PolicyTreeLinearNode *node)
    {
        if(node->children.size() == 0)
        {
            node->resetGradients();
        }
        else
        {
            for(int i = 0; i < node->children.size(); i++)
            {
                resetGradients(node->children[i]);
            }
        }
    }

    void performGradientStep()
    {
        performGradientStep(root);
    }

    void performGradientStep(PolicyTreeLinearNode *node)
    {
        if(node->children.size() == 0)
        {
            node->performGradientStep();
        }
        else
        {
            for(int i = 0; i < node->children.size(); i++)
            {
                performGradientStep(node->children[i]);
            }
        }
    }

    void updateActionProbabilities()
    {
        double normalization = 0;
        vector<double> sumParams(numActions, 0);
        PolicyTreeLinearNode* currentNode = getCurrentNode();
        assert(currentNode);
        assert(!currentNode->isFringe);
        
        for(int i = 0; i < numActions; i++)
        {
            double sum = 0;
            
            for(auto it = observations.begin(); it != observations.end(); it++)
            {
                assert(*it < numObservations);
                sum += currentNode->actionParameters[i][*it];
            }
            
            sumParams[i] = sum;
        }

        double maxSum = *max_element(sumParams.begin(), sumParams.end());
        
        for(int i = 0; i < numActions; i++)
        {    
            actionProbabilities[i] = exp((sumParams[i]-maxSum)/temperature);
            normalization += actionProbabilities[i];
        }
        
        for(int i = 0; i < numActions; i++)
        {    
            actionProbabilities[i] /= normalization;
        }
    }
        
    void cleanUpAfterEpisode()
    {
        cleanUpAfterEpisode(root);
    }

    void cleanUpAfterEpisode(PolicyTreeLinearNode *node)
    {
        if(node->children.size() == 0)
        {
            node->cleanUpAfterEpisode();
        }
        else
        {
            for(int i = 0; i < node->children.size(); i++)
            {
                cleanUpAfterEpisode(node->children[i]);
            }
        }
    }
};

#endif
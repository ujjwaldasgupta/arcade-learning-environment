export CXX := g++
export ALE := /home/ujjwal/Programs/ale_0.4.3/ale_0_4
export CXXFLAGS := -g -std=c++11 -I$(ALE)/src -L$(ALE) -lale -lz -lSDL -lSDL_gfx -lSDL_image -D__USE_SDL

PROGRAMS=agentPollyLinear

all:  $(PROGRAMS)

clean: 
	rm -f $(PROGRAMS)
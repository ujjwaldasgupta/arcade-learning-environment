#include <iostream>
#include <vector>
#include <cstdlib>
#include <cassert>

#include "policy_gradient_gpomdp.hpp"
#include "Asterix.h"

int main(int argc, char** argv)
{
    if(argc < 13)
    {
        std::cerr<<"Usage: "<<argv[0]<<"runs height width restTime learningRate temperature expand relative proximity powerChance pointRatio seed"<<'\n';
        return 1;
    }
    
    int runs = atoi(argv[1]);
    int height = atoi(argv[2]);
    int width = atoi(argv[3]);
    int restTime = atoi(argv[4]);
    double learningRate = atof(argv[5]);
    double temperature = atof(argv[6]);
    bool expand = atoi(argv[7]) > 0 ? true : false;
    bool relative = atoi(argv[8]) > 0 ? true : false;
    int proximity = atoi(argv[9]);
    double powerChance = atoi(argv[10]);
    int pointRatio = atoi(argv[11]);
    int seed = atoi(argv[12]);
    
    srand(seed);
    Asterix game(height, width, restTime, relative, proximity, powerChance, pointRatio);
    game.reset();
    double sumTotalReward = 0;
    int numFeatures = game.getNumFeatures();
    
    if(expand)
    {
        numFeatures = numFeatures + numFeatures*(numFeatures-1)/2;
    }
    
    PolicyGradientGPOMDP policy(1, numFeatures, game.getNumActions(), learningRate, temperature);

    for(int i=0; i < runs; i++)
    {
        game.reset();
        double totalReward = 0;
        
        while(game.isActive())
        {
            std::unordered_set<int> observations = game.getFeatures();
            if(expand)
            {
                observations = expandFeatures(observations, game.getNumFeatures());
            }
            int action = policy.storeObservationsAndGetAction(observations);
            double reward = game.processAction(action);
            policy.updateStoredGradient(action, reward);
            totalReward += reward;
        }
        
        policy.processEpisode();
        sumTotalReward += totalReward;
        std::cout<<"Episode "<<i<<" ended with score: "<<totalReward<<"\n";
    }
    
    return 0;
}
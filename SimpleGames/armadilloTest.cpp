#include <iostream>
#include <armadillo>

using namespace std;
using namespace arma;

int main(int argc, char** argv)
{
    mat A = randu<mat>(4,5);
    mat B = randu<mat>(4,5);
    mat C = zeros<mat>(4,0);

    C.insert_cols(0, A);
    
    cout << A*B.t() << endl;
    cout << A << endl;
    cout << A.col(0) << endl;
    cout << C << endl;
    
    return 0;
}
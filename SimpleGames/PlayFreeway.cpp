#include <iostream>
#include <vector>
#include <cstdlib>
#include <cassert>
#include <ctime>

#include "Freeway.h"

int main(int argc, char** argv)
{
    Freeway game(true, 2);
    double totalReward = 0;
    game.reset();
    srand(std::time(0));
    
    while(game.isActive())
    {
        std::vector<std::vector<int> > screen = game.getScreen();
        
        for(int i=0; i < screen.size(); i++)
        {
            for(int j=0; j < screen[i].size(); j++)
            {
                std::cout<<screen[i][j];
            }
            std::cout<<"\n";
        }
        
        int action;
        std::cin>>action;
        totalReward += game.processAction(action);
    }
    
    std::cout<<"Total Reward: "<<totalReward<<"\n";
    return 0;
}
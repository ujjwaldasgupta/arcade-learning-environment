#include <vector>
#include <random>

using namespace std;

default_random_engine generator;

int sample(vector<double>& probabilities)
{
	discrete_distribution<int> distribution(probabilities.begin(), probabilities.end());
	return distribution(generator);
}

#include <unordered_set>
#include <iostream>

#include "Invaders.h"

using namespace std;

int main(int argc, char** argv) 
{
    if(argc < 2)
    {
            cerr << "Usage: " << argv[0] << "runs" << endl;
            return 1;
    }
    
    int runs = atoi(argv[1]);
    double sumReward = 0;        
    double sumEpisodeLength = 0;
    
    Invaders game(4, 16);

    for(int episode=0; episode<runs; episode++) 
    {
        game.reset();
        double totalReward = 0;
        int episodeLength = 0;
        
        while(game.isActive()) 
        {
            unordered_set<int> features = game.getFeatures();
            vector<vector<int> > screen = game.getScreen();
            
//             for(int i=0; i < screen.size(); i++)
//             {
//                 for(int j=0; j < screen[i].size(); j++)
//                 {
//                     cout<<screen[i][j];
//                 }
//                 cout<<"\n";
//             }
//             
//             cout<<"\n";
//             
            int action = 0;
            
            if((features.find(321) != features.end()) ^ (features.find(121) != features.end()))
            {
                action = 3;
            }
            
            for(int i=322; i <= 325; i++)
            {
                if(features.find(i) != features.end())
                {
                    action = 1;
                    break;
                }
            }
            
            for(int i=347; i <= 350; i++)
            {
                if(features.find(i) != features.end())
                {
                    action = 2;
                    break;
                }
            }
            
            totalReward += game.processAction(action);
            episodeLength++;
        }
        sumReward += totalReward;
        sumEpisodeLength += episodeLength;
        cout<<"Score: "<<totalReward<<"\n";
        cout<<"Average Episode Length: "<<(double)sumEpisodeLength/(episode+1)<<"\n";
        cout<<"Average Score: "<<(double)sumReward/(episode+1)<<"\n";
    }

    return 0;
}
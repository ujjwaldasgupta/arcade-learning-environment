#include <iostream>
#include <vector>
#include <cstdlib>
#include <cassert>

#include "policy_gradient_natural.hpp"
#include "XOR.h"

using namespace std;

int main(int argc, char** argv)
{
    if(argc < 8)
    {
        cerr<<"Usage: "<<argv[0]<<"runs learningRate numTrajectories maxEpisodeLength timeVariant regularizationConstant seed"<<'\n';
        return 1;
    }
    
    int runs = atoi(argv[1]);
    double learningRate = atof(argv[2]);
    int numTrajectories = atoi(argv[3]);
    int maxEpisodeLength = atoi(argv[4]);
    bool timeVariant = atoi(argv[5]) > 0 ? true : false;
    double regularizationConstant = atof(argv[6]);
    int seed = atoi(argv[7]);

    srand(seed);
    XOR game;
    game.reset();
    double sumTotalReward = 0;
    int numFeatures = game.getNumFeatures();
    
    PolicyGradientNatural policy(numFeatures, game.getNumActions(), learningRate, numTrajectories, maxEpisodeLength, timeVariant, regularizationConstant);

    for(int i=0; i < runs; i++)
    {
        game.reset();
        double totalReward = 0;
        int episodeLength = 0;
        
        while(game.isActive() && episodeLength < maxEpisodeLength)
        {
            std::unordered_set<int> observations = game.getFeatures();
            int action = policy.getAction(observations);
            double reward = game.processAction(action);
            policy.updateTrace(action, reward, observations);
            totalReward += reward;
            episodeLength++;
        }
        
        cout<<"Episode "<<i<<" ended with score: "<<totalReward<<"\n";
        sumTotalReward += totalReward;
        cout<<"Average score: "<<sumTotalReward/(i+1)<<"\n";
        policy.processEpisode();
    }
    
    return 0;
}
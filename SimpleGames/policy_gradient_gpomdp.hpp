#include <vector>
#include <deque>
#include <cassert>
#include <cmath>
#include <utility>
#include <cstdlib>
#include <algorithm>
#include <unordered_set>
#include "sample.hpp"

using namespace std;

struct pair_hash
{
    inline size_t operator() ( const pair<int, int> &v ) const
    {
        return v.first ^ v.second;
    }
};

class PolicyGradientGPOMDP
{
public:
    int sizeHistory;
    int sizeObservations;
    int numActions;
    vector<pair<int, int> > observationsHistory;
    vector<vector<vector<double> > > weights;
    vector<vector<vector<double> > > weightsGradient;
    vector<vector<vector<double> > > Z;
    vector<vector<vector<double> > > actionWeightCounts;
    unordered_set<pair<int, int>, pair_hash> activeZIndices;
    vector<double> averageRewardAtTime;
    vector<double> cachedActionProbabilities;
    bool isActionProbabilitiesCacheClean;
    double learningRate;
    double temperature;
    double decayFactor;
    double intrinsicRewardScale;
    double intrinsicReward;
    int currTime;
    int numEpisodes;

    PolicyGradientGPOMDP(int sizeHistory, int sizeObservations, int numActions, double learningRate, double temperature, double decayFactor, double intrinsicRewardScale) : sizeHistory(sizeHistory), sizeObservations(sizeObservations), numActions(numActions), weights(numActions, vector<vector<double> >(sizeHistory, vector<double>(sizeObservations, 0))), weightsGradient(numActions, vector<vector<double> >(sizeHistory, vector<double>(sizeObservations, 0))), Z(numActions, vector<vector<double> >(sizeHistory, vector<double>(sizeObservations, 0))), actionWeightCounts(numActions, vector<vector<double> >(sizeHistory, vector<double>(sizeObservations, 0))), learningRate(learningRate), temperature(temperature), decayFactor(decayFactor), intrinsicRewardScale(intrinsicRewardScale), intrinsicReward(0), isActionProbabilitiesCacheClean(false), currTime(0), numEpisodes(0)
    {
    }

    int storeObservationsAndGetAction(unordered_set<int> observations)
    {
        storeObservations(observations);
        return getAction();
    }

    void updateStoredGradient(int action, double reward)
    {
        vector<double> actionProbabilities = getActionProbabilities();
        reward += intrinsicReward;
        double baseLine = 0;
        
        if(currTime < averageRewardAtTime.size())
        {
            baseLine = averageRewardAtTime[currTime];
        }

        for(int i=0 ; i < numActions; i++)
        {
            assert(actionProbabilities[i] >= 0);
            assert(actionProbabilities[i] <= 1);
            
            for(int j=0; j < observationsHistory.size(); j++)
            {
                assert(observationsHistory[j].first < sizeHistory);
                assert(observationsHistory[j].second < sizeObservations);

                activeZIndices.insert(observationsHistory[j]);

                if(i == action)
                {
                    Z[i][observationsHistory[j].first][observationsHistory[j].second] +=(1 - actionProbabilities[i]) / temperature;
                }
                else
                {
                    Z[i][observationsHistory[j].first][observationsHistory[j].second] +=(-1) * actionProbabilities[i] / temperature;
                }
            }

            for(auto it = activeZIndices.begin(); it != activeZIndices.end(); it++)
            {
                weightsGradient[i][it->first][it->second] += (reward - baseLine) * Z[i][it->first][it->second];
            }
        }
        
        if(currTime < averageRewardAtTime.size())
        {
            averageRewardAtTime[currTime] +=(reward - averageRewardAtTime[currTime]) /(numEpisodes + 1);
        }
        else
        {
            assert(currTime == averageRewardAtTime.size());
            averageRewardAtTime.push_back(reward /(numEpisodes + 1));
        }
        
        currTime++;
    }

    vector<double> getActionProbabilities()
    {
        if(isActionProbabilitiesCacheClean)
        {
            return cachedActionProbabilities;
        }

        vector<double> actionProbabilities(numActions);
        vector<double> sumArray(numActions);

        for(int i=0; i < numActions; i++)
        {
            double sum = 0;

            for(int j=0; j < observationsHistory.size(); j++)
            {
                assert(observationsHistory[j].first < sizeHistory);
                assert(observationsHistory[j].second < sizeObservations);

                sum += weights[i][observationsHistory[j].first][observationsHistory[j].second];
            }

            sumArray[i] = sum;
        }

        double maxSum = *max_element(sumArray.begin(), sumArray.end());
        double normalization = 0;

        for(int i=0; i < numActions; i++)
        {
            actionProbabilities[i] = exp((sumArray[i]-maxSum)/temperature);
            normalization += actionProbabilities[i];
        }

        for(int i=0; i < numActions; i++)
        {
            actionProbabilities[i] /= normalization;
        }

        cachedActionProbabilities = actionProbabilities;
        isActionProbabilitiesCacheClean = true;

        return actionProbabilities;
    }

    int getAction()
    {
        vector<double> actionProbabilities = getActionProbabilities();
        int action = sample(actionProbabilities);

        double sumIntrinsicReward = 0;

        for(int i=0; i < observationsHistory.size(); i++)
        {
            sumIntrinsicReward += exp(-1.0*actionWeightCounts[action][observationsHistory[i].first][observationsHistory[i].second]/decayFactor);
            actionWeightCounts[action][observationsHistory[i].first][observationsHistory[i].second]++;
        }

        intrinsicReward = intrinsicRewardScale*sumIntrinsicReward;
        return action;
    }

    void storeObservations(unordered_set<int> observations)
    {
        vector<pair<int, int> > newObservationsHistory;
        newObservationsHistory.reserve(observations.size() + observationsHistory.size());

        for(int i=0; i < observationsHistory.size(); i++)
        {
            if(observationsHistory[i].first + 1 < sizeHistory)
            {
                assert(observationsHistory[i].second < sizeObservations);
                newObservationsHistory.push_back(make_pair(observationsHistory[i].first+1, observationsHistory[i].second));
            }
        }

        for(auto it = observations.begin(); it != observations.end(); it++)
        {
            assert(*it < sizeObservations);
            newObservationsHistory.push_back(make_pair(0, *it));
        }

        observationsHistory = newObservationsHistory;
        isActionProbabilitiesCacheClean = false;
    }

    void processEpisode()
    {
        for(int i=0 ; i < numActions; i++)
        {
            for(int j=0 ; j < sizeHistory; j++)
            {
                for(int k=0; k < sizeObservations; k++)
                {
                    weights[i][j][k] += learningRate * weightsGradient[i][j][k];
                    weightsGradient[i][j][k] = 0;
                    Z[i][j][k] = 0;
                }
            }
        }

        activeZIndices.clear();
        isActionProbabilitiesCacheClean = false;
        observationsHistory.clear();
        intrinsicReward = 0;
        currTime = 0;
        numEpisodes++;
    }
};



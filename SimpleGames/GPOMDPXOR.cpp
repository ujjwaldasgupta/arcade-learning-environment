#include <iostream>
#include <vector>
#include <cstdlib>
#include <cassert>

#include "policy_gradient_gpomdp.hpp"
#include "XOR.h"

int main(int argc, char** argv)
{
    if(argc < 6)
    {
        std::cerr<<"Usage: "<<argv[0]<<"runs learningRate temperature expand seed"<<'\n';
        return 1;
    }
    
    int runs = atoi(argv[1]);
    double learningRate = atof(argv[2]);
    double temperature = atof(argv[3]);
    bool expand = atoi(argv[4]) > 0 ? true : false;
    int seed = atoi(argv[5]);

    srand(seed);
    XOR game;
    game.reset();
    double sumTotalReward = 0;
    int numFeatures = game.getNumFeatures();
    
    if(expand)
    {
        numFeatures = numFeatures + numFeatures*(numFeatures-1)/2;
    }
    
    PolicyGradientGPOMDP policy(1, numFeatures, game.getNumActions(), learningRate, temperature, 1, 0);

    for(int i=0; i < runs; i++)
    {
        game.reset();
        double totalReward = 0;
        
        while(game.isActive())
        {
            std::unordered_set<int> observations = game.getFeatures();
            if(expand)
            {
                observations = expandFeatures(observations, game.getNumFeatures());
            }
            int action = policy.storeObservationsAndGetAction(observations);
            double reward = game.processAction(action);
            policy.updateStoredGradient(action, reward);
            totalReward += reward;
        }
        
        policy.processEpisode();
        sumTotalReward += totalReward;
        std::cout<<"Episode "<<i<<" ended with score: "<<totalReward<<"\n";
    }
    
    return 0;
}
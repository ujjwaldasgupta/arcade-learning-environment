#include <iostream>
#include <vector>
#include <unordered_set>
#include <cstdlib>
#include <ctime>

#include "policy_tree_linear.hpp"
#include "ComplexXOR.h"

using namespace std;

int main(int argc, char** argv)
{
    if(argc < 16)
    {
        cerr<<"Usage: "<<argv[0]<<"runs learningRate minActionProbability maxLeafNodes gradientAverageWindow optimizationSteps useBaseLine useMultiSplit cosineThreshold onlyUseFringeBias useDotProductScore discountFactor parametersNormConstraint stopSplittingAfter seed"<<'\n';
        return 1;
    }

    int runs = atoi(argv[1]);
    double learningRate = atof(argv[2]);
    double minActionProbability = atof(argv[3]);
    int maxLeafNodes = atoi(argv[4]);
    int gradientAverageWindow = atoi(argv[5]);
    int optimizationSteps = atoi(argv[6]);
    bool useBaseLine = atoi(argv[7]) ? true : false;
    bool useMultiSplit = atoi(argv[8]) ? true : false;
    double cosineThreshold = atof(argv[9]);
    bool onlyUseFringeBias = atoi(argv[10]) ? true : false;
    bool useDotProductScore = atoi(argv[11]) ? true : false;
    double discountFactor = atof(argv[12]);
    double parametersNormConstraint = atof(argv[13]);
    double stopSplittingAfter = atof(argv[14]);
    int seed = atoi(argv[15]);

    srand(seed);
    ComplexXOR game;
    PolicyTreeLinear policyTreeLinear(game.getNumFeatures(), game.getNumActions(), learningRate, minActionProbability, maxLeafNodes, gradientAverageWindow, optimizationSteps, useBaseLine, useMultiSplit, cosineThreshold, onlyUseFringeBias, useDotProductScore, discountFactor, parametersNormConstraint);
    double sumReward = 0;

    for(int episode=0; episode < runs; episode++)
    {
        double totalReward = 0;
        int episodeLength = 0;
        game.reset();

        while(game.isActive())
        {
            unordered_set<int> observations = game.getFeatures();
            unsigned int action = policyTreeLinear.storeObservationsAndGetAction(observations);
            double reward = game.processAction(action);
            policyTreeLinear.processAction(action, reward);
            totalReward += reward;
            episodeLength++;
        }
        
        if(episode == stopSplittingAfter)
        {
            policyTreeLinear.optimizationSteps = 500000;
        }
        
        cout<<"Episode "<<episode<<" ended with score: "<<totalReward<<"\n";
        policyTreeLinear.processEpisode();
        sumReward += totalReward;
    }
}

#include <iostream>
#include <vector>
#include <cstdlib>
#include <cassert>
#include <ctime>

#include "Pacman.h"

using namespace std;

int main(int argc, char** argv)
{
    Pacman game(4, 16);
    double totalReward = 0;
    game.reset();
    srand(1);
    
    while(game.isActive())
    {
        vector<vector<int> > screen = game.getScreen();
        unordered_set<int> features = game.getFeatures();
        cout<<"Features: ";
        
        for(auto it = features.begin(); it != features.end(); it++)
        {
            cout<<*it<<" ";
        }
        
        cout<<"\n";
        
        for(int i=0; i < screen.size(); i++)
        {
            for(int j=0; j < screen[i].size(); j++)
            {
                cout<<screen[i][j];
            }
            cout<<"\n";
        }
        
        int action;
        cin>>action;
        totalReward += game.processAction(action);
    }
    
    cout<<"Total Reward: "<<totalReward<<"\n";
    return 0;
}
#include <vector>
#include <deque>
#include <cassert>
#include <cmath>
#include <utility>
#include <cstdlib>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <functional>
#include <iostream>

#include "sample.hpp"

const double epsilon = 1e-9;
    
using namespace std;

struct pair_hash {
    inline size_t operator()(const pair<int,int> &v) const {
        return v.first*31+v.second;
    }
};

class PolicyBooster
{
public:
    int numObservations;
    int numActions;
    int numFeatures;
    int optimizationSteps;
    int gradientAveragingSteps;
    int scoreType;
    double learningRate;
    double learningStrength;
    double temperature;
    double minActionProbability;
    int currTime;
    int numEpisodes;
    bool optimizing;
    vector<vector<int> > features;
    unordered_set<int> observations;
    unordered_set<int> activeFeatures;
    unordered_set<int> activeFeaturesHistory;
    unordered_set<pair<int, int>, pair_hash> activeFeaturesAndObservationsHistory;
    vector<vector<double> > weights;
    vector<vector<double> > gradient;
    vector<vector<double> > Z;
    vector<vector<vector<double> > > fringeGradient;
    vector<vector<vector<double> > > fringeZ;
    vector<double> averageRewardAtTime;
    vector<double> actionProbabilities;

    PolicyBooster(int numObservations, int numActions, double learningRate, double minActionProbability, int optimizationSteps, int gradientAveragingSteps, int scoreType) : numObservations(numObservations), numActions(numActions), numFeatures(numObservations), learningRate(learningRate), learningStrength(0), minActionProbability(minActionProbability), temperature(1), optimizationSteps(optimizationSteps), gradientAveragingSteps(gradientAveragingSteps), scoreType(scoreType), currTime(0), numEpisodes(0), optimizing(true), weights(numActions, vector<double>(numObservations, 0)), gradient(numActions, vector<double>(numObservations, 0)), Z(numActions, vector<double>(numObservations, 0)), actionProbabilities(numActions, 0)
    {
        assert(numObservations >= 0);
        assert(numActions >= 0);
        assert(learningRate >= 0);
        assert(minActionProbability >= 0);
        assert(numActions*minActionProbability <= 1);
        assert(optimizationSteps >= 1);
        assert(gradientAveragingSteps >= 1);
        assert(scoreType >= -1);
        
        features.push_back(vector<int>(1,0));
        learningStrength += 1;

        for(int i=1; i < numObservations; i++)
        {
            vector<int> feature(2, 0);
            feature[0] = 0;
            feature[1] = i;
            features.push_back(feature);
            learningStrength += 0.5;
        }
    }

    void resetFringe()
    {
        fringeGradient = vector<vector<vector<double> > >(numActions, vector<vector<double> >(numFeatures, vector<double>(numObservations, 0)));
        fringeZ = vector<vector<vector<double> > >(numActions, vector<vector<double> >(numFeatures, vector<double>(numObservations, 0)));
    }

    void setActiveFeatures()
    {
        activeFeatures.clear();

        for(int i=0; i < features.size(); i++)
        {
            bool set = true;
            
            for(int j=0; j < features[i].size(); j++)
            {
                if(observations.find(features[i][j]) == observations.end())
                {
                    set = false;
                    break;
                }
            }

            if(set)
            {
                activeFeatures.insert(i);
            }
        }
    }

    void calculateActionProbabilities()
    {
        double normalization = 0;
        vector<double> sumParams(numActions, 0);
        
        for(int i=0 ; i < numActions; i++)
        {
            double sum = 0;
            
            for(auto it = activeFeatures.begin(); it != activeFeatures.end(); it++)
            {
                sum += weights[i][*it];
            }

            sumParams[i] = sum;
        }
        
        double maxSum = *max_element(sumParams.begin(), sumParams.end());
        
        for(int i = 0; i < numActions; i++)
        {    
            actionProbabilities[i] = exp((sumParams[i]-maxSum)/temperature);
            normalization += actionProbabilities[i];
        }
        
        for(int i = 0; i < numActions; i++)
        {    
            actionProbabilities[i] /= normalization;
        }
    }

    void updateGradient(int action, double reward)
    {
        assert(Z.size() == numActions);
        assert(gradient.size() == numActions);
        
        for(int i=0 ; i < numActions; i++)
        {
            assert(actionProbabilities[i] >= 0);
            assert(actionProbabilities[i] <= 1);
            assert(Z[i].size() == numFeatures);
            assert(gradient[i].size() == numFeatures);

            for(auto it = activeFeatures.begin(); it != activeFeatures.end(); it++)
            {
                assert(*it < numFeatures);
                double correctiveTerm = 1 + minActionProbability/((1-numActions*minActionProbability)*actionProbabilities[i]);
                Z[i][*it] += ((i==action) - actionProbabilities[i])/(temperature*correctiveTerm);
                activeFeaturesHistory.insert(*it);
            }
            
            for(auto it = activeFeaturesHistory.begin(); it != activeFeaturesHistory.end(); it++)
            {
                gradient[i][*it] += reward * Z[i][*it];
            }
        }
    }

    void updateFringeGradient(int action, double reward)
    {
        for(int i=0 ; i < numActions; i++)
        {
            assert(actionProbabilities[i] >= 0);
            assert(actionProbabilities[i] <= 1);

            for(auto it1 = activeFeatures.begin(); it1 != activeFeatures.end(); it1++)
            {
                for(auto it2 = observations.begin(); it2 != observations.end(); it2++)
                {
                    double correctiveTerm = 1 + minActionProbability/((1-numActions*minActionProbability)*actionProbabilities[i]);
                    fringeZ[i][*it1][*it2] += ((i==action)-actionProbabilities[i])/(temperature*correctiveTerm);
                    activeFeaturesAndObservationsHistory.insert(make_pair(*it1, *it2));
                }
            }
            
            for(auto it = activeFeaturesAndObservationsHistory.begin(); it != activeFeaturesAndObservationsHistory.end(); it++)
            {
                fringeGradient[i][(*it).first][(*it).second] += reward * fringeZ[i][(*it).first][(*it).second];
            }
        }
    }

    int storeObservationsAndGetAction(unordered_set<int>& observations)
    {
        this->observations = observations;
        setActiveFeatures();
        calculateActionProbabilities();
        int action = sample(actionProbabilities);
        double r = (double)rand()/RAND_MAX;
        
        if(r < minActionProbability*numActions)
        {
            action = rand()%numActions;
        }
        
        return action;
    }

    void processActionAndReward(int action, double reward)
    {
        double baseLine = 0;

        if(currTime < averageRewardAtTime.size())
        {
            baseLine = averageRewardAtTime[currTime];
        }

        updateGradient(action, reward-baseLine);
        
        if(!optimizing)
        {
            updateFringeGradient(action, reward-baseLine);
        }

        if(currTime < averageRewardAtTime.size())
        {
            averageRewardAtTime[currTime] += (reward-averageRewardAtTime[currTime])/(numEpisodes+1);
        }
        else
        {
            assert(currTime == averageRewardAtTime.size());
            averageRewardAtTime.push_back(reward/(numEpisodes+1));
        }

        currTime++;
    }

    void performGradientStep()
    {
        for(int i=0 ; i < numActions; i++)
        {
            for(int j=0 ; j < numFeatures; j++)
            {
                weights[i][j] += learningRate * gradient[i][j] / learningStrength;
            }
        }
    }

    void resetGradient()
    {
        for(int i=0 ; i < numActions; i++)
        {
            for(int j=0 ; j < numFeatures; j++)
            {
                gradient[i][j] = 0;
            }
        }
    }
    
    void clearTrace()
    {
        for(int i=0 ; i < numActions; i++)
        {
            for(int j=0 ; j < numFeatures; j++)
            {
                Z[i][j] = 0;
            }
        }
    }
    
    void clearFringeTrace()
    {
        for(int i=0; i < numActions; i++)
        {
            for(int j=0; j < numFeatures; j++)
            {
                for(int k=0; k < numObservations; k++)
                {
                    assert(j < fringeZ[i].size());
                    assert(k < fringeZ[i][j].size());
                    fringeZ[i][j][k] = 0;
                }
            }
        }
    }
    
    void promoteFeature()
    {
        set<vector<int> > existingFeatures(features.begin(), features.end());
        vector<pair<double, vector<int> > > newFeaturesWithScores;

        for(int i=0; i < numFeatures; i++)
        {
            double parentScore = 0;
            double bestScore = 0;
            int bestObservation = -1;
            
            for(int k=0; k < numActions; k++)
            {
                if(scoreType == 0)
                {
                    if(fabs(gradient[k][i]) > parentScore)
                    {
                        parentScore = fabs(gradient[k][i]);
                    }
                }
                else
                {
                    parentScore += fabs(pow(gradient[k][i], scoreType));
                }
            }
            for(int j=0; j < numObservations; j++)
            {
                double score = 0;
                
                for(int k=0; k < numActions; k++)
                {
                    if(scoreType == -1)
                    {
                        score = parentScore + (double)rand()/RAND_MAX;
                    }
                    if(scoreType == 0)
                    {
                        if(fabs(fringeGradient[k][i][j]) > score)
                        {
                            score = fabs(fringeGradient[k][i][j]);
                        }
                        else if(fabs(gradient[k][i]-fringeGradient[k][i][j]) > score)
                        {
                            score = fabs(gradient[k][i]-fringeGradient[k][i][j]);
                        }
                    }
                    else
                    {
                        score += fabs(pow(fringeGradient[k][i][j], scoreType));
                        score += fabs(pow(gradient[k][i]-fringeGradient[k][i][j], scoreType));
                    }
                }
                
                if(score > bestScore)
                {
                    bestScore = score;
                    bestObservation = j;
                }
            }
            
            if((bestObservation != -1) && (bestScore > parentScore))
            {
                vector<int> feature = features[i];
                feature.push_back(bestObservation);
                sort(feature.begin(), feature.end());
                
                if(existingFeatures.find(feature) == existingFeatures.end())
                {
                    newFeaturesWithScores.push_back(make_pair(bestScore-parentScore, feature));
                    existingFeatures.insert(feature);
                }
            }
        }
        
        sort(newFeaturesWithScores.begin(), newFeaturesWithScores.end(), greater<pair<double, vector<int> > >());

        int featuresToAdd = newFeaturesWithScores.size();
        featuresToAdd = min(featuresToAdd, numObservations);

        for(int i=0; i < featuresToAdd; i++)
        {
            numFeatures++;
            
            features.push_back(newFeaturesWithScores[i].second);
            
            cout<<"Promoting Feature: ";
            
            for(int j=0; j < newFeaturesWithScores[i].second.size(); j++)
            {
                cout<<newFeaturesWithScores[i].second[j]<<" ";
            }
            
            cout<<"\n";

            for(int j=0; j < numActions; j++)
            {
                weights[j].push_back(0);
                gradient[j].push_back(0);
                Z[j].push_back(0);
            }

            learningStrength += pow(2, 1-(int)newFeaturesWithScores[i].second.size());
        }
    }

    void processEpisode()
    {
        numEpisodes++;
        clearTrace();
        activeFeaturesHistory.clear();
        activeFeaturesAndObservationsHistory.clear();
        
        if(optimizing)
        {
            performGradientStep();
            resetGradient();
            
            if((numEpisodes+gradientAveragingSteps)%(optimizationSteps+gradientAveragingSteps) == 0)
            {
                optimizing = false;
                resetFringe();
            }
        }
        else
        {
            clearFringeTrace();
            
            if((numEpisodes-optimizationSteps)%gradientAveragingSteps == 0)
            {
                promoteFeature();
                resetGradient();
                optimizing = true;
            }
        }

        currTime = 0;
    }
};



#include <unordered_set>
#include <iostream>

#include "Asterix.h"

int main(int argc, char** argv) 
{
    if(argc < 5)
    {
            std::cerr << "Usage: " << argv[0] << "height width restTime runs" << std::endl;
            return 1;
    }

    int height = atoi(argv[1]);
    int width = atoi(argv[2]);
    int restTime = atoi(argv[3]);
    int runs = atoi(argv[4]);
    double sumReward = 0;        

    Asterix game(height, width, restTime, true, 2, 0.1, 10);

    for(int episode=0; episode<runs; episode++) 
    {
        game.reset();
        double totalReward = 0;

        while(game.isActive()) 
        {
            std::unordered_set<int> features =  game.getFeatures();
            if((features.find(21) != features.end() || features.find(23) != features.end()) && (features.find(31) == features.end()))
                totalReward += game.processAction(1+(rand()%2)*2);
            else
                totalReward += game.processAction(0);
        }
        sumReward += totalReward;
        std::cout<<"Average score: "<<(double)sumReward/(episode+1)<<"\n";
    }

    return 0;
}
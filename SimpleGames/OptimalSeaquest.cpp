#include <unordered_set>
#include <iostream>
#include <limits>

#include "Seaquest.h"

using namespace std;

int main(int argc, char** argv) 
{
    if(argc < 2)
    {
            cerr << "Usage: " << argv[0] << "runs" << endl;
            return 1;
    }
    
    int runs = atoi(argv[1]);
    double sumReward = 0;        
    double sumEpisodeLength = 0;
    
    Seaquest game(4, 16);

    for(int episode=0; episode<runs; episode++) 
    {
        game.reset();
        double totalReward = 0;
        int episodeLength = 0;
        
        while(game.isActive()) 
        {
            unordered_set<int> features = game.getFeatures();
            vector<vector<int> > screen = game.getScreen();
            
            int action = 0;

            if(features.find(1) != features.end())
            {
                int minLogY = numeric_limits<int>::max();
                
                for(int i=201; i <= 300; i++)
                {
                    if(features.find(i) != features.end())
                    {
                        int quadrant = (i-201)/25;
                        int logY = ((i-201)%25)/5;
                        int logX = ((i-201)%25)%5;
                        
                        switch(quadrant)
                        {
                            case 0:
                            case 1:
                                if(logY < minLogY)
                                {
                                    if(logY == 0)
                                    {
                                        action = 0;
                                    }
                                    else
                                    {
                                        action = 1;
                                    }
                                    minLogY = logY;
                                }
                                break;
                            case 2:
                            case 3:
                                if(logY < minLogY)
                                {
                                    if(logY == 0)
                                    {
                                        action = 0;
                                    }
                                    else
                                    {
                                        action = 3;
                                    }
                                    minLogY = logY;
                                }
                                break;
                        }
                    }
                }
            }
            else
            {
                for(int i=401; i <= 500; i++)
                {
                    if(features.find(i) != features.end())
                    {
                        int quadrant = (i-401)/25;
                        int logY = ((i-401)%25)/5;
                        int logX = ((i-401)%25)%5;
                        
                        switch(quadrant)
                        {
                            case 0:
                                if(logY < logX)
                                {
                                    action = 4;
                                }
                                else
                                {
                                    action = 1;
                                }
                                break;
                            case 1:
                                if(logY < logX)
                                {
                                    action = 2;
                                }
                                else
                                {
                                    action = 1;
                                }
                                break;
                            case 2:
                                if(logY < logX)
                                {
                                    action = 4;
                                }
                                else
                                {
                                    action = 3;
                                }
                                break;
                            case 3:
                                if(logY < logX)
                                {
                                    action = 2;
                                }
                                else
                                {
                                    action = 3;
                                }
                                break;
                        }
                    }
                }
            }
            
            switch(action)
            {
                case 0:
                    if( (features.find(326) != features.end()) ||
                        (features.find(301) != features.end()) )
                    {
                        action = 1 + 2*(rand()%2);
                    }
                    break;
                case 1:
                    if( (features.find(307) != features.end()) ||
                        (features.find(332) != features.end()) )
                    {
                        action = 0;
                    }
                    break;
                case 2:
                    if(features.find(326) != features.end())
                    {
                        action = 1 + 2*(rand()%2);
                    }
                    break;
                case 3:
                    if( (features.find(357) != features.end()) ||
                        (features.find(382) != features.end()) )
                    {
                        action = 0;
                    }
                    break;
                case 4:
                    if(features.find(301) != features.end())
                    {
                        action = 1 + 2*(rand()%2);
                    }
                    break;
            }
            
            totalReward += game.processAction(action);
            episodeLength++;
        }
        sumReward += totalReward;
        sumEpisodeLength += episodeLength;
        cout<<"Average Episode Length: "<<(double)sumEpisodeLength/(episode+1)<<"\n";
        cout<<"Average Score: "<<(double)sumReward/(episode+1)<<"\n";
    }

    return 0;
}
#include <iostream>
#include <vector>
#include <unordered_set>
#include <cstdlib>
#include <ctime>

#include "policy_tree_linear.hpp"
#include "Invaders.h"

using namespace std;

int main(int argc, char** argv)
{
    if(argc < 14)
    {
        cerr<<"Usage: "<<argv[0]<<"runs featureType proximity learningRate minActionProbability maxLeafNodes gradientAverageWindow optimizationSteps useBaseLine onlyUseFringeBias useDotProductScore discountFactor seed"<<'\n';
        return 1;
    }

    int runs = atoi(argv[1]);
    int featureType = atoi(argv[2]);
    int proximity = atoi(argv[3]);
    double learningRate = atof(argv[4]);
    double minActionProbability = atof(argv[5]);
    int maxLeafNodes = atoi(argv[6]);
    int gradientAverageWindow = atoi(argv[7]);
    int optimizationSteps = atoi(argv[8]);
    bool useBaseLine = atoi(argv[9]) ? true : false;
    bool onlyUseFringeBias = atoi(argv[10]) ? true : false;
    int scoreType = atoi(argv[11]);
    double discountFactor = atof(argv[12]);
    int seed = atoi(argv[13]);

    generator.seed(seed);
    srand(seed);
    Invaders game(featureType, proximity);
    PolicyTreeLinear policyTreeLinear(game.getNumFeatures(), game.getNumActions(), learningRate, minActionProbability, maxLeafNodes, gradientAverageWindow, optimizationSteps, useBaseLine, onlyUseFringeBias, scoreType, discountFactor);

    double sumReward = 0;

    for(int episode=0; episode < runs; episode++)
    {
        double totalReward = 0;
        int episodeLength = 0;
        game.reset();

        while(game.isActive())
        {
            unordered_set<int> observations = game.getFeatures();
            unsigned int action = policyTreeLinear.storeObservationsAndGetAction(observations);
            double reward = game.processAction(action);
            policyTreeLinear.processAction(action, reward);
            totalReward += reward;
            episodeLength++;
        }

        cout<<"Episode "<<episode<<" ended at time "<<time(NULL)<<" with score: "<<totalReward<<"\n";
        policyTreeLinear.processEpisode();
        sumReward += totalReward;
    }
}

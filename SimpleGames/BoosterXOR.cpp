#include <iostream>
#include <vector>
#include <cstdlib>
#include <cassert>

#include "policy_booster.hpp"
#include "XOR.h"

int main(int argc, char** argv)
{
    if(argc < 8)
    {
        std::cerr<<"Usage: "<<argv[0]<<"runs learningRate temperature optimizationSteps gradientAveragingSteps expand seed"<<'\n';
        return 1;
    }

    int runs = atoi(argv[1]);
    double learningRate = atof(argv[2]);
    double temperature = atof(argv[3]);
    int optimizationSteps = atoi(argv[4]);
    int gradientAveragingSteps = atoi(argv[5]);
    bool expand = atoi(argv[6]) > 0 ? true : false;
    int seed = atoi(argv[7]);

    srand(seed);
    XOR game;
    game.reset();
    double sumTotalReward = 0;
    int numFeatures = game.getNumFeatures();

    if(expand)
    {
        numFeatures = numFeatures + numFeatures*(numFeatures-1) /2;
    }

    PolicyBooster policy(numFeatures, game.getNumActions(), learningRate, temperature, optimizationSteps, gradientAveragingSteps, 1024);

    for(int i=0; i < runs; i++)
    {
        game.reset();
        double totalReward = 0;

        while(game.isActive())
        {
            std::unordered_set<int> observations = game.getFeatures();
            
            if(expand)
            {
                observations = expandFeatures(observations, game.getNumFeatures());
            }
            
            int action = policy.storeObservationsAndGetAction(observations);
            double reward = game.processAction(action);
            policy.processActionAndReward(action, reward);
            totalReward += reward;
        }

        policy.processEpisode();
        sumTotalReward += totalReward;
        std::cout<<"Episode "<<i<<" ended with score: "<<totalReward<<"\n";
    }

    return 0;
}

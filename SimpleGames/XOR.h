#include <unordered_set>

using namespace std;

class XOR
{
public:
    static const int numActions = 2;
    static const int numFeatures = 11;
    static const int episodeLength = 256;
    
    bool active;
    unordered_set<int> features;
    int currTime;
    
    XOR() : active(false)
    {
    }
    
    void reset()
    {
        currTime = 0;
        active = true;
        resetFeatures();
    }
    
    void resetFeatures()
    {
        features = unordered_set<int>();
        features.insert(0);
        
        if(rand()%2)
        {
            features.insert(1);
        }
        
        if(rand()%2)
        {
            features.insert(2);
        }
        
        for(int i=0; i < 8; i++)
        {
            if(rand()%2)
            {
                features.insert(3+i);
            }
        }
        
    }
    
    bool isActive()
    {
        return active;
    }
    
    double processAction(int action)
    {
        assert(action < numActions);
        
        if(!active)
        {
            return 0;
        }
        
        int desiredAction = (features.find(1) != features.end()) ^ (features.find(2) != features.end());
        double reward = 0;
        
        if(action == desiredAction)
        {
            reward = 1;
        }
        else
        {
            reward = -1;
        }
        
        currTime++;
        
        if(currTime == episodeLength)
        {
            active = false;
        }
        
        resetFeatures();
        return reward;
    }
    
    unordered_set<int> getFeatures()
    {
        return features;
    }
    
    int getNumActions()
    {
        return numActions;
    }
    
    int getNumFeatures()
    {
        return numFeatures;
    }
};

unordered_set<int> expandFeatures(unordered_set<int>& features, int size)
{
    unordered_set<int> expandedFeatures = features;
    for(auto it1 = features.begin(); it1 != features.end(); it1++)
    {
        auto it2 = it1;
        it2++;
        
        for(; it2 != features.end(); it2++)
        {
            int minF = min(*it1, *it2);
            int maxF = max(*it1, *it2);
            expandedFeatures.insert(size + size*minF - minF*(minF+1)/2 + maxF - minF - 1);
        }
    }
    
    return expandedFeatures;
}
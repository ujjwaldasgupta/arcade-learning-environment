#include <iostream>
#include <vector>
#include <cstdlib>
#include <cassert>

#include "policy_gradient_natural.hpp"
#include "Asterix.h"

int main(int argc, char** argv)
{
    if(argc < 10)
    {
        std::cerr<<"Usage: "<<argv[0]<<"runs learningRate numTrajectories maxEpisodeLength timeVariant regularizationConstant featureType proximity seed"<<'\n';
        return 1;
    }
    
    int runs = atoi(argv[1]);
    double learningRate = atof(argv[2]);
    int numTrajectories = atoi(argv[3]);
    int maxEpisodeLength = atoi(argv[4]);
    bool timeVariant = atoi(argv[5]) > 0 ? true : false;
    int featureType = atoi(argv[6]);
    double regularizationConstant = atof(argv[7]);
    int proximity = atoi(argv[8]);
    int seed = atoi(argv[9]);

    srand(seed);
    Asterix game(featureType, proximity);
    game.reset();
    double sumTotalReward = 0;
    int numFeatures = game.getNumFeatures();
    
    PolicyGradientNatural policy(numFeatures, game.getNumActions(), learningRate, numTrajectories, maxEpisodeLength, timeVariant, regularizationConstant);

    for(int i=0; i < runs; i++)
    {
        game.reset();
        double totalReward = 0;
        int episodeLength = 0;
        
        while(game.isActive() && episodeLength < maxEpisodeLength)
        {
            std::unordered_set<int> observations = game.getFeatures();
            int action = policy.getAction(observations);
            double reward = game.processAction(action);
            policy.updateTrace(action, reward, observations);
            totalReward += reward;
            episodeLength++;
        }
        
        policy.processEpisode();
        sumTotalReward += totalReward;
        std::cout<<"Episode "<<i<<" ended with score: "<<totalReward<<"\n";
    }
    
    return 0;
}
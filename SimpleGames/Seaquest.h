#include <vector>
#include <cstdlib>
#include <cassert>
#include <unordered_set>
#include <cmath>

using namespace std;

class Seaquest
{
    vector<vector<int> > screen;
    static const int numColours = 6;
    enum colour
    {
        COLOUR_BLANK = 0,
        COLOUR_AGENT,
        COLOUR_FULL_AGENT,
        COLOUR_DIVER,
        COLOUR_FISH,
        COLOUR_DROP_POINT
    };
    static const int numActions = 5;
    enum actions 
    {
        ACTION_NULL = 0,
        ACTION_UP,
        ACTION_RIGHT,
        ACTION_DOWN,
        ACTION_LEFT
    };
    static const int maxDivers = 1;
    static const int width = 16;
    static const int height = 16;
    static const int maxEpisodeLength = 256;
    static const int NPCprobability = 25;
    int currTime;
    int featureType;
    int proximity;
    bool active;
    int diversCollected;
    pair<int, int> dropPoint;
    pair<int, int> agentLocation;
    vector<pair<pair<int, int >, bool> > diverLocations;
    vector<pair<pair<int, int >, bool> > fishLocations;

    public:
    Seaquest(int featureType, int proximity) : featureType(featureType), proximity(proximity), active(false), diversCollected(0), currTime(0), dropPoint(make_pair(0,0)), agentLocation(make_pair(0,0)), screen(height, vector<int>(width, 0))
    {
    }

    bool isActive()
    {
        return active;
    }
        
    vector<vector<int> > getScreen()
    {
        return screen;
    }
    
    void reset()
    {
        screen = vector<vector<int> >(height, vector<int>(width, 0));
        agentLocation.first = 0;
        agentLocation.second = 1 + rand()%(width-2);
        diverLocations.clear();
        fishLocations.clear();
        diversCollected = 0;
        currTime = 0;
        dropPoint = make_pair(1+rand()%(height-1), 1+rand()%(width-2));
        active = true;
        generateScreen();
    }

    void fillFromList(vector<pair<pair<int, int>, bool> > list, int type)
    {
        for(int i=0; i < list.size(); i++)
        {
            assert(list[i].first.first < height);
            assert(list[i].first.second < width);
            assert(list[i].first.first >= 0);
            assert(list[i].first.second >= 0);
            screen[list[i].first.first][list[i].first.second] = type;
        }
    }
    
    void generateScreen()
    {
        screen = vector<vector<int> >(height, vector<int>(width, 0));
        fillFromList(diverLocations, COLOUR_DIVER);
        fillFromList(fishLocations, COLOUR_FISH);
        assert(dropPoint.first > 0);
        assert(dropPoint.first < width);
        assert(dropPoint.second > 0);
        assert(dropPoint.second < width-1);
        screen[dropPoint.first][dropPoint.second] = COLOUR_DROP_POINT;
        assert(agentLocation.first < height);
        assert(agentLocation.first >= 0);
        assert(agentLocation.second < width-1);
        assert(agentLocation.second > 0);
        
        if(diversCollected < maxDivers)
        {
            screen[agentLocation.first][agentLocation.second] = COLOUR_AGENT;
        }
        else
        {
            screen[agentLocation.first][agentLocation.second] = COLOUR_FULL_AGENT;
        }
    }
    
    
    void generateNPC()
    {
        bool type = rand()%2;
        bool dir = rand()%2;
        int location = 1 + rand()%(height-1);
        
        if(type)
        {
            fishLocations.push_back(make_pair(make_pair(location, (!dir)*(width-1)), dir));
        }
        else
        {
            diverLocations.push_back(make_pair(make_pair(location, (!dir)*(width-1)), dir));
        }
    }
    
    vector<pair<pair<int, int>, bool> > pruneList(vector<pair<pair<int, int>, bool> >& list)
    {
        vector<pair<pair<int, int>, bool> > newList;
        
        for(int i=0; i < list.size(); i++)
        {
            if(list[i].first.second != -1)
            {
                assert(list[i].first.first > 0);
                assert(list[i].first.first < height);
                assert(list[i].first.second >= 0);
                assert(list[i].first.second < width);
                newList.push_back(list[i]);
            }
        }
        
        return newList;
    }
    
    double processAction(int action)
    {
        if(!active)
        {
            return 0;
        }
        
        assert(agentLocation.first >= 0);
        assert(agentLocation.first < height);
        assert(agentLocation.second > 0);
        assert(agentLocation.second < width - 1);
        
        switch(action)
        {
            case ACTION_NULL:
                break;
            case ACTION_UP:
                if(agentLocation.first > 0)
                    agentLocation.first--;
                break;
            case ACTION_RIGHT:
                if(agentLocation.second < width-2)
                    agentLocation.second++;
                break;
            case ACTION_DOWN:
                if(agentLocation.first < height-1)
                    agentLocation.first++;
                break;
            case ACTION_LEFT:
                if(agentLocation.second > 1)
                    agentLocation.second--;
                break;
            default:
                assert(0);
        }
        
        double reward = 0;
        
        if(agentLocation.first == dropPoint.first && agentLocation.second == dropPoint.second)
        {
            if(diversCollected == maxDivers)
            {
                reward = 1;
                diversCollected = 0;
                dropPoint.first = 1 + rand()%(height-1);
                dropPoint.second = 1 + rand()%(width-2);
            }
            else
            {
                active = false;
            }
        }

        for(int i=0; i < diverLocations.size(); i++)
        {
            assert(diverLocations[i].first.first > 0);
            assert(diverLocations[i].first.first < height);
            assert(diverLocations[i].first.second >= 0);
            assert(diverLocations[i].first.second < width);
            
            if(diverLocations[i].second)
            {
                diverLocations[i].first.second++;
                
                if(diverLocations[i].first.second >= width)
                {
                    diverLocations[i].first.second = -1;
                }
                else
                {
                    if( (diverLocations[i].first.first == agentLocation.first && diverLocations[i].first.second == agentLocation.second) ||
                        action == ACTION_LEFT && (diverLocations[i].first.first == agentLocation.first && diverLocations[i].first.second-1 == agentLocation.second)
                    )
                    {
                        if(diversCollected < maxDivers)
                        {
                            diversCollected++;
                            diverLocations[i].first.second = -1;
                        }
                    }
                }
            }
            else
            {
                diverLocations[i].first.second--;
                
                if(diverLocations[i].first.second < 0)
                {
                    diverLocations[i].first.second = -1;
                }
                else
                {
                    if( (diverLocations[i].first.first == agentLocation.first && diverLocations[i].first.second == agentLocation.second) ||
                        action == ACTION_RIGHT && (diverLocations[i].first.first == agentLocation.first && diverLocations[i].first.second == agentLocation.second-1)
                    )
                    {
                        if(diversCollected < maxDivers)
                        {
                            diversCollected++;
                            diverLocations[i].first.second = -1;
                        }
                    }
                }
            }
        }
        
        for(int i=0; i < fishLocations.size(); i++)
        {
            assert(fishLocations[i].first.first > 0);
            assert(fishLocations[i].first.first < height);
            assert(fishLocations[i].first.second >= 0);
            assert(fishLocations[i].first.second < width);
            
            if(fishLocations[i].second)
            {
                fishLocations[i].first.second++;
                
                if(fishLocations[i].first.second >= width)
                {
                    fishLocations[i].first.second = -1;
                }
                else
                {
                    if( (fishLocations[i].first.first == agentLocation.first && fishLocations[i].first.second == agentLocation.second) ||
                        action == ACTION_LEFT && (fishLocations[i].first.first == agentLocation.first && fishLocations[i].first.second-1 == agentLocation.second)
                    )
                    {
                        fishLocations[i].first.second = -1;
                        active = false;
                    }
                }
            }
            else
            {
                fishLocations[i].first.second--;
                
                if(fishLocations[i].first.second < 0)
                {
                    fishLocations[i].first.second = -1;
                }
                else
                {
                    if( (fishLocations[i].first.first == agentLocation.first && fishLocations[i].first.second == agentLocation.second) ||
                        action == ACTION_RIGHT && (fishLocations[i].first.first == agentLocation.first && fishLocations[i].first.second == agentLocation.second-1)
                    )
                    {
                        fishLocations[i].first.second = -1;
                        active = false;
                    }
                }
            }
        }
        
        diverLocations = pruneList(diverLocations);
        fishLocations = pruneList(fishLocations);
        currTime++;
        
        if(currTime == maxEpisodeLength)
        {
            active = false;
        }
        
        if(rand()%100 < NPCprobability)
        {
            generateNPC();
        }
        
        generateScreen();
        return reward;
    }
    
    int getNumActions()
    {
        return numActions;
    }
    
    unordered_set<int> getFeatures()
    {
        if(featureType == 4)
        {
            return getLogarithmicRelativeFeatures();
        }
        else if(featureType == 3)
        {
            return getPairwiseLogarithmicRelativeFeatures();
        }
        else if(featureType == 2)
        {
            return getPairwiseRelativeFeatures();
        }
        else if(featureType == 1)
        {
            return getRelativeFeatures();
        }
        else if(featureType == 0)
        {
            return getAbsoluteFeatures();
        }
        else
        {
            assert(0);
        }
    }
    
    int getNumFeatures()
    {
        if(featureType == 4)
        {
            return 1 + (numColours-1) * 4 * (floor(log2(height-1))+2) * (floor(log2(width-1))+2);
        }
        else if(featureType == 3)
        {
            return 1 + height*width*(numColours-1) + 4*(floor(log2(height-1))+2)*(floor(log2(width-1))+2)*(numColours-1)*(numColours)/2;
        }
        else if(featureType == 2)
        {
            return 1 + (2*proximity-1)*(2*proximity-1)*(numColours-1)*(numColours)/2;
        }
        else if(featureType == 1)
        {
            return 1 + (2*proximity-1)*(2*proximity-1)*(numColours-1);
        }
        else if(featureType == 0)
        {
            return 1 + height*width*(numColours-1);
        }
        else
        {
            assert(0);
        }
    }
    
    unordered_set<int> getAbsoluteFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i=0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                if(screen[i][j] > 0)
                {
                    features.insert(1 + (screen[i][j]-1)*height*width + i*width + j);
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = max(0, agentLocation.first-proximity+1); i < min(int(height), agentLocation.first+proximity); i++)
        {            
            for(int j = max(0, agentLocation.second-proximity+1); j < min(int(width), agentLocation.second+proximity); j++)
            {
                if(screen[i][j] > 0)
                {
                    features.insert(1 + (screen[i][j]-1)*(2*proximity-1)*(2*proximity-1) + (i-agentLocation.first+proximity-1)*(2*proximity-1) + (j-agentLocation.second+proximity-1));
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getPairwiseRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = 0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                for(int k = max(0, i-proximity+1); k < min(int(height), i+proximity); k++)
                {              
                    for(int l = max(0, j-proximity+1); l < min(int(width), j+proximity); l++)
                    {
                        if(screen[i][j] == 0 || screen[i][j] > screen[k][l])
                        {
                            continue;
                        }
                        
                        int featureIndex = 1 + ((screen[i][j]-1)*(numColours-1)-((screen[i][j]-1)*(screen[i][j]-2))/2+screen[k][l]-screen[i][j])*(2*proximity-1)*(2*proximity-1) + (k-i+proximity-1)*(2*proximity-1) + (l-j+proximity-1);
                        
                        if(features.find(featureIndex) == features.end())
                        {
                            features.insert(featureIndex);
                        }
                    }
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getPairwiseLogarithmicRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = 0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                for(int k = 0; k < height; k++)
                {              
                    for(int l = 0; l < width; l++)
                    {
                        if(screen[i][j] == 0 || screen[i][j] > screen[k][l])
                        {
                            continue;
                        }
                        
                        int quadrant = (k>i)*2 + (l>j);
                        int logX;
                        int logY;
                        
                        if(k==i)
                        {
                            logX = 0;
                        }
                        else
                        {
                            logX = floor(log2(abs(k-i)))+1;
                        }
                        
                        if(l==j)
                        {
                            logY = 0;
                        }
                        else
                        {
                            logY = floor(log2(abs(l-j)))+1;
                        }
                        
                        int featureIndex = 1 + height*width*(numColours-1) + ((screen[i][j]-1)*(numColours-1)-((screen[i][j]-1)*(screen[i][j]-2))/2+screen[k][l]-screen[i][j])*4*(floor(log2(height-1))+2)*(floor(log2(width-1))+2) + quadrant*(floor(log2(height-1))+2)*(floor(log2(width-1))+2) + logX*(floor(log2(width-1))+2) + logY;
                        
                        if(features.find(featureIndex) == features.end())
                        {
                            features.insert(featureIndex);
                        }
                    }
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getLogarithmicRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = max(0, agentLocation.first-proximity+1); i < min(int(height), agentLocation.first+proximity); i++)
        {            
            for(int j = max(0, agentLocation.second-proximity+1); j < min(int(width), agentLocation.second+proximity); j++)
            {
                if(screen[i][j] > 0)
                {
                    int quadrant = (i>agentLocation.first)*2 + (j>agentLocation.second);
                    int relI = abs(i-agentLocation.first);
                    int relJ = abs(j-agentLocation.second);
                    int logI;
                    int logJ;
                    
                    if(relI == 0)
                    {
                        logI = 0;
                    }
                    else
                    {
                        logI = 1+floor(log2(relI));
                    }
                    
                    if(relJ == 0)
                    {
                        logJ = 0;
                    }
                    else
                    {
                        logJ = 1+floor(log2(relJ));
                    }
                    
                    int featureIndex = ( (screen[i][j]-1) * 4 * (floor(log2(height-1))+2) * (floor(log2(width-1))+2) +
                                         quadrant * (floor(log2(height-1))+2) * (floor(log2(width-1))+2) +
                                         logI * (floor(log2(width-1))+2) +
                                         logJ +
                                         1
                                       );
                    
                    assert(featureIndex >= 0);
                    assert(featureIndex < (numColours-1) * 4 * (floor(log2(height-1))+2) * (floor(log2(width-1))+2) + 1);
                    
                    if(features.find(featureIndex) == features.end())
                    {
                        features.insert(featureIndex);
                    }
                }
            }
        }
        
        return features;
    }
};

unordered_set<int> expandFeatures(unordered_set<int>& features, int size)
{
    unordered_set<int> expandedFeatures = features;
    for(auto it1 = features.begin(); it1 != features.end(); it1++)
    {
        auto it2 = it1;
        it2++;
        
        for(; it2 != features.end(); it2++)
        {
            int minF = min(*it1, *it2);
            int maxF = max(*it1, *it2);
            expandedFeatures.insert(size + size*minF - minF*(minF+1)/2 + maxF - minF - 1);
        }
    }
    
    return expandedFeatures;
}
#include <vector>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <unordered_set>

using namespace std;

class Pacman
{
    static const int height = 16;
    static const int width = 16;
    int currTime;
    int featureType;
    int proximity;
    bool active;
    static const int numColours = 5;
    static const int numActions = 5;
    static const int numMonsters = 3;
    static const int powerUpTime = 16;
    static const int maxEpisodeLength = 256;
    
    pair<int, int> agentLocation;
    vector<pair<int, int> > monsterLocations;
    vector<pair<int, int> > powerUpLocations;
    int powerUpTimeRemaining;
    int monstersKilled;
    
    vector<vector<int> > screen;
    
    /* Possible screen values:
     * 0: Blank
     * 1: Agent
     * 2: Monster
     * 3: Power Up
     */
    
    enum colour
    {
        COLOUR_BLANK = 0,
        COLOUR_AGENT,
        COLOUR_POWERED_AGENT,
        COLOUR_MONSTER,
        COLOUR_POWER_UP,
    };
    
    /* Possible actions:
     * 0: Do nothing
     * 1: Up
     * 2: Right
     * 3: Down
     * 4: Left
     */
    
    enum action
    {
        ACTION_NOOP = 0,
        ACTION_UP,
        ACTION_RIGHT,
        ACTION_DOWN,
        ACTION_LEFT,
    };
    
    public:
    Pacman(int featureType, int proximity) : featureType(featureType), proximity(proximity), active(false), currTime(0), agentLocation(make_pair(0,0)), monsterLocations(numMonsters, make_pair(0,0)), monstersKilled(0), powerUpLocations(numMonsters, make_pair(0,0)), powerUpTimeRemaining(0), screen(height, vector<int>(width, 0))
    {
    }
    
    bool isActive()
    {
        return active;
    }
    
    vector<vector<int> > getScreen()
    {
        return screen;
    }
    
    void reset()
    {
        currTime = 0;
        agentLocation = make_pair(rand()%height, rand()%width);
        powerUpTimeRemaining = 0;
        monstersKilled = 0;
        active = true;
        
        for(int i=0; i < numMonsters; i++)
        {
            monsterLocations[i] = make_pair(rand()%(height-1), rand()%(width-1));
            
            if(monsterLocations[i].first >= agentLocation.first)
            {
                monsterLocations[i].first++;
            }
            
            if(monsterLocations[i].second >= agentLocation.second)
            {
                monsterLocations[i].second++;
            }
            
            powerUpLocations[i] = make_pair(rand()%(height-1), rand()%(width-1));
            
            if(powerUpLocations[i].first >= agentLocation.first)
            {
                powerUpLocations[i].first++;
            }
            
            if(powerUpLocations[i].second >= agentLocation.second)
            {
                powerUpLocations[i].second++;
            }
        }
        
        generateScreen();
    }
    
    void fillFromList(vector<pair<int, int> > list, int type)
    {
        for(int i=0; i < list.size(); i++)
        {
            if((list[i].first < height) && (list[i].second < width) && (list[i].first >= 0) && (list[i].second >= 0))
            {
                screen[list[i].first][list[i].second] = type;
            }
        }
    }
    
    void generateScreen()
    {
        screen = vector<vector<int> >(height, vector<int>(width, 0));
                
        fillFromList(powerUpLocations, COLOUR_POWER_UP);
        fillFromList(monsterLocations, COLOUR_MONSTER);
        
        assert(agentLocation.first >= 0);
        assert(agentLocation.first < height);
        assert(agentLocation.second >= 0);
        assert(agentLocation.second < width);
        if(powerUpTimeRemaining > 0)
        {
            screen[agentLocation.first][agentLocation.second] = COLOUR_POWERED_AGENT;
        }
        else
        {
            screen[agentLocation.first][agentLocation.second] = COLOUR_AGENT;
        }
    }

    double processAction(int action)
    {        
        if(!active)
            return 0;
        
        vector<int> monsterDirections(numMonsters, ACTION_NOOP);
        vector<vector<bool> > occupied(height, vector<bool>(width, false));
        
        if(powerUpTimeRemaining == 0)
        {
            for(int i=0; i < numMonsters; i++)
            {
                if(monsterLocations[i] == make_pair(-1,-1))
                {
                    continue;
                }
                
                if(abs(monsterLocations[i].first-agentLocation.first) > abs(monsterLocations[i].second-agentLocation.second))
                {
                    if(monsterLocations[i].first > agentLocation.first)
                    {
                        if(!occupied[monsterLocations[i].first-1][monsterLocations[i].second])
                        {
                            monsterLocations[i].first--;
                            monsterDirections[i] = ACTION_UP;
                        }
                    }
                    else
                    {
                        if(!occupied[monsterLocations[i].first+1][monsterLocations[i].second])
                        {
                            monsterLocations[i].first++;
                            monsterDirections[i] = ACTION_DOWN;
                        }
                    }
                }
                else
                {
                    if(monsterLocations[i].second > agentLocation.second)
                    {
                        if(!occupied[monsterLocations[i].first][monsterLocations[i].second-1])
                        {
                            monsterLocations[i].second--;
                            monsterDirections[i] = ACTION_LEFT;
                        }
                    }
                    else
                    {
                        if(!occupied[monsterLocations[i].first][monsterLocations[i].second+1])
                        {
                            monsterLocations[i].second++;
                            monsterDirections[i] = ACTION_RIGHT;
                        }
                    }
                }
                
                occupied[monsterLocations[i].first][monsterLocations[i].second] = true;
            }
        }
        switch(action)
        {
            case ACTION_NOOP:
                break;
                
            case ACTION_UP:
                if(agentLocation.first > 0)
                {
                    agentLocation.first--;
                }
                break;
            case ACTION_RIGHT:
                if(agentLocation.second < width-1)
                {
                    agentLocation.second++;
                }
                break;                
            case ACTION_DOWN:
                if(agentLocation.first < height-1)
                {
                    agentLocation.first++;
                }
                break;
            case ACTION_LEFT:
                if(agentLocation.second > 0)
                {
                    agentLocation.second--;
                }
                break;
            default:
                assert(0);
        }
        
        int reward = 0;
        
        /* Check if powerUp is hit */
        for(int i=0; i < numMonsters; i++)
        {
            if(agentLocation == powerUpLocations[i])
            {
                powerUpLocations[i] = make_pair(-1,-1);
                powerUpTimeRemaining = powerUpTime;
            }
        }
        
        /* Check if monsters are hit */
        for(int i=0; i < numMonsters; i++)
        {
            if(monsterLocations[i] == make_pair(-1,-1))
            {
                continue;
            }
            
            if( (agentLocation == monsterLocations[i]) ||
                ( (action == ACTION_RIGHT) && (monsterDirections[i] == ACTION_LEFT) && (agentLocation.first == monsterLocations[i].first) && (agentLocation.second == monsterLocations[i].second+1)) ||
                ( (action == ACTION_LEFT) && (monsterDirections[i] == ACTION_RIGHT) && (agentLocation.first == monsterLocations[i].first) && (agentLocation.second == monsterLocations[i].second-1)) ||
                ( (action == ACTION_DOWN) && (monsterDirections[i] == ACTION_UP) && (agentLocation.first == monsterLocations[i].first+1) && (agentLocation.second == monsterLocations[i].second)) ||
                ( (action == ACTION_UP) && (monsterDirections[i] == ACTION_DOWN) && (agentLocation.first == monsterLocations[i].first-1) && (agentLocation.second == monsterLocations[i].second))
              )
            {
                if(powerUpTimeRemaining > 0)
                {
                    monsterLocations[i] = make_pair(-1,-1);
                    reward++;
                    monstersKilled++;
                    powerUpTimeRemaining = 0;
                }
                else
                {
                    active = false;
                }
            }
        }
        
        if(powerUpTimeRemaining > 0)
        {
            powerUpTimeRemaining--;
        }
        
        if(monstersKilled == numMonsters)
        {
            active = false;
        }
        
        generateScreen();
        return (double)reward;
    }
    
    int getNumActions()
    {
        return numActions;
    }
    
    unordered_set<int> getFeatures()
    {
        if(featureType == 4)
        {
            return getLogarithmicRelativeFeatures();
        }
        else if(featureType == 3)
        {
            return getPairwiseLogarithmicRelativeFeatures();
        }
        else if(featureType == 2)
        {
            return getPairwiseRelativeFeatures();
        }
        else if(featureType == 1)
        {
            return getRelativeFeatures();
        }
        else if(featureType == 0)
        {
            return getAbsoluteFeatures();
        }
        else
        {
            assert(0);
        }
    }
    
    int getNumFeatures()
    {
        if(featureType == 4)
        {
            return 1 + (numColours-1) * 4 * (floor(log2(height-1))+2) * (floor(log2(width-1))+2);
        }
        else if(featureType == 3)
        {
            return 1 + height*width*(numColours-1) + 4*(floor(log2(height-1))+2)*(floor(log2(width-1))+2)*(numColours-1)*(numColours)/2;
        }
        else if(featureType == 2)
        {
            return 1 + (2*proximity-1)*(2*proximity-1)*(numColours-1)*(numColours)/2;
        }
        else if(featureType == 1)
        {
            return 1 + (2*proximity-1)*(2*proximity-1)*(numColours-1);
        }
        else if(featureType == 0)
        {
            return 1 + height*width*(numColours-1);
        }
        else
        {
            assert(0);
        }
    }
    
    unordered_set<int> getAbsoluteFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i=0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                if(screen[i][j] > 0)
                {
                    features.insert(1 + (screen[i][j]-1)*height*width + i*width + j);
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = max(0, agentLocation.first-proximity+1); i < min(int(height), agentLocation.first+proximity); i++)
        {            
            for(int j = max(0, agentLocation.second-proximity+1); j < min(int(width), agentLocation.second+proximity); j++)
            {
                if(screen[i][j] > 0)
                {
                    features.insert(1 + (screen[i][j]-1)*(2*proximity-1)*(2*proximity-1) + (i-agentLocation.first+proximity-1)*(2*proximity-1) + (j-agentLocation.second+proximity-1));
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getPairwiseRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = 0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                for(int k = max(0, i-proximity+1); k < min(int(height), i+proximity); k++)
                {              
                    for(int l = max(0, j-proximity+1); l < min(int(width), j+proximity); l++)
                    {
                        if(screen[i][j] == 0 || screen[i][j] > screen[k][l])
                        {
                            continue;
                        }
                        
                        int featureIndex = 1 + ((screen[i][j]-1)*(numColours-1)-((screen[i][j]-1)*(screen[i][j]-2))/2+screen[k][l]-screen[i][j])*(2*proximity-1)*(2*proximity-1) + (k-i+proximity-1)*(2*proximity-1) + (l-j+proximity-1);
                        
                        if(features.find(featureIndex) == features.end())
                        {
                            features.insert(featureIndex);
                        }
                    }
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getPairwiseLogarithmicRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = 0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                for(int k = 0; k < height; k++)
                {              
                    for(int l = 0; l < width; l++)
                    {
                        if(screen[i][j] == 0 || screen[i][j] > screen[k][l])
                        {
                            continue;
                        }
                        
                        int quadrant = (k>i)*2 + (l>j);
                        int logX;
                        int logY;
                        
                        if(k==i)
                        {
                            logX = 0;
                        }
                        else
                        {
                            logX = floor(log2(abs(k-i)))+1;
                        }
                        
                        if(l==j)
                        {
                            logY = 0;
                        }
                        else
                        {
                            logY = floor(log2(abs(l-j)))+1;
                        }
                        
                        int featureIndex = 1 + height*width*(numColours-1) + ((screen[i][j]-1)*(numColours-1)-((screen[i][j]-1)*(screen[i][j]-2))/2+screen[k][l]-screen[i][j])*4*(floor(log2(height-1))+2)*(floor(log2(width-1))+2) + quadrant*(floor(log2(height-1))+2)*(floor(log2(width-1))+2) + logX*(floor(log2(width-1))+2) + logY;
                        
                        if(features.find(featureIndex) == features.end())
                        {
                            features.insert(featureIndex);
                        }
                    }
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getLogarithmicRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = max(0, agentLocation.first-proximity+1); i < min(int(height), agentLocation.first+proximity); i++)
        {            
            for(int j = max(0, agentLocation.second-proximity+1); j < min(int(width), agentLocation.second+proximity); j++)
            {
                if(screen[i][j] > 0)
                {
                    int quadrant = (i>agentLocation.first)*2 + (j>agentLocation.second);
                    int relI = abs(i-agentLocation.first);
                    int relJ = abs(j-agentLocation.second);
                    int logI;
                    int logJ;
                    
                    if(relI == 0)
                    {
                        logI = 0;
                    }
                    else
                    {
                        logI = 1+floor(log2(relI));
                    }
                    
                    if(relJ == 0)
                    {
                        logJ = 0;
                    }
                    else
                    {
                        logJ = 1+floor(log2(relJ));
                    }
                    
                    int featureIndex = ( (screen[i][j]-1) * 4 * (floor(log2(height-1))+2) * (floor(log2(width-1))+2) +
                                         quadrant * (floor(log2(height-1))+2) * (floor(log2(width-1))+2) +
                                         logI * (floor(log2(width-1))+2) +
                                         logJ +
                                         1
                                       );
                    
                    assert(featureIndex >= 0);
                    assert(featureIndex < (numColours-1) * 4 * (floor(log2(height-1))+2) * (floor(log2(width-1))+2) + 1);
                    
                    if(features.find(featureIndex) == features.end())
                    {
                        features.insert(featureIndex);
                    }
                }
            }
        }
        
        return features;
    }
};

unordered_set<int> expandFeatures(unordered_set<int>& features, int size)
{
    unordered_set<int> expandedFeatures = features;
    for(auto it1 = features.begin(); it1 != features.end(); it1++)
    {
        auto it2 = it1;
        it2++;
        
        for(; it2 != features.end(); it2++)
        {
            int minF = min(*it1, *it2);
            int maxF = max(*it1, *it2);
            expandedFeatures.insert(size + size*minF - minF*(minF+1)/2 + maxF - minF - 1);
        }
    }
    
    return expandedFeatures;
}

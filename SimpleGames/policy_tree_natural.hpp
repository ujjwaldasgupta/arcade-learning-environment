#include <vector>
#include <cstdio>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <deque>
#include <set>
#include <numeric>
#include <functional>
#include <algorithm>
#include <iostream>
#include <unordered_set>
#include <armadillo>

#include "sample.hpp"

using namespace std;
using namespace arma;

template<class Matrix> void print_matrix(Matrix matrix)
{
    matrix.print(cout);
}

template void print_matrix<mat>(mat matrix);
template void print_matrix<vec>(vec matrix);

const double tolerance = 1e-6;

class PolicyTreeNatural
{
public:
    class PolicyTreeNaturalNode
    {
    public:
        int id;
        PolicyTreeNatural &policyTree;
        vector<PolicyTreeNaturalNode* > children;
        vector<int> splitIndices;
        
        PolicyTreeNaturalNode *parent;

        PolicyTreeNaturalNode(PolicyTreeNatural &policyTree, int id) : id(id), policyTree(policyTree)
        {
        }

        
        pair<double, int> checkForDivergingNodes()
        {
            return make_pair(1, 1);
        }

        void promoteObservationFringeNode(int splitIndex)
        {
            splitIndices.push_back(splitIndex);

            cout << "Adding split on " << splitIndex << "\n";
            
            children = vector<PolicyTreeNaturalNode * >(2, NULL);            
            children[0] = new PolicyTreeNaturalNode(policyTree, id);    
            children[0]->parent = this;
            children[0]->id = id;
            children[1] = new PolicyTreeNaturalNode(policyTree, policyTree.numLeafNodes);    
            children[1]->parent = this;
            children[1]->id = policyTree.numLeafNodes;
            policyTree.numLeafNodes += 1;
        }
    };

    PolicyTreeNaturalNode *root;
    int numObservations;
    int numActions;
    int numLeafNodes;
    double learningRate;
    double minActionProbability;
    int maxLeafNodes;
    int gradientAverageWindow;
    int gradientAverageWindowForSplit;
    int optimizationSteps;
    unordered_set<int> observations;
    deque<int > actionHistory;
    bool optimizing;
    int countSteps;
    int episodeLength;
    unsigned long long episodeCount;
    vector<double> averageRewardAtTime;
    double regularizationConstant;
    double parameterNormConstraint;
    double temperature;
    vector<double> actionProbabilities;
    vec parameters;
    vec eligibilityTrace;
    vec sumEligibilityTrace;
    vec sumGradient;
    mat sumFischerMatrix;
    double totalReward;
    double sumTotalReward;
    bool flag;
    
    PolicyTreeNatural(int numObservations, int numActions, double learningRate, double minActionProbability, int maxLeafNodes, int gradientAverageWindow, int gradientAverageWindowForSplit, int optimizationSteps, double regularizationConstant, double parameterNormConstraint): numObservations(numObservations), numActions(numActions), numLeafNodes(1), learningRate(learningRate), minActionProbability(minActionProbability), maxLeafNodes(maxLeafNodes), gradientAverageWindow(gradientAverageWindow), gradientAverageWindowForSplit(gradientAverageWindowForSplit), optimizationSteps(optimizationSteps), countSteps(0), episodeLength(0), optimizing(true), episodeCount(0), actionProbabilities(vector<double>(numActions, 0)), regularizationConstant(regularizationConstant), parameterNormConstraint(parameterNormConstraint), totalReward(0), sumTotalReward(0), temperature(1.0), flag(false)
    {
        root = new PolicyTreeNaturalNode(*this, 0);
        assert(gradientAverageWindow > 0);
        assert(optimizationSteps >= 0);
        assert(maxLeafNodes > 0);
        
        if(optimizationSteps == 0)
        {
            optimizing = false;
        }
        
        sumEligibilityTrace = zeros<vec>(numActions*numObservations);
        sumGradient = zeros<vec>(numActions*numObservations);
        sumFischerMatrix = zeros<mat>(numActions*numObservations, numActions*numObservations);
        sumTotalReward = 0;
        parameters = zeros<vec>(numActions*numObservations);
        eligibilityTrace = zeros<vec>(numActions*numObservations);
    }

    ~PolicyTreeNatural()
    {
        freePolicyTreeNaturalNode(root);
    }

    void freePolicyTreeNaturalNode(PolicyTreeNaturalNode *node)
    {
        for(int i = 0; i < node->children.size(); i++)
        {
            freePolicyTreeNaturalNode(node->children[i]);
        }

        delete node;
    }
    
    PolicyTreeNaturalNode *getCurrentNode()
    {
        return getCurrentNode(root);
    }

    PolicyTreeNaturalNode *getCurrentNode(PolicyTreeNaturalNode *node)
    {
        if(node->children.empty())
        {
            return node;
        }
        else
        {
            bool flag = false;

            for(int i = 0; i < node->splitIndices.size(); i++)
            {
                assert(node->splitIndices[i] < numObservations);
                
                if(observations.find(node->splitIndices[i]) != observations.end())
                {
                    flag = true;
                    break;
                }
            }

            if(flag)
            {
                return getCurrentNode(node->children[1]);
            }
            else
            {
                return getCurrentNode(node->children[0]);
            }
        }
    }

    
    void updateActionProbabilities()
    {
        assert(parameters.n_rows == numLeafNodes*numActions*numObservations);
        double normalization = 0;
        vector<double> expSum(numActions, 0);
        double maxSum = 0;
        int id = getCurrentNode()->id;
        
        for(int i = 0; i < numActions; i++)
        {
            for(auto it = observations.begin(); it != observations.end(); it++)
            {
                assert(*it < numObservations);
                expSum[i] += parameters(id*numActions*numObservations + i*numObservations + *it);
            }
            
            if(expSum[i] > maxSum)
            {
                maxSum = expSum[i];
            }
        }
        
        for(int i = 0; i < numActions; i++)
        {
            actionProbabilities[i] = exp((expSum[i]-maxSum)/temperature);
            normalization += actionProbabilities[i];
        }

        for(int i = 0; i < numActions; i++)
        {
            actionProbabilities[i] /= normalization;
        }
    }
        
    void resetGradients()
    {

    }

    void updateTrace(int nextAction)
    {
        int id = getCurrentNode()->id;
        
        if(flag)
        {
            cout<<"ID: "<<id<<"\n";
            cout<<"Observations:\n";
        }
        
        for(auto it = observations.begin(); it != observations.end() ; it++)
        {
            for(int i = 0; i < numActions; i++)
            {    
                assert(*it >= 0);
                assert(*it < numObservations);
                double correctiveTerm = 1 + minActionProbability/((1-numActions*minActionProbability)*actionProbabilities[i]);
                eligibilityTrace(id*numActions*numObservations + i*numObservations + *it) += ((i==nextAction)-actionProbabilities[i])/(temperature*correctiveTerm);
            }
            
            if(flag)
            {
                cout<<*it<<"\n";
            }
        }
        
        if(flag)
        {
            cout<<"Trace vector:\n"<<eligibilityTrace<<"\n";
        }
    }
        
    void storeObservations(unordered_set<int>& observations)
    {
        this->observations = observations;
        updateActionProbabilities();
    }

    int storeObservationsAndGetAction(unordered_set<int>& observations)
    {
        storeObservations(observations);
        return getAction();
    }

    int getAction()
    {
        int action = sample(actionProbabilities);        
        double r =(double) rand() / RAND_MAX;
        
        if(r < minActionProbability*numActions)
        {
            action = rand() % numActions;
        }
        
        return action;
    }

    void processAction(int action, double reward)
    {
        updateTrace(action);
        assert(is_finite(eligibilityTrace));
        totalReward += reward;
        episodeLength++;
    }
    
    void storeGradient()
    {
        sumEligibilityTrace += eligibilityTrace;
        sumGradient += totalReward * eligibilityTrace;
        mat fischerMatrix = eligibilityTrace * eligibilityTrace.t();
        sumFischerMatrix += fischerMatrix;
        sumTotalReward += totalReward;
    }
    
    void storeFringeGradient()
    {
    }

    void performGradientStep()
    {
        sumEligibilityTrace /= gradientAverageWindow;
        sumGradient /= gradientAverageWindow;
        sumFischerMatrix /= gradientAverageWindow;
        sumTotalReward /= gradientAverageWindow;
        
        sumFischerMatrix += regularizationConstant*eye(numLeafNodes*numActions*numObservations, numLeafNodes*numActions*numObservations);
        mat T = (gradientAverageWindow*sumFischerMatrix - sumEligibilityTrace*sumEligibilityTrace.t());
        T = sumEligibilityTrace.t()*inv(T)*sumEligibilityTrace;
        double Q = (1 + trace(T))/gradientAverageWindow;
        T = sumEligibilityTrace.t()*inv(sumFischerMatrix)*sumGradient;
        double b = Q * (sumTotalReward - trace(T));
        vec naturalG = inv(sumFischerMatrix) * (sumGradient - sumEligibilityTrace*b);
        parameters += learningRate * naturalG;
        
        if(norm(parameters, 2) > parameterNormConstraint)
        {
            parameters *= parameterNormConstraint/norm(parameters, 2);
        }
        
        sumEligibilityTrace = zeros<vec>(numLeafNodes*numActions*numObservations);
        sumGradient = zeros<vec>(numLeafNodes*numActions*numObservations);
        sumFischerMatrix = zeros<mat>(numLeafNodes*numActions*numObservations, numLeafNodes*numActions*numObservations);
        sumTotalReward = 0;
    }

    pair<PolicyTreeNaturalNode *, pair<double, int > > checkForDivergingNodes()
    {
        return checkForDivergingNodes(root);
    }

    pair<PolicyTreeNaturalNode *, pair<double, int > > checkForDivergingNodes(PolicyTreeNaturalNode *node)
    {
        pair<PolicyTreeNaturalNode *, pair<double, int > > ret;
        ret.first = NULL;
        ret.second.first = 0;
        ret.second.second = -1;

        if(node->children.size() == 0)
        {
            ret.first = node;
            ret.second = node->checkForDivergingNodes();
        }
        else
        {
            for(int i = 0; i < node->children.size(); i++)
            {
                pair<PolicyTreeNaturalNode *, pair<double, int > > childRet = checkForDivergingNodes(node->children[i]);
                if(childRet.second.first > ret.second.first)
                {
                    ret = childRet;
                }
            }
        }

        return ret;
    }
    
    void processEpisode()
    {
        episodeCount++;
        
        if(optimizing)
        {
            countSteps++;
            storeGradient();
            
            if(countSteps % gradientAverageWindow == 0)
            {
                performGradientStep();
            }
            
            if(countSteps == optimizationSteps)
            {
                optimizing = false;
                countSteps = 0;
            }
        }
        else
        {       
            countSteps++;
            storeFringeGradient();
            
            if(countSteps % gradientAverageWindowForSplit == 0)
            {
                pair<PolicyTreeNaturalNode *, pair<double, int> > ret = checkForDivergingNodes();
                if(ret.first && ret.second.second >= 0)
                {
                    ret.first->promoteObservationFringeNode(ret.second.second);
                    vec newParameters = parameters.rows(ret.first->id*numActions*numObservations, (ret.first->id+1)*numActions*numObservations-1);
                    parameters.insert_rows(parameters.n_rows, newParameters);
                    sumEligibilityTrace = zeros<vec>(numLeafNodes*numActions*numObservations);
                    sumGradient = zeros<vec>(numLeafNodes*numActions*numObservations);
                    sumFischerMatrix = zeros<mat>(numLeafNodes*numActions*numObservations, numLeafNodes*numActions*numObservations);
                    sumTotalReward = 0;
                }
                countSteps = 0;
                optimizing = true;
            }
        }

        eligibilityTrace = zeros<vec>(numLeafNodes*numActions*numObservations);
        observations.clear();
        episodeLength = 0;
        totalReward = 0;
    }
};

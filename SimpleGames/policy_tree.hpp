#include <vector>
#include <cstdio>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <deque>
#include <set>
#include <numeric>
#include <functional>
#include <algorithm>
#include <iostream>
#include <unordered_set>
#include "sample.hpp"

const double tolerance = 1e-6;

struct pair_hash
{
    inline std::size_t operator() ( const std::pair<int, int> &v ) const
    {
        return v.first ^ v.second;
    }
};

class PolicyTree
{
public:
    class PolicyTreeNode
    {
    public:
        PolicyTree &policyTree;
        bool isFringe;
        std::vector<PolicyTreeNode * > children;
        std::vector<std::vector<PolicyTreeNode * > > observationFringeChildren;
        std::vector<std::vector<PolicyTreeNode *> > actionFringeChildren;
        std::vector<double> actionProbabilities;
        std::vector<double> actionParameters;
        std::vector<double> Z;
        std::vector<int> actionCounts;
        std::vector<double> actionProbabilitiesGradient;
        std::vector<double> sumActionProbabilitiesGradient;
        std::unordered_set<std::pair<int, int>, pair_hash> observationsToUpdateCache;
        std::vector<std::pair<int, int> > splitIndices;
        PolicyTreeNode *parent;

        PolicyTreeNode ( PolicyTree &policyTree, bool isFringe ) : policyTree ( policyTree ), isFringe ( isFringe ), Z ( policyTree.numActions, 0 ), actionCounts ( policyTree.numActions, 0 ), actionParameters ( policyTree.numActions, 0 ), actionProbabilitiesGradient ( policyTree.numActions, 0 ), parent ( NULL ), sumActionProbabilitiesGradient ( policyTree.numActions, 0 )
        {
            if ( !isFringe )
            {
                createFringe();
                actionProbabilities = calculateActionProbabilitiesFromParameters ( actionParameters );
            }
        }

        ~PolicyTreeNode()
        {
            if ( !isFringe )
            {
                deleteFringe();
            }
        }

        void createFringe()
        {
            assert ( !isFringe );

            observationFringeChildren = std::vector<std::vector<PolicyTreeNode * > > ( policyTree.sizeHistory, std::vector<PolicyTreeNode * > ( policyTree.numObservations ) );

            for ( int i = 0; i < policyTree.sizeHistory; i++ )
            {
                for ( int j = 0; j < policyTree.numObservations; j++ )
                {
                    observationFringeChildren[i][j] = new PolicyTreeNode ( policyTree, true );
                    observationFringeChildren[i][j]->parent = this;
                }
            }

            actionFringeChildren = std::vector<std::vector<PolicyTreeNode *> > ( policyTree.sizeHistory, std::vector<PolicyTreeNode *> ( policyTree.numActions ) );

            for ( int i = 0; i < policyTree.sizeHistory; i++ )
            {
                for ( int j = 0; j < policyTree.numActions; j++ )
                {
                    actionFringeChildren[i][j] = new PolicyTreeNode ( policyTree, true );
                    actionFringeChildren[i][j]->parent = this;
                }
            }
        }

        void deleteFringe()
        {
            assert ( !isFringe );

            for ( int i = 0; i < policyTree.sizeHistory; i++ )
            {
                for ( int j = 0; j < policyTree.numObservations; j++ )
                {
                    if ( observationFringeChildren[i][j] )
                    {
                        delete observationFringeChildren[i][j];
                    }
                }
            }

            for ( int i = 0; i < policyTree.sizeHistory; i++ )
            {
                for ( int j = 0; j < policyTree.numActions; j++ )
                {
                    delete actionFringeChildren[i][j];
                }
            }

            observationFringeChildren.clear();
            actionFringeChildren.clear();
        }

        void updateZ ( int nextAction )
        {
            actionCounts[nextAction]++;

            if ( isFringe )
            {
                assert ( parent );
                if ( policyTree.useSoftMax )
                {
                    for ( int i = 0; i < policyTree.numActions; i++ )
                    {
                        double correctiveTerm = 1 + policyTree.minActionProbability / ( ( 1 - policyTree.numActions * policyTree.minActionProbability ) * parent->actionProbabilities[i] );

                        if ( i == nextAction )
                        {
                            Z[i] += ( 1 - parent->actionProbabilities[i] ) / ( policyTree.temperature * correctiveTerm );
                        }
                        else
                        {
                            Z[i] += ( -1 * parent->actionProbabilities[i] ) / ( policyTree.temperature * correctiveTerm );
                        }
                    }
                }
                else
                {
                    Z[nextAction] += 1.0 / parent->actionProbabilities[nextAction];
                }
            }
            else
            {
                if ( policyTree.useSoftMax )
                {
                    for ( int i = 0; i < policyTree.numActions; i++ )
                    {
                        double correctiveTerm = 1 + policyTree.minActionProbability / ( ( 1 - policyTree.numActions * policyTree.minActionProbability ) * actionProbabilities[i] );

                        if ( i == nextAction )
                        {
                            Z[i] += ( 1 - actionProbabilities[i] ) / ( policyTree.temperature * correctiveTerm );
                        }
                        else
                        {
                            Z[i] += ( -1 * actionProbabilities[i] ) / ( policyTree.temperature * correctiveTerm );
                        }
                    }
                }
                else
                {
                    Z[nextAction] += 1.0 / actionProbabilities[nextAction];
                }

                if ( !policyTree.optimizing )
                {
                    for ( int i = 0; i < policyTree.sizeHistory; i++ )
                    {
                        assert ( i < policyTree.observationsHistory.size() );
                        std::unordered_set<int> observations = policyTree.observationsHistory.at ( i );
                        for ( auto it = observations.begin(); it != observations.end(); it++ )
                        {
                            assert ( ( *it ) < policyTree.numObservations );
                            observationsToUpdateCache.insert ( std::make_pair ( i, ( *it ) ) );
                            observationFringeChildren[i][ ( *it )]->updateZ ( nextAction );
                        }

                        assert ( i < policyTree.actionHistory.size() );
                        int action = policyTree.actionHistory.at ( i );
                        assert ( actionFringeChildren[i][action] );
                        actionFringeChildren[i][action]->updateZ ( nextAction );
                    }
                }
            }
        }

        std::vector<double> calculateActionProbabilitiesFromParameters ( std::vector<double> actionParameters )
        {
            std::vector<double> actionProbabilities ( policyTree.numActions, 0 );
            assert ( actionParameters.size() == policyTree.numActions );
            double sum = 0;

            for ( int i = 0; i < policyTree.numActions; i++ )
            {
                actionProbabilities[i] = exp ( actionParameters[i] / policyTree.temperature );
                sum += actionProbabilities[i];
            }

            for ( int i = 0; i < policyTree.numActions; i++ )
            {
                actionProbabilities[i] /= sum;
            }

            return actionProbabilities;
        }

        void updateGPOMDPGradient ( double reward )
        {
            if ( policyTree.useTotalReward )
            {
                std::vector<double> delta ( policyTree.numActions );
                std::transform ( Z.begin(), Z.end(), delta.begin(), std::bind2nd ( std::multiplies<double>(), reward ) );
                std::transform ( actionProbabilitiesGradient.begin(), actionProbabilitiesGradient.end(), delta.begin(), actionProbabilitiesGradient.begin(), std::plus<double>() );
            }
            else
            {
                std::vector<double> delta ( policyTree.numActions );
                std::transform ( Z.begin(), Z.end(), delta.begin(), std::bind2nd ( std::multiplies<double>(), ( double ) reward ) );
                std::transform ( delta.begin(), delta.end(), actionProbabilitiesGradient.begin(), delta.begin(), std::minus<double>() );
                std::transform ( delta.begin(), delta.end(), delta.begin(), std::bind2nd ( std::divides<double>(), ( double ) policyTree.episodeLength ) );
                std::transform ( actionProbabilitiesGradient.begin(), actionProbabilitiesGradient.end(), delta.begin(), actionProbabilitiesGradient.begin(), std::plus<double>() );
            }

            if ( !isFringe && !policyTree.optimizing )
            {
                for ( auto it = observationsToUpdateCache.begin(); it != observationsToUpdateCache.end(); it++ )
                {
                    assert ( ( *it ).first < policyTree.sizeHistory );
                    assert ( ( *it ).second < policyTree.numObservations );
                    observationFringeChildren[ ( *it ).first][ ( *it ).second]->updateGPOMDPGradient ( reward );
                }
                for ( int i = 0 ; i < policyTree.sizeHistory; i++ )
                {
                    for ( int j = 0 ; j < policyTree.numActions; j++ )
                    {
                        actionFringeChildren[i][j]->updateGPOMDPGradient ( reward );
                    }
                }
            }
        }

        void updateREINFORCEGradient ( double totalReward )
        {
            if ( policyTree.useTotalReward )
            {
                std::transform ( Z.begin(), Z.end(), actionProbabilitiesGradient.begin(), std::bind2nd ( std::multiplies<double>(), totalReward ) );
            }
            else
            {
                std::transform ( Z.begin(), Z.end(), actionProbabilitiesGradient.begin(), std::bind2nd ( std::multiplies<double>(), totalReward / policyTree.episodeLength ) );
            }

            if ( !isFringe && !policyTree.optimizing )
            {
                for ( auto it = observationsToUpdateCache.begin(); it != observationsToUpdateCache.end(); it++ )
                {
                    observationFringeChildren[ ( *it ).first][ ( *it ).second]->updateREINFORCEGradient ( totalReward );
                }
                for ( int i = 0 ; i < policyTree.sizeHistory; i++ )
                {
                    for ( int j = 0 ; j < policyTree.numActions; j++ )
                    {
                        actionFringeChildren[i][j]->updateREINFORCEGradient ( totalReward );
                    }
                }
            }
        }

        std::vector<double> calculateActionProbabilitiesChange ( std::vector<double> actionProbabilitiesGradient )
        {
            assert ( !isFringe );

            if ( policyTree.useSoftMax )
            {
                std::vector<double> newActionParameters ( policyTree.numActions, 0 );
                std::vector<double> delta ( policyTree.numActions, 0 );
                std::transform ( actionProbabilitiesGradient.begin(), actionProbabilitiesGradient.end(), delta.begin(), std::bind2nd ( std::multiplies<double>(), policyTree.learningRate ) );
                std::transform ( actionParameters.begin(), actionParameters.end(), delta.begin(), newActionParameters.begin(), std::plus<double>() );

                std::vector<double> newActionProbabilities = calculateActionProbabilitiesFromParameters ( newActionParameters );
                std::transform ( newActionProbabilities.begin(), newActionProbabilities.end(), actionProbabilities.begin(), delta.begin(), std::minus<double>() );

                return delta;
            }
            else
            {
                double sum = std::accumulate ( actionProbabilitiesGradient.begin(), actionProbabilitiesGradient.end(), 0.0, std::plus<double>() );

                for ( int i = 0; i < policyTree.numActions; i++ )
                {
                    actionProbabilitiesGradient[i] -= sum / policyTree.numActions;
                }

                std::vector<double> actionProbabilitiesChange ( policyTree.numActions );

                for ( int i = 0; i < policyTree.numActions; i++ )
                {
                    actionProbabilitiesChange[i] = policyTree.learningRate * actionProbabilitiesGradient[i];
                }

                bool valid;
                std::set<int> dontTouch;

                do
                {
                    valid = true;
                    sum = 0;

                    for ( int i = 0; i < policyTree.numActions; i++ )
                    {
                        if ( ( dontTouch.find ( i ) == dontTouch.end() ) && ( actionProbabilities[i] + actionProbabilitiesChange[i] < policyTree.minActionProbability ) )
                        {
                            dontTouch.insert ( i );
                            sum += actionProbabilities[i] + actionProbabilitiesChange[i] - policyTree.minActionProbability;
                            actionProbabilitiesChange[i] = policyTree.minActionProbability - actionProbabilities[i];
                        }
                    }

                    for ( int i = 0; i < policyTree.numActions; i++ )
                    {
                        if ( dontTouch.find ( i ) == dontTouch.end() )
                        {
                            actionProbabilitiesChange[i] += sum / ( policyTree.numActions - dontTouch.size() );
                            if ( actionProbabilitiesChange[i] + actionProbabilities[i] < policyTree.minActionProbability )
                            {
                                valid = false;
                            }
                        }
                    }
                }
                while ( !valid );

                return actionProbabilitiesChange;
            }
        }

        std::pair<double, std::pair<int, int> > checkForDivergingNodes()
        {
            int bestHIndex = -1;
            int bestOIndex = -1;

            double bestScore = 0;
            
            std::vector<std::vector<double> > scores(policyTree.sizeHistory, std::vector<double>(policyTree.numObservations, 0));

            std::vector<double> averageActionProbabilitiesGradient ( policyTree.numActions );
            std::transform ( sumActionProbabilitiesGradient.begin(), sumActionProbabilitiesGradient.end(), averageActionProbabilitiesGradient.begin(), std::bind2nd ( std::divides<double>(), ( double ) policyTree.countSteps ) );
            std::vector<double> averageActionProbabilitiesChange = calculateActionProbabilitiesChange ( averageActionProbabilitiesGradient );

            for ( int i = 0; i < policyTree.sizeHistory; i++ )
            {
                for ( int j = 0; j < policyTree.numObservations; j++ )
                {
                    std::vector<double> averageFringeActionProbabilitiesGradient ( policyTree.numActions );
                    std::transform ( observationFringeChildren[i][j]->sumActionProbabilitiesGradient.begin(), observationFringeChildren[i][j]->sumActionProbabilitiesGradient.end(), averageFringeActionProbabilitiesGradient.begin(), std::bind2nd ( std::divides<double>(), ( double ) policyTree.countSteps ) );
                    std::vector<double> averageFringeActionProbabilitiesChange = calculateActionProbabilitiesChange ( averageFringeActionProbabilitiesGradient );
                    std::vector<double> averageComplementFringeActionProbabilitiesGradient ( policyTree.numActions );
                    std::transform ( averageActionProbabilitiesGradient.begin(), averageActionProbabilitiesGradient.end(), averageFringeActionProbabilitiesGradient.begin(), averageComplementFringeActionProbabilitiesGradient.begin(), std::minus<double>() );
                    std::vector<double> averageComplementFringeActionProbabilitiesChange = calculateActionProbabilitiesChange ( averageComplementFringeActionProbabilitiesGradient );
                    double fringeCos = cosine ( averageActionProbabilitiesGradient, averageFringeActionProbabilitiesGradient );
                    double fringeNorm = l2Norm ( averageFringeActionProbabilitiesChange );
                    double complementFringeCos = cosine ( averageActionProbabilitiesGradient, averageComplementFringeActionProbabilitiesGradient );
                    double complementFringeNorm = l2Norm ( averageComplementFringeActionProbabilitiesChange );

                    double score = 0;
                    
                    if(policyTree.useCosineSplit)
                    {
                        score = -1 * std::min ( fringeCos * l2Norm( averageFringeActionProbabilitiesGradient ), complementFringeCos * l2Norm( averageComplementFringeActionProbabilitiesGradient ) );
                    }
                    else
                    {
                        score = std::max ( infinityNorm( averageFringeActionProbabilitiesChange ), infinityNorm( averageComplementFringeActionProbabilitiesChange ) );
                    }
                    
                    scores[i][j] = score;
                    
                    if ( score > bestScore )
                    {
                        bestScore = score;
                        bestHIndex = i;
                        bestOIndex = j;
                    }
                }
            }

            return std::make_pair ( bestScore, std::make_pair ( bestHIndex, bestOIndex ) );
        }

        void cleanUpAfterEpisode()
        {
            actionProbabilitiesGradient = std::vector<double> ( policyTree.numActions, 0 );
            Z = std::vector<double> ( policyTree.numActions, 0 );
            actionCounts = std::vector<int> ( policyTree.numActions, 0 );

            if ( !policyTree.optimizing )
            {
                for ( auto it = observationsToUpdateCache.begin(); it != observationsToUpdateCache.end(); it++ )
                {
                    observationFringeChildren[ ( *it ).first][ ( *it ).second]->Z = std::vector<double> ( policyTree.numActions, 0 );
                    observationFringeChildren[ ( *it ).first][ ( *it ).second]->actionProbabilitiesGradient = std::vector<double> ( policyTree.numActions, 0 );
                }

                for ( int i = 0 ; i < policyTree.sizeHistory; i++ )
                {
                    for ( int j = 0 ; j < policyTree.numActions; j++ )
                    {
                        actionFringeChildren[i][j]->Z = std::vector<double> ( policyTree.numActions, 0 );
                        actionFringeChildren[i][j]->actionProbabilitiesGradient = std::vector<double> ( policyTree.numActions, 0 );
                    }
                }
            }

            observationsToUpdateCache.clear();
        }

        void performGradientStep()
        {
            assert ( !isFringe );

            if ( policyTree.useSoftMax )
            {
                std::vector<double> delta ( policyTree.numActions, 0 );
                std::transform ( actionProbabilitiesGradient.begin(), actionProbabilitiesGradient.end(), delta.begin(), std::bind2nd ( std::multiplies<double>(), policyTree.learningRate / policyTree.countSteps ) );
                std::transform ( actionParameters.begin(), actionParameters.end(), delta.begin(), actionParameters.begin(), std::plus<double>() );
                actionProbabilities = calculateActionProbabilitiesFromParameters ( actionParameters );
            }
            else
            {
                std::vector<double> actionProbabilitiesChange = calculateActionProbabilitiesChange ( actionProbabilitiesGradient );
                std::transform ( actionProbabilities.begin(), actionProbabilities.end(), actionProbabilitiesChange.begin(), actionProbabilities.begin(), std::plus<double>() );
            }
        }

        void storeGradient()
        {
            assert ( !policyTree.optimizing );
            std::transform ( sumActionProbabilitiesGradient.begin(), sumActionProbabilitiesGradient.end(), actionProbabilitiesGradient.begin(), sumActionProbabilitiesGradient.begin(), std::plus<double>() );

            if ( !isFringe )
            {
                for ( auto it = observationsToUpdateCache.begin(); it != observationsToUpdateCache.end(); it++ )
                {
                    assert ( ( *it ).first < policyTree.sizeHistory );
                    assert ( ( *it ).second < policyTree.numObservations );
                    observationFringeChildren[ ( *it ).first][ ( *it ).second]->storeGradient();
                }

                for ( int i = 0; i < policyTree.sizeHistory; i++ )
                {
                    for ( int j = 0 ; j < policyTree.numActions; j++ )
                    {
                        actionFringeChildren[i][j]->storeGradient();
                    }
                }
            }
        }

        void resetGradients()   /* This should be called only after a "converged" tree is ready to be optimized again (i.e. a node has been added or removed, or the tree has been found to be divergent). */
        {
            actionProbabilitiesGradient = std::vector<double> ( policyTree.numActions, 0 );
            sumActionProbabilitiesGradient = std::vector<double> ( policyTree.numActions, 0 );
            policyTree.countSteps = 0;

            if ( !isFringe )
            {
                for ( int i = 0; i < policyTree.sizeHistory; i++ )
                {
                    for ( int j = 0; j < policyTree.numObservations; j++ )
                    {
                        observationFringeChildren[i][j]->resetGradients();
                    }

                    for ( int j = 0 ; j < policyTree.numActions; j++ )
                    {
                        actionFringeChildren[i][j]->resetGradients();
                    }
                }
            }
        }

        void promoteObservationFringeNode ( int hIndex, int oIndex )
        {
            splitIndices.push_back ( std::make_pair ( hIndex, oIndex ) );

            std::cout << "Adding split on (" << hIndex << ", " << oIndex << ") ";
            
            if ( policyTree.useMultiSplit )
            {
                std::vector<double> averageActionProbabilitiesGradient ( policyTree.numActions );
                std::transform ( sumActionProbabilitiesGradient.begin(), sumActionProbabilitiesGradient.end(), averageActionProbabilitiesGradient.begin(), std::bind2nd ( std::divides<double>(), ( double ) policyTree.countSteps ) );
                std::vector<double> averageChosenFringeActionProbabilitiesGradient ( policyTree.numActions );
                std::transform ( observationFringeChildren[hIndex][oIndex]->sumActionProbabilitiesGradient.begin(), observationFringeChildren[hIndex][oIndex]->sumActionProbabilitiesGradient.end(), averageChosenFringeActionProbabilitiesGradient.begin(), std::bind2nd ( std::divides<double>(), ( double ) policyTree.countSteps ) );
                std::vector<double> averageComplementChosenFringeActionProbabilitiesGradient ( policyTree.numActions );
                std::transform ( averageActionProbabilitiesGradient.begin(), averageActionProbabilitiesGradient.end(), averageChosenFringeActionProbabilitiesGradient.begin(), averageComplementChosenFringeActionProbabilitiesGradient.begin(), std::minus<double>() );

                for ( int i = 0; i < policyTree.sizeHistory; i++ )
                {
                    for ( int j = 0; j < policyTree.numObservations; j++ )
                    {
                        std::vector<double> averageFringeActionProbabilitiesGradient ( policyTree.numActions );
                        std::transform ( observationFringeChildren[i][j]->sumActionProbabilitiesGradient.begin(), observationFringeChildren[i][j]->sumActionProbabilitiesGradient.end(), averageFringeActionProbabilitiesGradient.begin(), std::bind2nd ( std::divides<double>(), ( double ) policyTree.countSteps ) );
                        if ( cosine ( averageFringeActionProbabilitiesGradient, averageChosenFringeActionProbabilitiesGradient ) > policyTree.cosineThreshold )
                        {
                            std::cout << ", (" << i << ", " << j << ") ";
                            splitIndices.push_back ( std::make_pair ( i, j ) );
                        }
                    }
                }
            }
            
            std::cout << "\n";
            children = std::vector<PolicyTreeNode * > ( 2, NULL );
            children[1] = new PolicyTreeNode ( policyTree, false );
            children[0] = new PolicyTreeNode ( policyTree, false );
            children[0]->actionProbabilities = actionProbabilities;
            children[0]->actionParameters = actionParameters;
            children[1]->actionProbabilities = actionProbabilities;
            children[1]->actionParameters = actionParameters;
            children[0]->parent = this;
            children[1]->parent = this;
            deleteFringe();
            policyTree.numLeafNodes += 1;
        }

        double infinityNorm ( std::vector<double> &v )
        {
            double max = *std::max_element ( v.begin(), v.end() );
            double min = *std::min_element ( v.begin(), v.end() );
            return ( std::abs ( max ) > std::abs ( min ) ) ? std::abs ( max ) : std::abs ( min );
        }

        double l2Norm ( std::vector<double> v )
        {
            std::transform ( v.begin(), v.end(), v.begin(), v.begin(), std::multiplies<double>() );
            double ans = std::accumulate ( v.begin(), v.end(), 0.0, std::plus<double>() );
            return std::sqrt ( ans );
        }

        double dotProduct ( std::vector<double> v1, std::vector<double> v2 )
        {
            assert ( v1.size() == v2.size() );
            std::vector<double> temp ( v1.size() );
            std::transform ( v1.begin(), v1.end(), v2.begin(), temp.begin(), std::multiplies<double>() );
            double ans = std::accumulate ( temp.begin(), temp.end(), 0.0, std::plus<double>() );

            return ans;
        }

        double cosine ( std::vector<double> v1, std::vector<double> v2 )
        {
            assert ( v1.size() == v2.size() );
            double v1DotV2 = dotProduct ( v1, v2 );
            double v1DotV1 = dotProduct ( v1, v1 );
            double v2DotV2 = dotProduct ( v2, v2 );

            if ( v1DotV2 == 0 )
            {
                return 0;
            }
            else
            {
                return v1DotV2 / ( sqrt ( v1DotV1 ) * sqrt ( v2DotV2 ) );
            }
        }
    };

    PolicyTreeNode *root;
    int sizeHistory;
    int numObservations;
    int numActions;
    int numLeafNodes;
    double minActionProbability;
    double learningRate;
    int maxLeafNodes;
    int gradientAverageWindow;
    int optimizationSteps;
    std::deque<std::unordered_set<int> > observationsHistory;
    std::deque<int > actionHistory;
    bool optimizing;
    int countSteps;
    int episodeLength;
    double cosineThreshold;
    bool useGPOMDP;
    double totalReward;
    double averageReward;
    unsigned long long averageRewardCount;
    double averageTotalReward;
    unsigned long long episodeCount;
    std::vector<double> averageRewardAtTime;
    bool useTotalReward;
    bool useBaseLine;
    bool useSoftMax;
    bool useMultiSplit;
    bool useCosineSplit;
    
    double temperature;

    PolicyTree ( int sizeHistory, int numObservations, int numActions, double minActionProbability, double learningRate, int maxLeafNodes, int gradientAverageWindow, int optimizationSteps, double cosineThreshold, bool useGPOMDP, bool useTotalReward, bool useBaseLine, bool useSoftMax, bool useMultiSplit, bool useCosineSplit ) : sizeHistory ( sizeHistory ), numObservations ( numObservations ), numActions ( numActions ), numLeafNodes ( 1 ), minActionProbability ( minActionProbability ), learningRate ( learningRate ), maxLeafNodes ( maxLeafNodes ), gradientAverageWindow ( gradientAverageWindow ), optimizationSteps ( optimizationSteps ), cosineThreshold ( cosineThreshold ), countSteps ( 0 ), episodeLength ( 0 ), optimizing ( true ), useGPOMDP ( useGPOMDP ), averageReward ( 0 ), averageTotalReward ( 0 ), averageRewardCount ( 0 ), episodeCount ( 0 ), useTotalReward ( useTotalReward ), useBaseLine ( useBaseLine ), useSoftMax ( useSoftMax ), useMultiSplit ( useMultiSplit ), useCosineSplit ( useCosineSplit ), temperature ( 1.0 )
    {
        root = new PolicyTreeNode ( *this, false );
        assert ( numActions * minActionProbability < 1.0 );
        assert ( minActionProbability > 0 );
        assert ( gradientAverageWindow > 0 );
        assert ( optimizationSteps >= 0 );
        assert ( maxLeafNodes > 0 );
        resetHistory();
        if ( optimizationSteps == 0 )
        {
            optimizing = false;
        }
    }

    ~PolicyTree()
    {
        freePolicyTreeNode ( root );
    }

    void freePolicyTreeNode ( PolicyTreeNode *node )
    {
        for ( int i = 0; i < node->children.size(); i++ )
        {
            freePolicyTreeNode ( node->children[i] );
        }

        delete node;
    }

    PolicyTreeNode *getCurrentNode()
    {
        return getCurrentNode ( root );
    }

    PolicyTreeNode *getCurrentNode ( PolicyTreeNode *node )
    {
        if ( node->children.empty() )
        {
            return node;
        }
        else
        {
            bool flag = false;

            for ( int i = 0; i < node->splitIndices.size(); i++ )
            {
                assert ( node->splitIndices[i].first >= 0 );
                assert ( node->splitIndices[i].first < observationsHistory.size() );
                std::unordered_set<int> observations = observationsHistory.at ( node->splitIndices[i].first );
                if ( observations.find ( node->splitIndices[i].second ) != observations.end() )
                {
                    flag = true;
                    break;
                }
            }

            if ( flag )
            {
                return getCurrentNode ( node->children[1] );
            }
            else
            {
                return getCurrentNode ( node->children[0] );
            }
        }
    }

    void storeObservations ( std::unordered_set<int> observations )
    {
        observationsHistory.push_front ( observations );

        if ( observationsHistory.size() > sizeHistory )
        {
            observationsHistory.pop_back();
        }
    }

    void storeAction ( int action )
    {
        actionHistory.push_front ( action );

        if ( actionHistory.size() > sizeHistory )
        {
            actionHistory.pop_back();
        }
    }

    int storeObservationsAndGetAction ( std::unordered_set<int> observations )
    {
        storeObservations ( observations );
        return getAction();
    }

    int getAction()
    {
        PolicyTreeNode *currentNode = getCurrentNode();
        assert ( currentNode );
        assert ( !currentNode->isFringe );
        assert ( currentNode->actionProbabilities.size() == numActions );
        int action = sample ( currentNode->actionProbabilities );
        if ( useSoftMax )
        {
            double r = ( double ) rand() / RAND_MAX;
            if ( r < minActionProbability * numActions )
            {
                action = rand() % numActions;
            }
        }
        return action;
    }

    void processAction ( int action, double reward )
    {
        PolicyTreeNode *currentNode = getCurrentNode();
        assert ( currentNode );
        assert ( !currentNode->isFringe );
        assert ( currentNode->actionProbabilities.size() == numActions );
        currentNode->updateZ ( action );

        averageRewardCount++;
        averageReward += ( reward - averageReward ) / averageRewardCount;
        totalReward += reward;

        if ( useGPOMDP )
        {
            double baseLine = 0;

            if ( episodeLength < averageRewardAtTime.size() )
            {
                baseLine = averageRewardAtTime[episodeLength];
            }

            if ( useBaseLine )
            {
                updateGPOMDPGradient ( reward - baseLine );
            }
            else
            {
                updateGPOMDPGradient ( reward );
            }
        }

        if ( episodeLength < averageRewardAtTime.size() )
        {
            averageRewardAtTime[episodeLength] += ( ( double ) 1 / ( episodeCount + 1 ) ) * ( reward - averageRewardAtTime[episodeLength] );
        }
        else
        {
            assert ( episodeLength == averageRewardAtTime.size() );
            averageRewardAtTime.push_back ( reward / ( episodeCount + 1 ) );
        }

        storeAction ( action );
        episodeLength++;
    }

    void processEpisode()
    {
        episodeCount++;
        averageTotalReward += ( totalReward - averageTotalReward ) / episodeCount;

        if ( !useGPOMDP )
        {
            if ( useBaseLine )
            {
                if ( useTotalReward )
                {
                    updateREINFORCEGradient ( totalReward - averageTotalReward );
                }
                else
                {
                    updateREINFORCEGradient ( totalReward - averageReward * episodeLength );
                }
            }
            else
            {
                updateREINFORCEGradient ( totalReward );
            }
        }

        if ( optimizing )
        {
            countSteps++;
            performGradientStep();

            if ( countSteps == optimizationSteps )
            {
                optimizing = false;
                countSteps = 0;
            }
        }
        else
        {
            countSteps++;
            storeGradient();
            if ( countSteps % gradientAverageWindow == 0 )
            {
                std::pair<PolicyTreeNode *, std::pair<double, std::pair<int, int> > > ret = checkForDivergingNodes();
                if ( ret.first && ret.second.second.first >= 0 )
                {
                    assert ( ret.second.second.second >= 0 );
                    ret.first->promoteObservationFringeNode ( ret.second.second.first, ret.second.second.second );
                    resetGradients();
                    countSteps = 0;
                    optimizing = true;
                }
            }
        }

        cleanUpAfterEpisode();
        episodeLength = 0;
        totalReward = 0;
        resetHistory();
    }


    std::pair<PolicyTreeNode *, std::pair<double, std::pair<int, int> > > checkForDivergingNodes()
    {
        return checkForDivergingNodes ( root );
    }

    std::pair<PolicyTreeNode *, std::pair<double, std::pair<int, int> > > checkForDivergingNodes ( PolicyTreeNode *node )
    {
        std::pair<PolicyTreeNode *, std::pair<double, std::pair<int, int> > > ret;
        ret.first = NULL;
        ret.second.first = 0;
        ret.second.second.first = -1;
        ret.second.second.second = -1;

        if ( node->children.size() == 0 )
        {
            ret.first = node;
            ret.second = node->checkForDivergingNodes();
        }
        else
        {
            for ( int i = 0; i < node->children.size(); i++ )
            {
                std::pair<PolicyTreeNode *, std::pair<double, std::pair<int, int> > > childRet = checkForDivergingNodes ( node->children[i] );
                if ( childRet.second.first > ret.second.first )
                {
                    ret = childRet;
                }
            }
        }

        return ret;
    }

    void updateGPOMDPGradient ( double reward )
    {
        updateGPOMDPGradient ( root, reward );
    }

    void updateGPOMDPGradient ( PolicyTreeNode *node, double reward )
    {
        if ( node->children.size() == 0 )
        {
            node->updateGPOMDPGradient ( reward );
        }
        else
        {
            for ( int i = 0; i < node->children.size(); i++ )
            {
                updateGPOMDPGradient ( node->children[i], reward );
            }
        }
    }

    void updateREINFORCEGradient ( double totalReward )
    {
        updateREINFORCEGradient ( root, totalReward );
    }

    void updateREINFORCEGradient ( PolicyTreeNode *node, double totalReward )
    {
        if ( node->children.size() == 0 )
        {
            node->updateREINFORCEGradient ( totalReward );
        }
        else
        {
            for ( int i = 0; i < node->children.size(); i++ )
            {
                updateREINFORCEGradient ( node->children[i], totalReward );
            }
        }
    }

    void storeGradient()
    {
        storeGradient ( root );
    }

    void storeGradient ( PolicyTreeNode *node )
    {
        if ( node->children.size() == 0 )
        {
            node->storeGradient();
        }
        else
        {
            for ( int i = 0; i < node->children.size(); i++ )
            {
                storeGradient ( node->children[i] );
            }
        }
    }

    void resetGradients()
    {
        resetGradients ( root );
    }

    void resetGradients ( PolicyTreeNode *node )
    {
        if ( node->children.size() == 0 )
        {
            node->resetGradients();
        }
        else
        {
            for ( int i = 0; i < node->children.size(); i++ )
            {
                resetGradients ( node->children[i] );
            }
        }
    }

    void performGradientStep()
    {
        performGradientStep ( root );
    }

    void performGradientStep ( PolicyTreeNode *node )
    {
        if ( node->children.size() == 0 )
        {
            node->performGradientStep();
        }
        else
        {
            for ( int i = 0; i < node->children.size(); i++ )
            {
                performGradientStep ( node->children[i] );
            }
        }
    }

    void cleanUpAfterEpisode()
    {
        cleanUpAfterEpisode ( root );
    }

    void cleanUpAfterEpisode ( PolicyTreeNode *node )
    {
        if ( node->children.size() == 0 )
        {
            node->cleanUpAfterEpisode();
        }
        else
        {
            for ( int i = 0; i < node->children.size(); i++ )
            {
                cleanUpAfterEpisode ( node->children[i] );
            }
        }
    }

    void resetHistory()
    {
        observationsHistory.clear();
        actionHistory.clear();

        for ( int i = 0; i < sizeHistory; i++ )
        {
            storeObservations ( std::unordered_set<int>() );
            storeAction ( 0 );
        }
    }
};

%File: formatting-instruction.tex
\documentclass[letterpaper]{article}
\usepackage{aaai}
\usepackage{times}
\usepackage{helvet}
\usepackage{courier}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage{float}
\usepackage{subcaption}

\newcommand{\sgn}{\operatorname{sgn}}

\frenchspacing
\setlength{\pdfpagewidth}{8.5in}
\setlength{\pdfpageheight}{11in}
\pdfinfo{
/Title (Policy Tree)
/Author (Ujjwal Das Gupta, Michael Bowling, Erik Talvitie)}
\setcounter{secnumdepth}{2}  
 \begin{document}
% The file aaai.sty is the style file for AAAI Press 
% proceedings, working notes, and technical reports.
%
\title{Policy Tree: Adaptive Representation for Policy Gradient}
\author{Paper ID: 1958}
\maketitle
\begin{abstract}
\begin{quote}
Much of the focus on finding good representations in reinforcement learning has been on learning complex non-linear predictors of value. Methods like policy gradient, that do not learn a value function and instead directly represent policy, often need fewer parameters to learn good policies. However, they typically employ a fixed parametric representation that may not be sufficient for complex domains. This paper introduces the Policy Tree algorithm, which can learn an adaptive representation of policy, in the form of a decision tree over different instantiations of a base policy. Policy gradient is used both to optimize the parameters and to grow the tree, by choosing splits that enable the maximum local increase in the expected return of the policy. Experiments show that this algorithm can choose genuinely helpful splits and significantly improve upon the commonly used linear Gibbs softmax policy, which we choose as our base policy.
\end{quote}
\end{abstract}

\section{Introduction}
\label{sec:introduction}

The search for state representation in reinforcement learning is a challenging problem. Value function based algorithms are not guaranteed to work well in the presence of partial observability \cite{singh1994learning} or when function approximation is used \cite{boyan1995generalization}, and so learning an accurate representation of state is important.

A way to get an adaptive representation of state is to learn a decision tree over the history of observations. The U-Tree algorithm \cite{mccallum1996learning} is an example of this. It starts with a single node to represent state, and recursively performs statistical tests to check if there exists a split for which the child nodes have significantly different value. The resulting tree represents a piecewise constant estimation of value. Some enhancements to U-Tree include alternative heuristics for growing the decision tree \cite{au2004automatic}, and extensions to continuous \cite{uther1998tree} and relational \cite{dabney2007utile} domains.

Such an algorithm would grow the tree whenever doing so improves the value estimate. It is possible for an entire branch of the tree to contain multiple splits over fine distinctions of value, even if the optimum action from each of the nodes in the branch is the same. For example, Figure \ref{fig:simple_tree} shows a simple grid world with a single terminal state, indicated by a circle, transitioning to which gives a reward of $1$. The agent is equipped with four sensors to detect whether there is a wall in each direction. Assuming a discount factor of $0.5$, the values of the states are shown in each grid cell. To represent an accurate value function, a decision tree would need to split on current as well as past observations, and eventually distinguish between all the states. In complex domains, one would end up learning a very large tree. In contrast, if we were to represent the policy itself as a function over the observations, we could get a simple and optimal policy in the form of the decision tree on the right.

\begin{figure}[t]
	\includegraphics[scale=0.25]{decisionTree.pdf}
	\caption{A simple world, and a corresponding policy tree}
    \label{fig:simple_tree}
\end{figure}


Policy gradient algorithms are an alternate approach to control in reinforcement learning, which directly optimize a parametrized policy, instead of trying to approximate a value function. Some commonly used algorithms of this type are REINFORCE \cite{williams1992simple} and GPOMDP \cite{baxter2000direct}. Unlike value-based methods, they are guaranteed to converge even when complete information about the state is unavailable. As the previous example demonstrates, the policy function is often simpler and requires fewer parameters than the value function. However, state of the art policy gradient algorithms use a fixed parametrization, with less work on how the policy representation could be learned or improved.

One notable work on adaptive (or non-parametric) representation for policy gradient includes the NPPG algorithm \cite{kersting2008non}. In each iteration of the algorithm, a regression model is learned over a batch of data, with the functional gradient as its target. The final policy is a weighted sum of these models. The regression model can be any complex function of the data, including decision trees. A disadvantage of this method is that each gradient step adds a new model to the policy, increasing the computational cost of action selection, and degrading the generalization ability \cite{da2014napping}. Additionally, the functional gradient, as the derivative of the value, could be as complex as the value function itself. And so, as with value-based representation learning, a more complex representation may be learned than is necessary.

In this paper, we describe a simpler approach to adaptive representation for policy gradient, which we call the Policy Tree algorithm. It aims to directly learn a function representing the policy, avoiding representation of value. This function takes the form of a decision tree, where the decision nodes test single feature variables, and the leaves of the tree contain a parametrized representation of policy. This kind of representation has been studied in regression and classification scenarios \cite{gama2004functional}, but not in reinforcement learning. The tree is grown only when doing so improves the expected return of the policy, and not to increase the prediction accuracy of a value function or a gradient estimate. 


\section{Reinforcement Learning \& Policy Gradient}
\label{sec:reinforcement_learning}

The reinforcement learning problem we consider involves an agent which interacts with its environment at discrete time steps $t \in \{1, 2, 3, ...\}$. At each time step, the agent perceives its environment via a vector of features $\boldsymbol{\phi}$, selects an action $a \in \boldsymbol{A}$, and receives a scalar reward $r$.

The agent chooses an action $a$ at each time step by sampling a probability distribution  $\pi_{\boldsymbol{\theta}}(a | \boldsymbol{\phi})$, where $\boldsymbol{\theta}$ represents an internal parametrization of its policy. In episodic tasks, the cumulative sum of rewards over an episode is usually defined as the return of an episode. In continuing tasks, this sum is unbounded, and the return is defined to be either the average reward per time step or a discounted sum of rewards. The goal of a learning agent is to find the policy parameters which maximize the expected return from its interaction with the environment, denoted as $\eta(\boldsymbol{\theta})$. 

Policy gradient works by applying gradient ascent to find a locally optimal solution to this problem. Note that computing the gradient $\nabla_{\boldsymbol{\theta}}\eta(\boldsymbol{\theta})$ would involve an expectation over observed rewards, with the underlying probability distribution being a function of both the policy and the model of the environment. The model is unknown to the agent, but a sample estimate of the gradient can be obtained by observing trajectories of observations and rewards, while acting on-policy. This is the principle which underlies all policy gradient algorithms, like GPOMDP and REINFORCE, also known as actor-only methods. Methods which additionally use a critic, like Actor Critic \cite{sutton1999policy} and Natural Actor Critic \cite{peters2008natural}, maintain a value function estimate in the form of a compatible function approximation, which is used to reduce the variance of the gradient estimate.

The actual policy can be any valid probability distribution over actions that is smooth or differentiable with respect to all its parameters. That is, $\nabla_{\boldsymbol{\theta}} \pi_{\boldsymbol{\theta}}(a | \boldsymbol{\phi})$ should exist.  A commonly used parametrization is the linear Gibbs softmax policy:
\begin{equation} \label{eq:policy_softmax}
\pi_{\boldsymbol{\theta}}(a | \boldsymbol{\phi}) = \frac{\exp({\boldsymbol{\theta_{a}}}^T \boldsymbol{\phi})}{\sum_{i=1}^{| \boldsymbol{A} |} \exp({{\boldsymbol{\theta_{i}}^T \boldsymbol{\phi}}})},
\end{equation}
where $\boldsymbol{\theta_a}$ is a vector of parameters corresponding to action $a$. 

Another possible parametrization consists of a single scalar parameter $\theta_a$ per action. This corresponds to a multi-armed bandit agent, which has a fixed probability of taking a certain action, independent of the observations:
\begin{equation} \label{eq:policy_bandit}
\pi_{\boldsymbol{\theta}}(a | \boldsymbol{\phi}) = \frac{\exp(\theta_{a})}{\sum_{i=1}^{| \boldsymbol{A} |} \exp(\theta_{i})}.
\end{equation}


\section{The Policy Tree Algorithm}
\label{sec:algorithm}

The Policy Tree algorithm consists of a base parametric policy and a binary decision tree. We assume that the features are $D$-dimensional binary vectors $\boldsymbol{\phi} \in \{0,1\}^D $. There exist methods, such as Coarse Coding, Tile Coding or Kanerva Coding \cite{sutton1998introduction}, which can convert a space of real valued parameters into such a binary vector space.

The internal nodes of the tree are decisions on an element of the feature vector. We call the index of this element as the decision index of the node. Every feature vector $\boldsymbol{\phi}$ maps to one leaf node $l(\boldsymbol{\phi})$ in the tree, which is associated with a real valued parameter vector $\boldsymbol{\theta_{l(\phi)}}$. The base policy at the leaf is denoted by $\pi_{\boldsymbol{\theta_{l(\phi)}}}(a | \boldsymbol{\phi})$. This could be the linear Gibbs softmax or the multi-armed bandit policy, as described in Equations \ref{eq:policy_softmax} and \ref{eq:policy_bandit}, or any other valid parametrization of policy.

The high level procedure can be described as:

\begin{enumerate}
  \item Start with a single-node decision tree, with its root node containing a randomly initialized parametrization of the base policy.
  \item Optimize all leaf node parameters using policy gradient for a fixed number of episodes or time steps.
  \item Choose a leaf node and an observation index to split, according to our tree growth criterion. The parameters are kept fixed for a number of episodes or time steps, while the merit of each split is judged. Create two new children of this node, which inherit the same policy parameters as their parent. Go to step $2$ and repeat.
\end{enumerate}

We describe in detail the steps $2$ and $3$ of the algorithm in the following sections.

\subsection{Parameter Optimization}

During this phase, the tree structure is kept fixed, while the parameters are optimized using a policy gradient algorithm. Note that $\frac{\partial}{\partial \theta}\pi_{\boldsymbol{\theta_{l(\phi)}}}(a | \boldsymbol{\phi})$ is known to exist for $\theta \in \boldsymbol{\theta_{l(\phi)}}$, and is trivially $0$ for $\theta \notin \boldsymbol{\theta_{l(\phi)}}$, as $\pi_{\boldsymbol{\theta_{l(\phi)}}}(a | \boldsymbol{\phi})$ is not a function of any such parameter. 

The parameters can thus be optimized using any policy gradient technique. It is possible to use an actor-critic method here. However, as we do not assume that a compatible function approximation can represent the value function accurately, the critic would have an uncertain effect on the variance of the gradient estimate. The per-step computational complexity during this phase depends on the actual algorithm and parametrization used. For most policy gradient algorithm, this would be linear in the number of parameters, which in our case is $\mathcal{O}(N_L N_P)$, where $N_L$ is the number of leaf nodes and $N_P$ is the number of parameters in the base policy.

\subsection{Tree Growth}

In this phase, the structure of the tree is altered by splitting one of the leaf nodes, changing the underlying representation. In order to choose a good candidate split, we would like to know the global effect of a split after optimizing the resulting tree. This would require making every candidate split and performing parameter optimization in each case, which is unrealistic and inefficient. However, if we suggest a candidate split, and keep its parameters fixed, the gradient of the expected return of this new policy function gives us a first order approximation of the expected return. This approximation is valid within a small region of the parameter space, and can be used to measure the local effect of the split. This is the basis of our criterion to grow the tree. First, we describe a method to calculate the gradients corresponding to every possible split in the tree.

A valid addition to the policy tree involves a split on one of the leaf nodes, on a parameter $k \in [1,D]$, such that $k$ is not a decision index on the path from the leaf node to the root. For every leaf node $L$ in the tree, and for every valid index $k$ in $[1,D]$, a pair of fringe child nodes are created, denoted as $F_{L,k}$ and $F^{'}_{L,k}$. They represent the child nodes of $L$ which would be active when $o_k = 1$ and $o_k = 0$, respectively. Both of these nodes are associated with a parameter vector which is the same as that of the parent leaf node, that is, for all $L$ and $k$:
\begin{equation} \label{eq:fringe_leaf_equality}
\boldsymbol{\theta_{F_{L,k}}} = \boldsymbol{\theta_{F^{'}_{L,k}}} = \boldsymbol{\theta_{L}} .
\end{equation}

Let $\boldsymbol{\psi_{L,k}}$ denote the combined vector of all the parameters associated with the tree, when it is expanded to include the pair of fringe nodes $F_{L,k}$ and $F^{'}_{L,k}$.  Note that each such vector corresponds to a different policy function, which we denote by $\pi_{\boldsymbol{\psi_{L,k}}}$. Let $\eta(\boldsymbol{\psi_{L,k}})$ denote the corresponding expected return.

Equation \ref{eq:fringe_leaf_equality} ensures that $\pi_{\boldsymbol{\psi}_{l(\phi),k}}(a | \boldsymbol{\phi}) = \pi_{\boldsymbol{\theta_{l(\phi)}}}(a | \boldsymbol{\phi})$, which means that all these policies have the same distribution over actions as the one represented by the existing policy tree, even though the underlying representation has changed. This ensures that we can measure the correct sample estimate of the gradient $\nabla_{\boldsymbol{\psi_{L,k}}} \eta(\boldsymbol{\psi_{L,k}})$ by following the policy of the tree. It is important to obtain a good estimate of these gradients, to avoid splitting on noise. Therefore, during this phase, the policy is kept fixed while a suitably large number of trajectories are observed, and $\nabla_{\boldsymbol{\psi_{L,k}}} \eta(\boldsymbol{\psi_{L,k}})$ is estimated for all $L$ and $k$.

We choose the leaf node $L$ to split on, and the split index $k$ by the following procedure:
\begin{equation} \label{eq:best_fringe}
L,k = \operatorname*{arg\,max}_{L,k} \; || \nabla_{\boldsymbol{\psi_{L,k}}} \eta(\boldsymbol{\psi_{L,k}}) ||_q .
\end{equation}

\begin{figure}
	\centerline{\includegraphics[scale=0.4]{normBalls.pdf}}
	\caption{The $p$-norm spheres representing $||\boldsymbol{\Delta \theta} ||_p = \epsilon$}
    \label{fig:norm_sphere}
\end{figure}

It is worth reflecting on the interpretation of the $q$-norm of the gradient vector, in order to interpret our criterion. By using a first order Taylor expansion of the expected return, we can measure the change corresponding to a tiny step $\boldsymbol{\Delta \psi}$:
\begin{equation} \label{eq:gradient_taylor}
\begin{aligned}
\eta(\boldsymbol{\psi_{L,k}} + \boldsymbol{\Delta \psi}) = \eta(\boldsymbol{\psi_{L,k}}) + \nabla_{\boldsymbol{\psi_{L,k}}} \eta(\boldsymbol{\psi_{L,k}})^T \boldsymbol{\Delta \psi}.
\end{aligned}
\end{equation}

If we constrain $\boldsymbol{\Delta \psi_{L,k}}$ to lie within a small $p$-norm sphere, we have:
\begin{equation} \label{eq:gradient_duality}
\begin{aligned}
\operatorname*{max}_{\boldsymbol{\Delta \psi}} \; \nabla_{\boldsymbol{\psi_{L,k}}} &\eta(\boldsymbol{\psi_{L,k}})^T \boldsymbol{\Delta \psi} = ||\nabla_{\boldsymbol{\psi_{L,k}}} \eta(\boldsymbol{\psi_{L,k}})||_q \epsilon\\ 
\text{s.t.} \; \; || \boldsymbol{\Delta \psi} &||_p \leq \epsilon \\
&\; \; \; \; \text{where,} \; \frac{1}{p} + \frac{1}{q} = 1.
\end{aligned}
\end{equation}

This shows that the $q$-norm of the gradient represents the maximum change in the objective function within a local region of the parameter space bounded by the $p$-norm sphere. Figure \ref{fig:norm_sphere} shows a graphical representation of various $p$-norm spheres. 

As we need to measure $N_L D$ gradients involving $N_P$ parameters, the per-step computational complexity during this phase for most policy gradient algorithms would be $\mathcal{O}(N_P N_L D)$. Note that the length of this phase will almost always be considerably lower than the previous one, as we simply want a high accuracy gradient estimate and do not need to optimize the parameters.

\subsection{Fringe Bias Approximation}

For some base policies, the number of parameters will increase with the number of features, making the complexity of the tree growth phase quadratic (or worse) in the number of features.  Here, we describe an approximation which can reduce this complexity, and applies particularly when we have a discrete set of actions $A$, and use some function of a linear transformation of the features as our base policy.

The standard practise when defining a linear function is to augment the input vector $\boldsymbol{\phi}$ with a bias term, usually chosen as the first term of the vector. This term, denoted as $\phi_1$, is always $1$. If we had to choose a few components to represent the gradient of the fringe parameters, choosing the parameters associated with $\phi_1$ is a reasonable choice. 

To apply this approximation to the Gibbs policy in Equation \ref{eq:policy_softmax} as the base, we compute $\frac{\partial}{\partial \theta_{{F_{L,k}a1}}} \eta(\boldsymbol{\psi_{L,k}})$ for all actions $a \in \boldsymbol{A}$, and set the other terms of the gradient to zero. The tree growth criterion remains the same, which is to measure the norm of the gradient in this reduced space. The computational complexity becomes $\mathcal{O}(N_L D |\boldsymbol{A}|)$. As $N_P = D|\boldsymbol{A}|$ for this policy, the per-step complexity for tree growth becomes the same as that for parameter optimization.

\section{Experiments}
\label{sec:experiments}

We wish to evaluate the following questions empirically:

\begin{enumerate}
  \item Can the Policy Tree improve over the base policy?
  \item How well does the fringe bias approximation work?
  \item Is the improvement merely due to an increase in the number of parameters, or does Policy Tree choose intelligent splits?
\end{enumerate}

To answer these questions, we consider a set of domains inspired by arcade games. 

\subsection{Domains}

Our test suite is a set of $4$ simple games, which have a $16$x$16$ pixel game screen with $4$ colours. A pictorial representation of them is presented in Figure \ref{fig:games}. All of these games are episodic with a maximum episode length of $256$ time steps, and every object moves with a speed of one pixel per step. Objects in these games, including the player agent, enemy agents, friendly agents or bullets are a single pixel in size, and each object type is of a distinct colour. Unless specified otherwise, the actions available to the agent are to move up, down, left, right or stay still. The objective of the learning agent is to maximize the expected undiscounted return.

\begin{figure}[t]
	\includegraphics[scale=0.26]{Games2.png}
	\caption{The graphical representation of the games. Clockwise from top-left, they are Monsters, Switcheroo, Mothership and Rescue}
    \label{fig:games}
\end{figure}

\begin{figure}[t]
	\includegraphics[scale=0.25]{LogarithmicRelativeMapping4.png}
	\caption{The feature mapping process.}
    \label{fig:feature_generation}
\end{figure}

For generating the binary feature vector, the location of the player agent is assumed to be known. We divide the game screen into zones, the size of which grows exponentially with the distance from the agent. Formally, if $(x,y)$ represents the displacement of a pixel from the agent, all pixels with the same value of $\left \lfloor{\log_2|x|}\right \rfloor$, $\left \lfloor{\log_2|x|}\right \rfloor$, $\sgn(x)$ and $\sgn(y)$ belong to the same zone. For each zone, we have a feature corresponding to every colour used in the game. A feature is set to $1$ if an object of the corresponding colour is found in that zone. An example of feature generation is shown in Figure \ref{fig:feature_generation}. The left image shows a game screen for Mothership, with the black lines representing the zones centred around the agent. The right screen indicates the active features in each zone via coloured dots. $7$ features are active in this example. Such a choice of features allows fine distinctions to be made between objects near the agent, while ensuring that the feature size grows only logarithmically with the size of the game screen.

\subsubsection{Monsters:}
The player agent is green. There are three red monsters and three blue power suits randomly located in the game area. The agent can collect and wear a power suit, allowing it to kill the next monster it collides with for one point. A power suit lasts a maximum of 16 time steps once it is worn. The monsters chase the agent when it is not powered, and stay still otherwise.

\subsubsection{Switcheroo:}
There are two types of objects, red and blue. At regular intervals, groups of $8$ objects of a randomly chosen type move across the screen horizontally. The player agent starts in the center of the screen, as an object of a random type. Hitting objects of the other type terminates the episode, while hitting an object of the same type earns a point. After a point is earned, the agent switches to the other type.

\subsubsection{Mothership:}
The blue mothership sits at the top of the screen, while $4$ rows of $8$ red guard ships patrol horizontally below. The green player agent is constrained to the bottom of the screen, and cannot move up or down. It needs to shoot the mothership to collect $5$ points, and then shoot the guard ships for $1$ point each. Shooting the guards before destroying the mothership simply reflects bullets back at the agent, and alerts the enemy, causing the mothership to move randomly, and the guard ships to shoot bullets. The agent is only allowed to have one active bullet at a time, and there is a random wait of $0$ or $1$ time steps after its bullet is consumed. All the bullets are coloured yellow.

\subsubsection{Rescue:}
The green player agent has to collect yellow hostages and bring them to the blue rescue location to collect a point. The rescue location is randomly located after every successful rescue, and the player starts from a random location at the top of the screen. The hostages and red enemies move across the screen horizontally at random intervals, and collision with the enemies results in instant episode termination.

These games contain elements of partial observability, and non-linearity in the optimum policy function. As examples, the direction of objects in the games cannot be determined from a single game screen, and the best direction to move in Monsters is conditional on multiple variables.

\subsection{Experimental Setup}

We use the linear Gibbs softmax policy as the base policy for our algorithm, augmented with $\epsilon$-greedy exploration. This policy is defined as:

\begin{equation} \label{eq:policy_softmax_epsilon_greedy}
\pi_{\boldsymbol{\theta_L}}(a | \boldsymbol{\phi}) = (1-\epsilon)\frac{e^{{\boldsymbol{\theta_{L,a}}}^T \boldsymbol{\phi}}}{\sum_{i=1}^{| \boldsymbol{A} |} e^{{\boldsymbol{\theta_{L,i}}^T \boldsymbol{\phi}}}} + \frac{\epsilon}{|\boldsymbol{A}|}.
\end{equation}

The $\epsilon$ term ensures that the policy is sufficiently stochastic, even if $\theta$ takes on heavily deterministic values. This allows accurate estimation of the gradient at all times, and ensures that the policy does not quickly converge to a poor deterministic policy due to noise. Policy Tree benefits a great deal from exploration, as often highly optimized parameters are inherited from parent nodes, and need to be re-optimized after a split. We noticed that adding this exploration term helped the base policy as well. 

We compare four different algorithms on these domains. The first is the standard Policy Tree algorithm with the $\epsilon$-greedy Gibbs policy as the base. The second is a version with the fringe bias approximation enabled. The third is a version of the algorithm which chooses a random split during tree growth, for the purpose of testing whether the Policy Tree is just benefiting from having a larger number parameters, or whether it makes good representation choices. And finally, we test the base policy, which represents the standard parametric approach to policy gradient.

For Policy Tree, we used a parameter optimization stage of $49000$ episodes, and a gradient averaging phase during tree growth of $1000$ episodes. Splits are therefore made after every $50000$ episodes. The value of $\epsilon$ used was $0.001$. For the tree growth criterion, we choose the $q=1$ norm of the gradient.

We use the episodic GPOMDP algorithm with no discounting to measure the gradient in all of the algorithms. This is known to have lower variance than other actor-only algorithms like REINFORCE. Additionally, we use the optimal baseline for reducing variance \cite{weaver2001optimal}. The optimal learning rates for all the algorithms were obtained via a parameter sweep for each domain.

For each algorithm, we performed $10$ different runs of learning over $500000$ episodes. The average return was measured as the moving average of the total undiscounted return per episode with a window length of $50000$.

\subsection{Results}

\begin{figure}
	\centering
	\subcaptionbox{Monsters}
		{\includegraphics[scale=0.6]{Monsters.pdf}}
	\subcaptionbox{Switcheroo}
		{\includegraphics[scale=0.6]{Switcheroo.pdf}}
	\subcaptionbox{Mothership}
		{\includegraphics[scale=0.6]{Mothership.pdf}}
	\subcaptionbox{Rescue}
		{\includegraphics[scale=0.6]{Rescue.pdf}}
	\caption{Results}
	\label{fig:results}
\end{figure}


The learning curves of the Policy Tree algorithm as compared to the base policy are shown in Figure \ref{fig:results}. The standard error in the results, across the $10$ runs, is represented by the vertical error bars in the graphs. These results allow us to answer the questions posed earlier:

\begin{enumerate}
\item The Policy Tree algorithm improves upon the underlying base policy with statistical significance.
\item The fringe bias approximation does not do as well the exact measure in most domains, but also improves over the linear parametrization significantly, without enduring additional computational complexity during tree growth. 
\item An arbitrary increase in the number of parameters via the random splitting does not improve performance at all. This shows that the our tree growth criterion contributes significantly to the effectiveness of the algorithm.
\end{enumerate}

The Monsters domain shows the biggest improvement, as well as the greatest gap between the exact and approximate versions. In this domain, the feature that represents whether or not the agent is powered is very informative. Policy Tree chooses this as the first split in $80\%$ of the runs, while this drops to $20\%$ with the approximation enabled. However, we observed that the approximate version outperforms the base policy even when this split is never made, indicating that the chosen splits are not meaningless.

\section{Future Work}
\label{sec:future_work}

There are a number of areas in which this work can be expanded. The current version of our algorithm has fixed lengths for the parameter optimization and tree growth phases. One could test for convergence of the objective function during optimization, allowing the tree to get the best possible performance before we try to expand the structure. However, highly optimized policies tend to be highly deterministic, making re-optimization of the parameters after a split trickier. The Gibbs policy, for instance, has a low gradient in regions of high determinism. It is possible that the use of natural gradients would alleviate this problem, by measuring the change of the objective function with respect to the actual change in the policy distribution, rather than the change in parameter values.

Due to the presence of noise in the gradients, or due to the locality of our improvement measuring criterion, it is possible that some splits do not enhance the policy significantly. This causes needless increase in the number of parameters, and slows down learning by splitting the data available to each branch. A possible fix for this would be to prune the tree if optimization after a split does not significantly change the parameters.

The splits in the decision tree are currently based on the value of a single observation variable. In general, we could define a split over a conjunction or a disjunction of observation variables. This would increase the size of the fringe used during tree growth, but would allow the algorithm to find splits which may be significantly more meaningful. A different kind of split in the decision nodes would be on a linear combination of features. This can be viewed as a split on a non-axis aligned hyperplane in the observation space. As there are infinite such splits, it is not possible to measure them using our fringe structure. However, there may be ways to develop alternative criterion in order to grow a tree with such a representation. Prior research suggests that such an architecture is useful for classification but not for regression \cite{breiman1984classification}. It is unclear how useful it would be in the search for optimal policy.

The two phase structure of our algorithm is slightly sample inefficient, as the experience during tree growth is not used to optimize the parameters. In our experiments, $2\%$ of the episodes are unused for optimization. Due to the requirement to stay on-policy to estimate the gradient, this is difficult to avoid. One possible solution would be to use importance sample weighting and utilize off-policy trajectories to compute the gradient. This would in fact avoid the necessity of keeping the policy fixed during the tree growth stage. The use of importance sampling in policy gradient has been studied previously \cite{jie2010connection}. However, the weights for the off-policy samples would reduce exponentially with the horizon of the problem, and we cannot be certain whether it is possible to have a reliable off-policy estimate of the gradient in most domains.

\section{Conclusion}
\label{sec:conclusion}

We presented the Policy Tree algorithm, and demonstrated its utility on a variety of domains inspired by games. To the best of our knowledge, this is the first algorithm which can learn a decision tree to represent the policy in a model-free setting. This algorithm has the same convergence guarantees as parametric policy gradient methods, but can adapt its representation whenever doing so improves the policy. We also present an approximate form of this algorithm with the Gibbs policy, which has a linear computational complexity per time-step in the number of parameters during the entire learning phase.

\bibliographystyle{aaai}
\bibliography{policytree}

\end{document}

%File: formatting-instruction.tex
\documentclass[letterpaper]{article}
\usepackage{aaai}
\usepackage{times}
\usepackage{helvet}
\usepackage{courier}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage{float}
\frenchspacing
\setlength{\pdfpagewidth}{8.5in}
\setlength{\pdfpageheight}{11in}
\pdfinfo{
/Title (Policy Tree)
/Author (Ujjwal Das Gupta, Michael Bowling, Erik Talvitie)}
\setcounter{secnumdepth}{0}  
 \begin{document}
% The file aaai.sty is the style file for AAAI Press 
% proceedings, working notes, and technical reports.
%
\title{Policy Tree: Adaptive Representation for Policy Gradient}
\author{Anonymous}
\maketitle
\begin{abstract}
\begin{quote}
Much of the focus on finding good representations in reinforcement learning has been on learning complex non-linear predictors of value. Methods like policy gradient,that do not learn a value function and instead directly represent policy, often need fewer parameters to learn good policies, but typically employ a fixed parametric representation that may not be sufficient for complex domains. This paper introduces the policy tree algorithm, which can learn an adaptive representation of policy, in the form of a decision tree over different instantiations of a base policy. Policy gradient is used both to optimize the parameters and to grow the tree, by choosing splits which enable the maximum local increase in the expected return of the policy. Experiments show that this algorithm can choose genuinely helpful splits and significantly improve upon the commonly used Gibbs linear softmax policy, which we choose as our base policy.
\end{quote}
\end{abstract}

\section{Introduction}

In traditional value-based Reinforcement Learning, there has been plenty of work on automatically adapting state representation. A common approach is to use a decision tree over the observations to represent a piecewise constant value function. The U-Tree algorithm \cite{mccallum1996reinforcement} is an example of this. It starts with a single node to represent state, and recursively performs statistical tests to check if there exists a split for which the child nodes have significantly different value. Some enhancements along this line of research include alternative heuristics for growing the decision tree \cite{au2004automatic}, and extensions to continuous \cite{uther1998tree} and relational \cite{dabney2007utile} domains.

Such an algorithm would grow the tree whenever doing so improves the value estimate. It is possible for an entire branch of the tree to contain multiple splits over fine distinctions of value, even if the optimum action from each of the nodes in the branch is the same. For example, Figure \ref{fig:simple_tree} shows a simple block world which has one terminal state, indicated by a circle, transitioning to which gives a reward of $1$. Assuming a discount factor of $0.5$, the values of each state are marked on it. A decision tree on values would necessarily distinguish between all the states, although the decision tree on the right, which makes just a single distinction, correctly represents the optimal policy.

\begin{figure}
	\includegraphics[scale=0.25]{decisionTree.pdf}
	\caption{A simple world, and a corresponding policy tree}
    \label{fig:simple_tree}
\end{figure}


Algorithms which directly optimize a parametrized policy, instead of the value function, are another approach to Reinforcement Learning. Unlike value-based methods, they do not bootstrap, and usually encounter higher variance during learning. However, they do not suffer from some of the problems affecting traditional methods, like the lack of guarantee of convergence of the value function. As the previous example demonstrates, some policies can be represented using fewer parameters than needed for estimating the value function. Still, a fixed parametrization of policy may be insufficient for complex tasks.

Previous work on adaptive (or non-parametric) representation for policy search includes the NPPG algorithm \cite{kersting2008non}. In each iteration of the algorithm, a regression model is learned over a batch of data, with the functional gradient as its target. The final policy is a weighted sum of these models. The regression model can be any complex function of the data, including decision trees. A disadvantage of this method is that each gradient step adds a new model to the policy, increasing the computational cost of action selection, and degrading the generalization ability \cite{da2014napping}.

In this paper, we describe a simpler approach for adaptive representation in policy gradient, which we call the Policy Tree algorithm. It consists of a decision tree over observations, such that the leaves of the tree contain a parametrized representation of policy, rather than a value estimate. The tree is grown only when doing so improves the expected return of the policy, and not to increase the prediction accuracy of a value function or a gradient estimate. We use the principle of policy gradient, both to optimize the parameters of the policy, and to guide the growth of the tree.

\section{The Reinforcement Learning Problem and Policy Gradient}

The Reinforcement Learning problem we consider involves an agent which interacts with its environment at discrete time steps $t \in [1,\infty)$. At each time step, the agent receives a vector of observations $\boldsymbol{o}$, selects an action $a \in \boldsymbol{A}$, and receives a scalar reward $r$.

The agent chooses an action $a$ at each time step by sampling a probability distribution  $\pi_{\boldsymbol{\theta}}(a | \boldsymbol{o})$, where $\boldsymbol{\theta}$ represents an internal parametrization of its policy. In episodic tasks, the cumulative sum of rewards over an episode is defined as the return of an episode. In continuing tasks, this sum is unbounded, and either the average reward per time step, or a discounted sum of rewards, is defined to be the return \cite{mahadevan1996average}. The goal of a learning agent is to find the policy parameters which maximize the expected return from its interaction with the environment, denoted as $\eta(\boldsymbol{\theta})$. 

Note that computing the gradient $\nabla_{\boldsymbol{\theta}}\eta(\boldsymbol{\theta})$ would involve an expectation over observed rewards, with the underlying probability distribution being a function of both the policy and the model of the environment. The model is unknown to the agent, but a sample estimate of the gradient can be obtained by observing trajectories of observations and rewards, while acting on-policy. This is the principle which underlies all policy gradient algorithms.

Some commonly used algorithms of this type are REINFORCE \cite{williams1992simple}, GPOMDP \cite{baxter2000direct}, Actor Critic \cite{sutton1999policy} and Natural Actor Critic \cite{peters2008natural}. Each of these approaches describe a method to obtain an estimate of $\nabla_{\boldsymbol{\theta}}\eta(\boldsymbol{\theta})$ from the observed trajectories. These algorithms are independent of the actual policy used, and the only requirement is that the policy should be smooth or differentiable with respect to each parameter. That is, $\nabla_{\boldsymbol{\theta}} \pi_{\boldsymbol{\theta}}(a | \boldsymbol{o})$ should exist.

A commonly used parametrization is the linear Gibbs softmax policy:

\begin{equation} \label{eq:policy_softmax}
\pi_{\boldsymbol{\theta}}(a | \boldsymbol{o}) = \frac{e^{{\boldsymbol{\theta_{a}}}^T \boldsymbol{o}}}{\sum_{i=1}^{| \boldsymbol{A} |} e^{{\boldsymbol{\theta_{i}}^T \boldsymbol{o}}}}
\end{equation}

Where $\boldsymbol{\theta_a}$ is a vector of parameters corresponding to action $a$. 

Another possible parametrization consists of a single scalar parameter $\theta_a$ per action. This corresponds to a multi-armed bandit agent, which has a fixed probability of taking a certain action, independent of the observations.

\begin{equation} \label{eq:policy_bandit}
\pi_{\boldsymbol{\theta}}(a | \boldsymbol{o}) = \frac{e^{\theta_{a}}}{\sum_{i=1}^{| \boldsymbol{A} |} e^{\theta_{i}}}
\end{equation}


\section{The Policy Tree Algorithm}

In essence, the Policy Tree algorithm consists of a decision tree and a base policy. Each leaf node of the tree is associated with an instantiation of the base policy. We start with a single node, and gradually add splits, creating base policy instantiations. The tree structure partitions the observation space into distinct regions, and in each of these, a unique policy can be learned.

The algorithm has two alternating phases: Parameter Optimization and Tree Growth. The first keeps the structure of the tree fixed, and optimizes the parameters at the leaves using Policy Gradient. The second keeps the parameters fixed, and adds a split on one of the leaf nodes. The split is chosen by measuring the gradients on the parameters which are proposed to be added to the tree. 

\subsection{Representation}

We assume that the observations are $D$-dimensional binary vectors $\boldsymbol{o} \in \{0,1\}^D $. There exist methods, such as Tile Coding \cite{sutton1998introduction}, which can convert a space of real valued parameters into a suitable binary vector space.

Every observation vector $\boldsymbol{o}$ maps to one leaf node $l(\boldsymbol{o})$ in the tree, which is associated with a real valued parameter vector $\boldsymbol{\theta_{l(o)}}$. The base policy at the leaf is denoted by $\pi_{\boldsymbol{\theta_{l(o)}}}(a | \boldsymbol{o})$. This could be the linear Gibbs softmax or the multi-armed bandit policy, which were described previously in Equations \ref{eq:policy_softmax} and \ref{eq:policy_bandit}, or any other smooth parametrization of policy.

\subsection{Parameter Optimization}

During this phase, the Policy Tree structure is kept fixed, while the parameters are optimized using a policy gradient algorithm. Note that $\frac{\partial}{\partial \theta}\pi_{\boldsymbol{\theta_{l(o)}}}(a | \boldsymbol{o})$ is known to exist for $\theta \in \boldsymbol{\theta_{l(o)}}$, and is trivially $0$ for $\theta \notin \boldsymbol{\theta_{l(o)}}$, as $\pi_{\boldsymbol{\theta_{l(o)}}}(a | \boldsymbol{o})$ is not a function of any such parameter. The Policy Tree parametrization is thus valid for optimization using policy gradient.


\subsection{Tree Growth}

A candidate addition to the policy tree involves a split on one of the leaf nodes, on a parameter $k \in [1,D]$, such that $k$ is not a decision index on the path from the leaf node to the root. This would create two new leaf nodes, which can reasonably be initialized with the same value of parameters as their parent.

The gradient of the expected return with respect to these hypothetical parameters give us some idea of the effect the split. A high gradient indicates that it is possible to significantly improve the policy, when using the candidate policy tree. We use this intuition to choose a node to split in the decision tree. First, we describe a method to calculate the gradients corresponding to every possible split at all leaf nodes.

For every leaf node $L$ in the tree, and for every observation index $k$ in $[1,D]$, a fringe child node is created, denoted as $F_{L,k}$ . This represents a potential child node of $L$, which would be active when $o_k = 1$. Each fringe node has a complement $F^{'}_{L,k}$, which represents the child which would be active when $o_k = 0$. Both the fringe and the complement nodes are associated with a parameter vector which is the same as that of the parent leaf node, that is:

\begin{equation} \label{eq:fringe_leaf_equality}
\boldsymbol{\theta_{F_{L,k}}} = \boldsymbol{\theta_{F^{'}_{L,k}}} = \boldsymbol{\theta_{L}}, \forall k \in [1,D]
\end{equation}

Let $\boldsymbol{\psi_{L,k}}$ denote the combined vector of all the fringe parameters associated with the pair of fringe nodes $F_{L,k}$ and $F^{'}_{L,k}$.  Note that each such pair of fringe parameter vectors corresponds to a different policy parametrization, which we denote by $\pi_{\boldsymbol{\psi_{L,k}}}$. Let $\eta(\boldsymbol{\psi_{L,k}})$ denote the corresponding expected return. 

In order to get the correct sample estimate of the gradient $\nabla_{\boldsymbol{\psi_{L,k}}} \eta(\boldsymbol{\psi_{L,k}})$, we need to follow the policy $\pi_{\boldsymbol{\psi}_{L,k}}$. However, Equation \ref{eq:fringe_leaf_equality} ensures that $\pi_{\boldsymbol{\psi}_{l(o),k}}(a | \boldsymbol{o}) = \pi_{\boldsymbol{\theta_{l(o)}}}(a | \boldsymbol{o})$, thereby the gradient with respect to any fringe node parameter can be obtained while following the tree policy.

It is important to obtain a good estimate of these gradients, to avoid splitting on noise. Therefore, during the tree growth phase, the policy is kept fixed while a suitably large number of trajectories are observed and $\nabla_{\boldsymbol{\psi_{L,k}}} \eta(\boldsymbol{\psi_{L,k}})$ is estimated for all $L$ and $k$.

Our motivation is to choose the split, corresponding to the maximum increase in expected return in the local region of parameter space. This measure depends on the definition of locality, and it is worth reflecting on the interpretation of the gradient vector, in order to derive the exact criterion. By definition, the $2$-norm of the gradient represents the maximum rate of change in the objective function within a $2$-norm sphere. That is:

\begin{equation} \label{eq:gradient_l2}
\begin{aligned}
\epsilon ||\nabla_{\boldsymbol{\theta}} f(\boldsymbol{\theta)}||_2   = \operatorname*{max}_{\boldsymbol{\Delta \theta}} \; &f(\boldsymbol{\theta} + \boldsymbol{\Delta \theta}) - f(\boldsymbol{\theta}) \\ 
\text{s.t.} &|| \boldsymbol{\Delta \theta} ||_2 = \epsilon
\end{aligned}
\end{equation}

This can be generalized to other norms as well. The maximum rate of change in the objective function within a $p$-norm sphere, is given by the dual norm.

\begin{equation} \label{eq:gradient_lp}
\begin{aligned}
\epsilon ||\nabla_{\boldsymbol{\theta}} f(\boldsymbol{\theta)}||_q = \operatorname*{max}_{\boldsymbol{\Delta \theta}} \; &f(\boldsymbol{\theta} + \boldsymbol{\Delta \theta}) - f(\boldsymbol{\theta}) \\ 
\text{s.t.} &|| \boldsymbol{\Delta \theta} ||_p = \epsilon \\
\text{Where,} \; \frac{1}{p} + \frac{1}{q} = 1
\end{aligned}
\end{equation}

\begin{figure}
	\centerline{\includegraphics[scale=0.4]{normBalls.pdf}}
	\caption{The $\epsilon$-norm spheres corresponding to different norms.}
    \label{fig:norm_sphere}
\end{figure}

From this we can see that the gradient allows us to measure the change in expected return within any norm sphere around the current parameter values. Figure \ref{fig:norm_sphere} gives a graphical representation of a few different norm spheres, from which it is clear that the $\infty$-norm sphere has the greatest volume, and encompasses all other spheres.

We are interested in measuring the global change in the objective function in order to judge a split. This is not possible without actually making each split and optimizing them individually. To avoid this, we simply use the gradient to measure the maximum possible local change, which lies within the $\infty$-norm sphere. From Equation \ref{eq:gradient_lp}, we can see that this change is given by the $1$-norm of the gradient. 

So, we choose the leaf node $L$ to split on, and the split index $k$ by the following procedure:

\begin{equation} \label{eq:best_fringe}
L,k = \operatorname*{arg\,max}_{L,k} \; || \nabla_{\boldsymbol{\psi_{L,k}}} \eta(\boldsymbol{\psi_{L,k}}) ||_1
\end{equation}

The newly created leaf nodes inherit their parameters from $L$.

\section{Implementation}

The Policy Tree algorithm, as defined in the previous section, is independent of the actual base policy $\pi_{\boldsymbol{\theta_{L}}}(a | \boldsymbol{o})$ used at the leaf nodes. For our results, we use a linear Gibbs softmax policy, augmented with $\epsilon$-greedy exploration. This policy is defined as:

\begin{equation} \label{eq:policy_softmax_epsilon_greedy}
\pi_{\boldsymbol{\theta_L}}(a | \boldsymbol{o}) = (1-\epsilon)\frac{e^{{\boldsymbol{\theta_{L,a}}}^T \boldsymbol{o}}}{\sum_{i=1}^{| \boldsymbol{A} |} e^{{\boldsymbol{\theta_{L,i}}^T \boldsymbol{o}}}} + \frac{\epsilon}{|\boldsymbol{A}|}
\end{equation}

$\boldsymbol{\theta_{L,a}}$ is a vector of parameters at leaf node $L$ for each action $a$. The presence of the $\epsilon$ term ensures that each action has, at minimum, an $\frac{\epsilon}{|\boldsymbol{A}|}$ chance of being selected. 
A reason for this choice is that the parameters in $\boldsymbol{\theta_{L}}$ may be set to values which result in a highly deterministic softmax term. These parameters may be inherited by future child nodes, and could need to be re-optimized to different values. The $\epsilon$-greedy exploration ensures that the policy is sufficiently stochastic, allowing a good sample estimate of $\nabla_{\boldsymbol{\theta_{L}}} V(\pi_{\boldsymbol{\theta_{L}}})$. 
We use the GPOMDP algorithm for estimating the gradients, along with the optimal baseline for reducing variance \cite{weaver2001optimal}.


\section{Experiments}

We wish to evaluate the following questions empirically:

\begin{enumerate}
  \item Can the Policy Tree improve over the base policy?
  \item Is the improvement merely due to an increase in the number of parameters, or does the tree growth criterion help?
  \item How well does the Fringe Bias Approximation work?
\end{enumerate}

To answer these questions, we need environments where the linear Gibbs softmax policy is insufficient. We want these environments to be generic and represent situations which might be obtained in a variety of domains, for example, arcade games. Our test suite is a set of such simple games, which have a 16x16 pixel game screen with upto 8 colours. In all these games, the maximum episode length is 256 time steps, and every object moves with a speed of one pixel per time step. Objects in these games, including the player agent, enemy agents, friendly agents or bullets are a single pixel in size, and each distinct object type is of a distinct colour. Unless specified otherwise, the actions available to the agent are to move up, down, left, right or stay still. The objective of the learning agent is to maximize the expected undiscounted return.

\begin{figure}
	\includegraphics[scale=0.26]{Games2.png}
	\caption{The graphical representation of the games. Clockwise from top-left, they are Monsters, Tag, Mothership and Rescue}
    \label{fig:games}
\end{figure}

For generating the observations vector, the location of the player agent is assumed to be known. We divide the game screen into zones, the size of which grows exponentially with the distance from the agent. Formally, if $x$ and $y$ represent the distance of a pixel from the agent, all pixels with the same value of $\left \lfloor{\log_2(x)}\right \rfloor$, $\left \lfloor{\log_2(x)}\right \rfloor$, $\sgn(x)$ and $\sgn(y)$ belong to the same zone. For each zone, we have a feature corresponding to every colour used in the game. A feature is set active if an object of the corresponding colour is found in that zone. An example of feature generation is shown in Figure \ref{fig:feature_generation}. The left image shows a game screen for Mothership, with the black lines representing the zones centred around the agent. The right screen indicates the active features in each zone via coloured dots. $7$ features are active in this example. Such a choice of features allows fine distinctions to be made between objects near the agent, while ensuring that the feature size grows only logarithmically with the size of the game screen.


\begin{figure}
	\includegraphics[scale=0.25]{LogarithmicRelativeMapping4.png}
	\caption{The feature mapping process.}
    \label{fig:feature_generation}
\end{figure}

A brief description of the games is as follows:

\textbf{Monsters:} The player agent is green. There are three red monsters, as well as three blue power ups in the game. Eating a power up causes the agent to change colour to yellow, and allows it to kill the next monster it collides with, to earn one point. A power up lasts a maximum of 16 time steps. The monsters chase the agent when it is not powered, and stay still otherwise.

\begin{figure}
	\includegraphics[scale=0.6]{Monsters.pdf}
	\caption{Results on Monsters}
    \label{fig:results_monsters}
\end{figure}


\textbf{Tag:} There are two teams, red and blue. Non-player agents from either team move across the screen horizontally. The player agent starts on a random team. Hitting agents from the other team terminates the episode, and catching a member of its own team earns a point. After a point is earned, the agent switches teams. An agent has the same color as its team name.

\begin{figure}
	\includegraphics[scale=0.6]{Tag.pdf}
	\caption{Results on Tag}
    \label{fig:results_tag}
\end{figure}


\textbf{Mothership:} The blue mothership sits at the top of the screen, while red guard ships patrol horizontally below. The green player agent needs to shoot the mothership to collect $5$ points, and then shoot the guard ships for $1$ point each. Shooting the guards before destroying the mothership simply reflects bullets back at the agent, and alerts the enemy, causing the mothership to move randomly, and the guard ships to shoot bullets. All bullets are coloured yellow.

\begin{figure}
	\includegraphics[scale=0.6]{Mothership.pdf}
	\caption{Results on Mothership}
    \label{fig:results_mothership}
\end{figure}

\textbf{Rescue:} The green coloured player agent has to collect a yellow hostage from an area on the screen, bringing it to the blue drop off location to collect a point. The hostages and drop off location are randomly distributed after every collection, and the player starts from a random location at the top of the screen. Red enemy agents move across the screen horizontally, and collision with them results in instant episode termination.

\begin{figure}
	\includegraphics[scale=0.6]{Rescue.pdf}
	\caption{Results on Rescue}
    \label{fig:results_rescue}
\end{figure}

The results of Policy Tree on these games are shown in Figures \ref{fig:results_rescue} to \ref{fig:results_tag}. Policy Tree (Approx) shows the results of enabling the Fringe Bias approximation. We also test a version of the algorithm which randomly chooses a split to make during the Tree Growth phase, created to answer the second question posed earlier. The base policy is the linear Gibbs softmax policy, with $\epsilon$-greedy exploration. We also tested a version of linear Gibbs softmax with $\epsilon = 0$, in order to compare against standard linear approaches, but there was no significant gain in performance.

For each algorithm, we performed $10$ different runs of learning over $500000$ episodes. The Average Return in the graph is a moving average of the total undiscounted return. The window for the moving average is $50000$. The standard error in the results, across the $10$ runs, is represented by the vertical error bars in the graph.

For Policy Tree, we used a Parameter Optimization stage of $49000$ episodes, and a gradient averaging phase during Tree Growth of $1000$ episodes. Splits are therefore made after every $50000$ episodes. The value of $\epsilon$ used was $0.001$. The optimum learning rates for both algorithms was obtained via a parameter sweep for each game.

From the results it can be seen that Policy Tree (as well as its approximate version), can both outperform the base policy with statistical significance. The version of the algorithm with random splits performs roughly at par with the base policy.

\section{Discussion}

The results demonstrate that the Policy Tree algorithm can improve upon its underlying base policy, in these environments. These games contain intricacies, because of which a linear policy cannot fully represent an optimal policy. At the same time, they are a realistic representation of the complexity found in arcade games or other similar domains. In general, we would expect the Policy Tree algorithm to be used in such a setting, where the base policy is found to be inadequate.

It can be seen that an arbitrary increase in the number of parameters (via the random splitting criterion) actually hurts performance, because the amount of data available to optimize each parameter is reduced. This shows that the criterion used to grow the decision tree contributes significantly to the effectiveness of the algorithm.

The Policy Tree algorithm in general represents a family of algorithms, characterized by the choice of base policy. This is similar to the previously described NPPG framework, which is characterized by its choice of regression model. We do not compare the two algorithms, as we believe that it would be an apples-to-oranges comparison with highly domain specific results. 

The NPPG framework has the advantage of being able to use standard regression models (like regression tree algorithms) to estimate the functional gradient. Policy Tree, on the other hand, is conceptually and computationally simpler. The NPPG results show that it can outperform parametric approaches only late in the learning process, as it starts with an empty function. Policy Tree, on the other hand, can perform atleast as well as the base policy, early during the learning process.
 
There are a number of areas in which this work can be expanded upon. The current version of our algorithm makes one split during the tree growth stage. However, in general, we could make any number of splits during this phase (by having a threshold over the gradient based scoring criterion). This would allow the tree to grow exponentially in size, rather than linearly, as in the present version.

The splits in the decision tree are currently based on the value of a single observation variable. In general, we could define a split over a conjunction or a disjunction of observation variables. This would increase the size of the fringe used during tree growth, but would allow the algorithm to find splits which are significantly more meaningful.

\bibliographystyle{aaai}
\bibliography{policytree}

\end{document}

#include <iostream>
#include <vector>
#include <unordered_set>
#include <cstdlib>
#include <ctime>

#include "policy_tree_linear.hpp"
#include "XOR.h"


int main(int argc, char** argv)
{
    if(argc < 12)
    {
        std::cerr<<"Usage: "<<argv[0]<<"runs featureType proximity learningRate minActionProbability maxLeafNodes gradientAverageWindow optimizationSteps useBaseLine onlyUseFringeBias useDotProductScore discountFactor seed"<<'\n';
        return 1;
    }

    int runs = atoi(argv[1]);
    double learningRate = atof(argv[2]);
    double minActionProbability = atof(argv[3]);
    int maxLeafNodes = atoi(argv[4]);
    int gradientAverageWindow = atoi(argv[5]);
    int optimizationSteps = atoi(argv[6]);
    bool useBaseLine = atoi(argv[7]) ? true : false;
    bool onlyUseFringeBias = atoi(argv[8]) ? true : false;
    int scoreType = atoi(argv[9]);
    double discountFactor = atof(argv[10]);
    int seed = atoi(argv[11]);

    srand(seed);
    XOR game;
    PolicyTreeLinear policyTreeLinear(game.getNumFeatures(), game.getNumActions(), learningRate, minActionProbability, maxLeafNodes, gradientAverageWindow, optimizationSteps, useBaseLine, onlyUseFringeBias, scoreType, discountFactor);
    double sumReward = 0;

    for(int episode=0; episode < runs; episode++)
    {
        double totalReward = 0;
        int episodeLength = 0;
        game.reset();

        while(game.isActive())
        {
            std::unordered_set<int> observations = game.getFeatures();
            unsigned int action = policyTreeLinear.storeObservationsAndGetAction(observations);
            double reward = game.processAction(action);
            policyTreeLinear.processAction(action, reward);
            totalReward += reward;
            episodeLength++;
        }
        
        std::cout<<"Episode "<<episode<<" ended with score: "<<totalReward<<"\n";
        policyTreeLinear.processEpisode();
        sumReward += totalReward;
    }
}

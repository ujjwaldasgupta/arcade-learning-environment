#include <unordered_set>

using namespace std;

class ComplexXOR
{
public:
    static const int numActions = 2;
    static const int numFeatures = 10;
    static const int episodeLength = 256;
    
    bool active;
    unordered_set<int> features;
    int currTime;
    
    ComplexXOR() : active(false), currTime(0)
    {
    }
    
    void reset()
    {
        active = true;
        currTime = 0;
        resetFeatures();
    }
    
    void resetFeatures()
    {
        features = unordered_set<int>();
        features.insert(0);
                
        if(rand()%2)
        {
            features.insert(1);
        }
        
        if(rand()%2)
        {
            features.insert(2);
        }
        
        if(rand()%16 == 0)
        {
            features.insert(3);
        }
        
        if(rand()%64 == 0)
        {
            features.insert(4);
        }
        
        if(rand()%2 == 0)
        {
            features.insert(5);
        }
        
        if(rand()%2 == 0)
        {
            features.insert(6);
        }
        
        if(rand()%2)
        {
            features.insert(7);
        }
        
        if(rand()%2)
        {
            features.insert(8);
        }
        
        if(rand()%2)
        {
            features.insert(9);
        }
    }
    
    bool isActive()
    {
        return active;
    }
    
    double processAction(int action)
    {
        assert(action < numActions);
        double reward = 0;
        
        if(!active)
        {
            return 0;
        }
        
        int desiredAction = (features.find(1) != features.end()) ^ (features.find(2) != features.end());

        if((features.find(4) == features.end()) && (features.find(3) != features.end()))
        {
            desiredAction = !desiredAction;
        }
        
        if(action == desiredAction)
        {
            reward = 1;
        }
        else
        {
            reward = -1;
        }
        
        currTime++;
        
        if(currTime == episodeLength)
        {
            active = false;
        }
        
        resetFeatures();
        return reward;
    }
    
    unordered_set<int> getFeatures()
    {
        return features;
    }
    
    int getNumActions()
    {
        return numActions;
    }
    
    int getNumFeatures()
    {
        return numFeatures;
    }
};

unordered_set<int> expandFeatures(unordered_set<int>& features, int size)
{
    unordered_set<int> expandedFeatures = features;
    for(auto it1 = features.begin(); it1 != features.end(); it1++)
    {
        auto it2 = it1;
        it2++;
        
        for(; it2 != features.end(); it2++)
        {
            int minF = min(*it1, *it2);
            int maxF = max(*it1, *it2);
            expandedFeatures.insert(size + size*minF - minF*(minF+1)/2 + maxF - minF - 1);
        }
    }
    
    return expandedFeatures;
}
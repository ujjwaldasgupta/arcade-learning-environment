#include <iostream>
#include <vector>
#include <cstdlib>
#include <cassert>
#include <ctime>

#include "policy_booster.hpp"
#include "Pacman.h"

using namespace std;

int main(int argc, char** argv)
{
    if(argc < 11)
    {
        cerr<<"Usage: "<<argv[0]<<"runs learningRate minActionProbability optimizationSteps gradientAveragingSteps scoreType expand featureType proximity seed"<<'\n';
        return 1;
    }

    int runs = atoi(argv[1]);
    double learningRate = atof(argv[2]);
    double minActionProbability = atof(argv[3]);
    int optimizationSteps = atoi(argv[4]);
    int gradientAveragingSteps = atoi(argv[5]);
    int scoreType = atoi(argv[6]);
    bool expand = atoi(argv[7]) > 0 ? true : false;
    int featureType = atoi(argv[8]);
    int proximity = atoi(argv[9]);
    int seed = atoi(argv[10]);

    generator.seed(seed);
    srand(seed);
    Pacman game(featureType, proximity);
    game.reset();
    double sumTotalReward = 0;
    int numFeatures = game.getNumFeatures();

    if(expand)
    {
        numFeatures = numFeatures + numFeatures*(numFeatures-1) /2;
    }

    PolicyBooster policy(numFeatures, game.getNumActions(), learningRate, minActionProbability, optimizationSteps, gradientAveragingSteps, scoreType);
    
    for(int i=0; i < runs; i++)
    {
        game.reset();
        double totalReward = 0;

        while(game.isActive())
        {
            unordered_set<int> observations = game.getFeatures();
            
            if(expand)
            {
                observations = expandFeatures(observations, game.getNumFeatures());
            }
            
            int action = policy.storeObservationsAndGetAction(observations);
            double reward = game.processAction(action);
            policy.processActionAndReward(action, reward);
            totalReward += reward;
        }
        cout<<"Episode "<<i<<" ended at time "<<time(NULL)<<" with score: "<<totalReward<<"\n";
        policy.processEpisode();
        sumTotalReward += totalReward;
    }

    for(int i=0; i < policy.numActions; i++)
    {
        for(int j=0; j < policy.numFeatures; j++)
        {
            cout<<"w["<<i<<"]["<<j<<"] = "<<policy.weights[i][j]<<"\n";
        }
    }

    return 0;
}

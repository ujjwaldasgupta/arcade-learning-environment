#include <vector>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <unordered_set>

using namespace std;

class Invaders
{
    static const int height = 16;
    static const int width = 16;
    int currTime;
    int featureType;
    int proximity;
    bool active;
    static const int numColours = 5;
    static const int numActions = 4;
    static const int monsterGridSize = 4;
    static const int startingMonsterRow = 1;
    bool dir;
    static const int monsterFireTime = 2;
    static const int bonusReward = 5;
    static const int maxBullets = 256;
    static const int maxEpisodeLength = 256;
    
    int agentLocation;
    vector<pair<int, int> > monsterLocations;
    pair<int, int> agentBulletLocation;
    vector<pair<int, int> > enemyBulletLocations;
    int powerPlantLocation;
    int bulletsFired;
    bool agentRest;
    bool alerted;
    vector<vector<int> > screen;
    
    /* Possible screen values:
     * 0: Blank
     * 1: Agent
     * 2: Monster
     * 3: Bullet
     * 4: Power Plant
     */
    
    enum colour
    {
        COLOUR_BLANK = 0,
        COLOUR_AGENT,
        COLOUR_MONSTER,
        COLOUR_BULLET,
        COLOUR_POWER_PLANT
    };
    
    enum action
    {
        ACTION_NOOP = 0,
        ACTION_LEFT,
        ACTION_RIGHT,
        ACTION_FIRE,
    };
    
    public:
    Invaders(int featureType, int proximity) : featureType(featureType), proximity(proximity), active(false), dir(true), currTime(0), agentLocation(0), powerPlantLocation(0), agentRest(false), alerted(false), agentBulletLocation(make_pair(-1,-1)), screen(height, vector<int>(width, 0))
    {
    }
    
    bool isActive()
    {
        return active;
    }
    
    vector<vector<int> > getScreen()
    {
        return screen;
    }
    
    void reset()
    {
        monsterLocations = vector<pair<int, int> >(0);
        agentBulletLocation = make_pair(-1,-1);
        enemyBulletLocations = vector<pair<int, int> >(0);
        int startingMonsterColumn = rand()%2;
        
        for(int i=0; i < monsterGridSize; i++)
        {
            for(int j=startingMonsterColumn; j < width; j += 2)
            {
                pair<int, int> monsterLocation = make_pair(startingMonsterRow + i*2, j);
                assert(monsterLocation.first < height - 1);
                assert(monsterLocation.second < width);
                assert(monsterLocation.first >= 0);
                assert(monsterLocation.second >= 0);
                monsterLocations.push_back(monsterLocation);
            }
        }
        agentLocation = rand() % width;
        powerPlantLocation = rand() % width;
        bulletsFired = 0;
        agentRest = false;
        alerted = false;
        currTime = 0;
        generateScreen();
        active = true;
        dir = rand()%2;
    }
    
    void fillFromList(vector<pair<int, int> > list, int type)
    {
        for(int i=0; i < list.size(); i++)
        {
            assert(list[i].first < height);
            assert(list[i].second < width);
            assert(list[i].first >= 0);
            assert(list[i].second >= 0);
            screen[list[i].first][list[i].second] = type;
        }
    }
    
    void generateScreen()
    {
        screen = vector<vector<int> >(height, vector<int>(width, 0));
                
        fillFromList(enemyBulletLocations, COLOUR_BULLET);
        fillFromList(monsterLocations, COLOUR_MONSTER);
        
        if(powerPlantLocation != -1)
        {
            assert(powerPlantLocation >= 0);
            assert(powerPlantLocation < width);
            screen[0][powerPlantLocation] = COLOUR_POWER_PLANT;
        }
        
        if(agentBulletLocation.first != -1)
        {
            assert(agentBulletLocation.second >= 0);
            assert(agentBulletLocation.second < width);
            screen[agentBulletLocation.first][agentBulletLocation.second] = COLOUR_BULLET;
        }
        
        assert(agentLocation >= 0);
        assert(agentLocation < width);
        screen[height-1][agentLocation] = COLOUR_AGENT;
    }

    void monsterFire()
    {
        if(!alerted || monsterLocations.size() == 0)
        {
            return;
        }
        
        int i = rand() % monsterLocations.size();
        enemyBulletLocations.push_back(make_pair(monsterLocations[i].first+1, monsterLocations[i].second));
    }
    
    void agentFire()
    {
        if(agentBulletLocation.first == -1 && bulletsFired < maxBullets && !agentRest)
        {
            agentBulletLocation = make_pair(height-1, agentLocation);
            bulletsFired++;
        }
    }
        
    void processWorld()
    {  
        for(int i=0; i < monsterLocations.size(); i++)
        {
            if(dir)
            {
                monsterLocations[i].second++;
                
                if(monsterLocations[i].second >= width)
                {
                    monsterLocations[i].second = 0;
                }
            }
            else
            {
                monsterLocations[i].second--;
                
                if(monsterLocations[i].second < 0)
                {
                    monsterLocations[i].second = width-1;
                }
            }
            
            assert(monsterLocations[i].second < width);
            assert(monsterLocations[i].second >= 0);
        }
        
        vector<pair<int, int> > newEnemyBulletLocations;
        
        for(int i=0; i < enemyBulletLocations.size(); i++)
        {
            if(enemyBulletLocations[i].first < height-1)
            {
                newEnemyBulletLocations.push_back(make_pair(enemyBulletLocations[i].first+1, enemyBulletLocations[i].second));
            }
        }
        
        enemyBulletLocations = newEnemyBulletLocations;
        
        if(currTime % monsterFireTime == 0)
        {
            monsterFire();
        }
        
        if(alerted && powerPlantLocation != -1)
        {
            powerPlantLocation += (-1+2*(rand()%2));
            
            if(powerPlantLocation < 0)
            {
                powerPlantLocation = width-1;
            }
            else if(powerPlantLocation >= width)
            {
                powerPlantLocation = 0;
            }
        }
    }
    
    double processAction(int action)
    {
        /* Possible actions:
         * 0: Do nothing
         * 1: Fire
         * 2: Left
         * 3: Right
         * 4: Left Fire
         * 5: Right Fire
         */
        
        if(!active)
            return 0;
        
        processWorld();
        
        int r = rand()%100;
        
        switch(action)
        {
            case ACTION_NOOP:
                break;
                
            case ACTION_LEFT:
                if(agentLocation > 0)
                {
                    agentLocation--;
                }
                
                break;
                
            case ACTION_RIGHT:
                if(agentLocation < width-1)
                {
                    agentLocation++;
                }
                
                break;
            case ACTION_FIRE:
                agentFire();
                
                break;                
                
            default:
                assert(0);
        }
        
        int reward = 0;
        
        if(agentRest)
        {
            agentRest = false;
        }
        
        if(agentBulletLocation.first >= 0)
        {
            agentBulletLocation.first--;

            if(agentBulletLocation.first == -1)
            {
                if(rand()%2)
                {
                    agentRest = true;
                }
                
                alerted = true;
            }
        }
        
        /* Check if monsters are hit */
        for(int i=0; i < monsterLocations.size(); i++)
        {
            if(agentBulletLocation == monsterLocations[i])
            {
                if(powerPlantLocation == -1)
                {
                    monsterLocations[i].first = -1;
                    monsterLocations[i].second = -1;
                    reward++;
                }
                else
                {
                    enemyBulletLocations.push_back(make_pair(monsterLocations[i].first+1, monsterLocations[i].second));
                    alerted = true;
                }
                
                agentBulletLocation.first = -1;
                
                if(rand()%2)
                {
                    agentRest = true;
                }
            }
        }
        
        vector<pair<int, int> > newMonsterLocations;
        
        
        for(int i=0; i < monsterLocations.size(); i++)
        {
            if( !(monsterLocations[i].first == -1 && monsterLocations[i].second == -1) )
            {
                newMonsterLocations.push_back(monsterLocations[i]);
            }
        }
        
        monsterLocations = newMonsterLocations;
        
        /* Check if power plant is hit */
        if(agentBulletLocation.first == 0 && agentBulletLocation.second == powerPlantLocation)
        {
            agentBulletLocation.first = -1;
            powerPlantLocation = -1;
            reward += bonusReward;
            alerted = true;
        }
        
        /* Check if agent is hit */
        bool hit = false;
        
        for(int i=0; i < enemyBulletLocations.size(); i++)
        {
            if(enemyBulletLocations[i] == make_pair(height-1, agentLocation))
            {
                hit = true;
                break;
            }
        }
        
        if(hit)
        {
            active = false;
        }
        
        currTime++;
        
        if(currTime >= maxEpisodeLength)
        {
            active = false;
        }
        
        generateScreen();
        return (double)reward;
    }
    
    int getNumActions()
    {
        return numActions;
    }
    
    unordered_set<int> getFeatures()
    {
        if(featureType == 4)
        {
            return getLogarithmicRelativeFeatures();
        }
        else if(featureType == 3)
        {
            return getPairwiseLogarithmicRelativeFeatures();
        }
        else if(featureType == 2)
        {
            return getPairwiseRelativeFeatures();
        }
        else if(featureType == 1)
        {
            return getRelativeFeatures();
        }
        else if(featureType == 0)
        {
            return getAbsoluteFeatures();
        }
        else
        {
            assert(0);
        }
    }
    
    int getNumFeatures()
    {
        if(featureType == 4)
        {
            return 1 + (numColours-1) * 4 * (floor(log2(height-1))+2) * (floor(log2(width-1))+2);
        }
        else if(featureType == 3)
        {
            return 1 + height*width*(numColours-1) + 4*(floor(log2(height-1))+2)*(floor(log2(width-1))+2)*(numColours-1)*(numColours)/2;
        }
        else if(featureType == 2)
        {
            return 1 + (2*proximity-1)*(2*proximity-1)*(numColours-1)*(numColours)/2;
        }
        else if(featureType == 1)
        {
            return 1 + (2*proximity-1)*(2*proximity-1)*(numColours-1);
        }
        else if(featureType == 0)
        {
            return 1 + height*width*(numColours-1);
        }
        else
        {
            assert(0);
        }
    }
    
    unordered_set<int> getAbsoluteFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i=0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                if(screen[i][j] > 0)
                {
                    features.insert(1 + (screen[i][j]-1)*height*width + i*width + j);
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = max(0, height-proximity); i < min(int(height), height-1+proximity); i++)
        {            
            for(int j = max(0, agentLocation-proximity+1); j < min(int(width), agentLocation+proximity); j++)
            {
                if(screen[i][j] > 0)
                {
                    features.insert(1 + (screen[i][j]-1)*(2*proximity-1)*(2*proximity-1) + (i-height+proximity)*(2*proximity-1) + (j-agentLocation+proximity-1));
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getPairwiseRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = 0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                for(int k = max(0, i-proximity+1); k < min(int(height), i+proximity); k++)
                {              
                    for(int l = max(0, j-proximity+1); l < min(int(width), j+proximity); l++)
                    {
                        if(screen[i][j] == 0 || screen[i][j] > screen[k][l])
                        {
                            continue;
                        }
                        
                        int featureIndex = 1 + ((screen[i][j]-1)*(numColours-1)-((screen[i][j]-1)*(screen[i][j]-2))/2+screen[k][l]-screen[i][j])*(2*proximity-1)*(2*proximity-1) + (k-i+proximity-1)*(2*proximity-1) + (l-j+proximity-1);
                        
                        if(features.find(featureIndex) == features.end())
                        {
                            features.insert(featureIndex);
                        }
                    }
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getPairwiseLogarithmicRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = 0; i < height; i++)
        {
            for(int j=0; j < width; j++)
            {
                for(int k = 0; k < height; k++)
                {              
                    for(int l = 0; l < width; l++)
                    {
                        if(screen[i][j] == 0 || screen[i][j] > screen[k][l])
                        {
                            continue;
                        }
                        
                        int quadrant = (k>i)*2 + (l>j);
                        int logX;
                        int logY;
                        
                        if(k==i)
                        {
                            logX = 0;
                        }
                        else
                        {
                            logX = floor(log2(abs(k-i)))+1;
                        }
                        
                        if(l==j)
                        {
                            logY = 0;
                        }
                        else
                        {
                            logY = floor(log2(abs(l-j)))+1;
                        }
                        
                        int featureIndex = 1 + height*width*(numColours-1) + ((screen[i][j]-1)*(numColours-1)-((screen[i][j]-1)*(screen[i][j]-2))/2+screen[k][l]-screen[i][j])*4*(floor(log2(height-1))+2)*(floor(log2(width-1))+2) + quadrant*(floor(log2(height-1))+2)*(floor(log2(width-1))+2) + logX*(floor(log2(width-1))+2) + logY;
                        
                        if(features.find(featureIndex) == features.end())
                        {
                            features.insert(featureIndex);
                        }
                    }
                }
            }
        }
        
        return features;
    }
    
    unordered_set<int> getLogarithmicRelativeFeatures()
    {
        unordered_set<int> features;
        features.insert(0);
        
        for(int i = max(0, height-proximity); i < min(int(height), height-1+proximity); i++)
        {            
            for(int j = max(0, agentLocation-proximity+1); j < min(int(width), agentLocation+proximity); j++)
            {
                if(screen[i][j] > 0)
                {
                    int quadrant = (i>height-1)*2 + (j>agentLocation);
                    int relI = abs(i-height+1);
                    int relJ = abs(j-agentLocation);
                    int logI;
                    int logJ;
                    
                    if(relI == 0)
                    {
                        logI = 0;
                    }
                    else
                    {
                        logI = 1+floor(log2(relI));
                    }
                    
                    if(relJ == 0)
                    {
                        logJ = 0;
                    }
                    else
                    {
                        logJ = 1+floor(log2(relJ));
                    }
                    
                    int featureIndex = ( (screen[i][j]-1) * 4 * (floor(log2(height-1))+2) * (floor(log2(width-1))+2) +
                                         quadrant * (floor(log2(height-1))+2) * (floor(log2(width-1))+2) +
                                         logI * (floor(log2(width-1))+2) +
                                         logJ +
                                         1
                                       );
                    
                    assert(featureIndex >= 0);
                    assert(featureIndex < (numColours-1) * 4 * (floor(log2(height-1))+2) * (floor(log2(width-1))+2) + 1);
                    
                    if(features.find(featureIndex) == features.end())
                    {
                        features.insert(featureIndex);
                    }
                }
            }
        }
        
        return features;
    }
};

unordered_set<int> expandFeatures(unordered_set<int>& features, int size)
{
    unordered_set<int> expandedFeatures = features;
    for(auto it1 = features.begin(); it1 != features.end(); it1++)
    {
        auto it2 = it1;
        it2++;
        
        for(; it2 != features.end(); it2++)
        {
            int minF = min(*it1, *it2);
            int maxF = max(*it1, *it2);
            expandedFeatures.insert(size + size*minF - minF*(minF+1)/2 + maxF - minF - 1);
        }
    }
    
    return expandedFeatures;
}

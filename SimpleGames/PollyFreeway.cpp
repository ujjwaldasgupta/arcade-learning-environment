#include <iostream>
#include <vector>
#include <unordered_set>
#include <cstdlib>
#include <ctime>

#include "policy_tree.hpp"
#include "Freeway.h"


int main ( int argc, char** argv )
{
    if ( argc < 18 )
    {
        std::cerr<<"Usage: "<<argv[0]<<"runs featureType proximity sizeHistory minActionProbability learningRate maxLeafNodes gradientAverageWindow optimizationSteps cosineThreshold useGPOMDP useTotalReward useBaseLine useSoftMax useMultiSplit useCosineSplit seed"<<'\n';
        return 1;
    }

    int runs = atoi ( argv[1] );
    int featureType = atoi ( argv[2] );
    int proximity = atoi ( argv[3] );
    int sizeHistory = ( bool ) atoi ( argv[4] );
    double minActionProbability = atof ( argv[5] );
    double learningRate = atof ( argv[6] );
    int maxLeafNodes = atoi ( argv[7] );
    int gradientAverageWindow = atoi ( argv[8] );
    int optimizationSteps = atoi ( argv[9] );
    double cosineThreshold = atof ( argv[10] );
    bool useGPOMDP = atoi ( argv[11] ) ? true : false;
    bool useTotalReward = atoi ( argv[12] ) ? true : false;
    bool useBaseLine = atoi ( argv[13] ) ? true : false;
    bool useSoftMax = atoi ( argv[14] ) ? true : false;
    bool useMultiSplit = atoi ( argv[15] ) ? true : false;
    bool useCosineSplit = atoi ( argv[16] ) ? true : false;
    int seed = atoi ( argv[17] );

    srand ( seed );
    Freeway game ( featureType, proximity );
    PolicyTree policyTree ( sizeHistory, game.getNumFeatures(), game.getNumActions(), minActionProbability, learningRate, maxLeafNodes, gradientAverageWindow, optimizationSteps, cosineThreshold, useGPOMDP, useTotalReward, useBaseLine, useSoftMax, useMultiSplit, useCosineSplit);

    double sumReward = 0;

    for ( int episode=0; episode < runs; episode++ )
    {
        double totalReward = 0;
        int episodeLength = 0;
        game.reset();

        while ( game.isActive() )
        {
            std::unordered_set<int> observations = game.getFeatures();
            unsigned int action = policyTree.storeObservationsAndGetAction ( observations );
            double reward = game.processAction ( action );
            policyTree.processAction ( action, reward );
            totalReward += reward;
            episodeLength++;
        }

        policyTree.processEpisode();
        sumReward += totalReward;
        std::cout<<"Episode "<<episode<<" ended with score: "<<totalReward<<"\n";
    }
}

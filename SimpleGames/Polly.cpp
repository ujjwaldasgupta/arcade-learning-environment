#include <iostream>
#include <vector>
#include <unordered_set>
#include <cstdlib>
#include <ctime>

#include "policy_tree.hpp"
#include "Asterix.h"


int main ( int argc, char** argv )
{
    if ( argc < 23 )
    {
        std::cerr<<"Usage: "<<argv[0]<<" runs height width restTime relative proximity sizeHistory minActionProbability learningRate maxLeafNodes gradientAverageWindow optimizationSteps cosineThreshold useGPOMDP useTotalReward useBaseLine useSoftMax useMultiSplit useCosineSplit powerChance pointRatio seed"<<'\n';
        return 1;
    }

    int runs = atoi ( argv[1] );
    int height = atoi ( argv[2] );
    int width = atoi ( argv[3] );
    int restTime = atoi ( argv[4] );
    bool relative = atoi ( argv[5] ) ? true : false;
    int proximity = atoi ( argv[6] );
    int sizeHistory = ( bool ) atoi ( argv[7] );
    double minActionProbability = atof ( argv[8] );
    double learningRate = atof ( argv[9] );
    int maxLeafNodes = atoi ( argv[10] );
    int gradientAverageWindow = atoi ( argv[11] );
    int optimizationSteps = atoi ( argv[12] );
    double cosineThreshold = atof ( argv[13] );
    bool useGPOMDP = atoi ( argv[14] ) ? true : false;
    bool useTotalReward = atoi ( argv[15] ) ? true : false;
    bool useBaseLine = atoi ( argv[16] ) ? true : false;
    bool useSoftMax = atoi ( argv[17] ) ? true : false;
    bool useMultiSplit = atoi ( argv[18] ) ? true : false;
    bool useCosineSplit = atoi ( argv[19] ) ? true : false;
    double powerChance = atof ( argv[20] );
    int pointRatio = atoi ( argv[21] );
    int seed = atoi ( argv[22] );

    srand ( seed );
    Asterix game ( height, width, restTime, relative, proximity, powerChance, pointRatio );
    PolicyTree policyTree ( sizeHistory, game.getNumFeatures(), game.getNumActions(), minActionProbability, learningRate, maxLeafNodes, gradientAverageWindow, optimizationSteps, cosineThreshold, useGPOMDP, useTotalReward, useBaseLine, useSoftMax, useMultiSplit, useCosineSplit );

    double sumReward = 0;

    for ( int episode=0; episode < runs; episode++ )
    {
        double totalReward = 0;
        int episodeLength = 0;
        game.reset();

        while ( game.isActive() )
        {
            std::unordered_set<int> observations = game.getFeatures();
            unsigned int action = policyTree.storeObservationsAndGetAction ( observations );
            double reward = game.processAction ( action );
            policyTree.processAction ( action, reward );
            totalReward += reward;
            episodeLength++;
        }

        policyTree.processEpisode();
        sumReward += totalReward;
        std::cout<<"Episode "<<episode<<" ended with score: "<<totalReward<<"\n";
    }
}

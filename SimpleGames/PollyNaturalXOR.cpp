#include <iostream>
#include <vector>
#include <cstdlib>
#include <cassert>

#include "policy_tree_natural.hpp"
#include "XOR.h"

using namespace std;

int main(int argc, char** argv)
{
    if(argc < 10)
    {
        cerr<<"Usage: "<<argv[0]<<"runs learningRate minActionProbability gradientAverageWindow gradientAverageWindowForSplit optimizationSteps regularizationConstant parameterNormConstraint seed"<<'\n';
        return 1;
    }
    
    int runs = atoi(argv[1]);
    double learningRate = atof(argv[2]);
    double minActionProbability = atof(argv[3]);
    int gradientAverageWindow = atoi(argv[4]);
    int gradientAverageWindowForSplit = atoi(argv[5]);
    int optimizationSteps = atoi(argv[6]);
    double regularizationConstant = atof(argv[7]);
    double parameterNormConstraint = atof(argv[8]);
    int seed = atoi(argv[9]);

    srand(seed);
    XOR game;
    game.reset();
    double sumTotalReward = 0;
    int numFeatures = game.getNumFeatures();
    
    PolicyTreeNatural policy(numFeatures, game.getNumActions(), learningRate, minActionProbability, 1024, gradientAverageWindow, gradientAverageWindowForSplit, optimizationSteps, regularizationConstant, parameterNormConstraint);

    for(int i=0; i < runs; i++)
    {
        game.reset();
        double totalReward = 0;
        int episodeLength = 0;
        
        while(game.isActive())
        {
            std::unordered_set<int> observations = game.getFeatures();
            int action = policy.storeObservationsAndGetAction(observations);
            double reward = game.processAction(action);
            policy.processAction(action, reward);
            totalReward += reward;
            episodeLength++;
        }
        
        cout<<"Episode "<<i<<" ended with score: "<<totalReward<<"\n";
        sumTotalReward += totalReward;
        policy.processEpisode();
    }
    
    return 0;
}
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cassert>
#include <ctime>
#include "policy_gradient_gpomdp.hpp"
#include "Seaquest.h"

using namespace std;

int main(int argc, char** argv)
{
    if(argc < 10)
    {
        cerr<<"Usage: "<<argv[0]<<"runs learningRate temperature expand decayFactor intrinsicRewardScale featureType proximity seed"<<'\n';
        return 1;
    }
    
    int runs = atoi(argv[1]);
    double learningRate = atof(argv[2]);
    double temperature = atof(argv[3]);
    bool expand = atoi(argv[4]) > 0 ? true : false;
    double decayFactor = atof(argv[5]);
    double intrinsicRewardScale = atof(argv[6]);
    int featureType = atoi(argv[7]);
    int proximity = atoi(argv[8]);
    int seed = atoi(argv[9]);

    srand(seed);
    Seaquest game(featureType, proximity);
    game.reset();
    double sumTotalReward = 0;
    int numFeatures = game.getNumFeatures();
    
    if(expand)
    {
        numFeatures = numFeatures + numFeatures*(numFeatures-1)/2;
    }
    
    PolicyGradientGPOMDP policy(1, numFeatures, game.getNumActions(), learningRate, temperature, decayFactor, intrinsicRewardScale);

    for(int i=0; i < runs; i++)
    {
        game.reset();
        double totalReward = 0;
        
        while(game.isActive())
        {
            unordered_set<int> observations = game.getFeatures();
            if(expand)
            {
                observations = expandFeatures(observations, game.getNumFeatures());
            }
            int action = policy.storeObservationsAndGetAction(observations);
            double reward = game.processAction(action);
            policy.updateStoredGradient(action, reward);
            totalReward += reward;
        }
        
        policy.processEpisode();
        sumTotalReward += totalReward;
        cout<<"Episode "<<i<<" ended at time "<<time(NULL)<<" with score: "<<totalReward<<"\n";
    }
    
    return 0;
}
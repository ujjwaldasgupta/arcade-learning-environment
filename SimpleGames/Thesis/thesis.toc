\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {chapter}{\numberline {2}The Reinforcement Learning Problem}{4}
\contentsline {section}{\numberline {2.1}The Environment}{4}
\contentsline {section}{\numberline {2.2}The Agent}{5}
\contentsline {section}{\numberline {2.3}The Goal}{5}
\contentsline {chapter}{\numberline {3}Policy Gradient}{7}
\contentsline {section}{\numberline {3.1}The REINFORCE Algorithm}{7}
\contentsline {section}{\numberline {3.2}The Policy Gradient/GPOMDP Algorithm}{9}
\contentsline {section}{\numberline {3.3}Using Baselines to Reduce Variance}{11}
\contentsline {section}{\numberline {3.4}Policy Parametrizations for Factored State}{11}
\contentsline {subsection}{\numberline {3.4.1}Linear Gibbs Softmax Policy}{11}
\contentsline {subsection}{\numberline {3.4.2}Normally Distributed Policy}{12}
\contentsline {subsection}{\numberline {3.4.3}Multi-Armed Bandit Policy}{12}
\contentsline {section}{\numberline {3.5}Non-Parametric Policy Gradients}{12}
\contentsline {chapter}{\numberline {4}The Policy Tree Algorithm}{14}
\contentsline {section}{\numberline {4.1}Notation}{14}
\contentsline {section}{\numberline {4.2}Overview of the Policy Tree Algorithm}{15}
\contentsline {section}{\numberline {4.3}Parameter Optimization}{15}
\contentsline {section}{\numberline {4.4}Tree Growth}{15}
\contentsline {subsection}{\numberline {4.4.1}Fringe Bias Approximation}{18}
\contentsline {chapter}{\numberline {5}Experiments on Policy Tree}{19}
\contentsline {section}{\numberline {5.1}Domains}{19}
\contentsline {subsection}{\numberline {5.1.1}Monsters}{20}
\contentsline {subsection}{\numberline {5.1.2}Switcheroo}{20}
\contentsline {subsection}{\numberline {5.1.3}Mothership}{20}
\contentsline {subsection}{\numberline {5.1.4}Rescue}{22}
\contentsline {section}{\numberline {5.2}Feature Generation}{22}
\contentsline {section}{\numberline {5.3}Base Policy}{23}
\contentsline {section}{\numberline {5.4}Experimental Setup}{24}
\contentsline {section}{\numberline {5.5}Results}{24}
\contentsline {chapter}{\numberline {6}The Policy Conjunction Algorithm}{28}
\contentsline {section}{\numberline {6.1}A Flat View of the Policy Tree Algorithm}{28}
\contentsline {section}{\numberline {6.2}Overview of the Policy Conjunction Algorithm}{29}
\contentsline {section}{\numberline {6.3}Adding candidate features}{30}
\contentsline {section}{\numberline {6.4}Modifying the Step Size during Gradient Optimization}{31}
\contentsline {section}{\numberline {6.5}Experiments}{31}
\contentsline {chapter}{\numberline {7}Future Work}{35}
\contentsline {section}{\numberline {7.1}Detecting Convergence of the Phases}{35}
\contentsline {section}{\numberline {7.2}Removing Redundant Features/Splits}{35}
\contentsline {section}{\numberline {7.3}Generalizing the Decision Nodes in Policy Tree}{36}
\contentsline {section}{\numberline {7.4}Using Off-Policy Experience}{36}
\contentsline {chapter}{\numberline {8}Conclusion}{37}
\contentsline {chapter}{Bibliography}{38}

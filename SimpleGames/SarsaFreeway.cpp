#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cassert>
#include <limits>
#include <unordered_set>

#include "Freeway.h"

double getQ(std::unordered_set<int> &f, std::vector<double> &w)
{
        double Q = 0;
        
        for(std::unordered_set<int>::iterator it = f.begin(); it != f.end(); it++)
        {
                assert(*it < w.size());
                Q += w[*it];
        }
        
        return Q;
}

int bestAction(std::unordered_set<int> &f, std::vector<std::vector<double> > &w) 
{
        int bestAction = rand()%w.size();
        double bestQ = getQ(f, w[bestAction]);
        
        for(int i=0; i<w.size(); i++)
        {
                double Q = getQ(f, w[i]);
                
                if(Q > bestQ)
                {
                        bestQ = Q;
                        bestAction = i;
                }
        }
        
        return bestAction;
}

int main(int argc, char** argv) 
{
        if(argc < 9)
        {
                std::cerr << "Usage: " << argv[0] << " alpha gamma epsilon initialValue runs featureType proximity seed" << std::endl;
                return 1;
        }

        double alpha = atof(argv[1]);
        double gamma = atof(argv[2]);
        double epsilon = atof(argv[3]);
        double initialValue = atof(argv[4]);
        int runs = atoi(argv[5]);
        int featureType = atoi(argv[6]);
        int proximity = atoi(argv[7]);
        int seed = atoi(argv[8]);

        srand(seed);
        Freeway game(featureType, proximity);
        int numFeatures = game.getNumFeatures();
        int numActions = game.getNumActions();
        
        std::vector<std::vector<double> > w(numActions, std::vector<double>(numFeatures, initialValue/numFeatures));
        alpha = alpha/numFeatures;
        
        double sumReward = 0;
        for(int episode=0; episode<runs; episode++) 
        {
                game.reset();
                double totalReward = 0;
                std::unordered_set<int> currF = game.getFeatures();
                int currAction;
                
                if((double)rand()/RAND_MAX < epsilon)
                {
                        currAction = rand()%numActions;
                }
                else
                {
                        currAction = bestAction(currF, w);
                }

                while (game.isActive()) 
                {
                        double currReward = game.processAction(currAction);
                        totalReward += currReward;
                        int nextAction;

                        std::unordered_set<int> nextF =  game.getFeatures();
                        
                        if((double)rand()/RAND_MAX < epsilon)
                        {
                                nextAction = rand()%numActions;
                        }
                        else
                        {
                                nextAction = bestAction(nextF, w);
                        }
                        
                        double delta;
                        
                        if(game.isActive())
                        {
                            delta = currReward + gamma*getQ(nextF, w[nextAction])-getQ(currF, w[currAction]);
                        }
                        else
                        {
                            delta = currReward - getQ(currF, w[currAction]);
                        }
                        
                        for(auto it = currF.begin(); it != currF.end(); it++)
                        {
                                assert(*it < w[currAction].size());
                                w[currAction][*it] += alpha*delta;
                        }
                                
                        currAction = nextAction;
                        currF = nextF;
                }
                
                sumReward += totalReward;
                std::cout<<"Episode "<<episode<<" ended with score: "<<totalReward<<"\n";
        }
};


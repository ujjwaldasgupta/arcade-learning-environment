#include <vector>
#include <deque>
#include <cassert>
#include <cmath>
#include <utility>
#include <cstdlib>
#include <set>
#include <unordered_set>
#include <armadillo>
#include "sample.hpp"

using namespace std;
using namespace arma;

class PolicyGradientNatural
{
public:
    int numObservations;
    int numActions;
    vector<vector<double> > weights;
    vector<vector<vector<vector<double> > > > Z; /* Index order: Trajectory, Time Step, Action, Observation */
    vector<vector<double> > rewards; /* Index order: Trajectory, Time Step */
    vector<vector<unordered_set<int> > > cachedObservations;
    vector<double> cachedActionProbabilities;
    vector<int> episodeLengths;
    bool isActionProbabilitiesCacheClean;
    double learningRate;
    double temperature;
    bool timeVariant;
    int numTrajectories;
    int maxEpisodeLength;
    int maxSeenEpisodeLength;
    int currTime;
    int numEpisodes;
    int currTrajectory;
    double regularizationConstant;

    PolicyGradientNatural(int numObservations, int numActions, double learningRate, int numTrajectories, int maxEpisodeLength, bool timeVariant, double regularizationConstant) : numObservations(numObservations), numActions(numActions), weights(numActions, vector<double>(numObservations, 0)), Z(vector<vector<vector<vector<double> > > >(numTrajectories, vector<vector<vector<double> > >(maxEpisodeLength, vector<vector<double> >(numActions, vector<double>(numObservations, 0))))), rewards(vector<vector<double> >(numTrajectories, vector<double>(maxEpisodeLength, 0))), cachedObservations(vector<vector<unordered_set<int> > >(numTrajectories, vector<unordered_set<int> >(maxEpisodeLength))), episodeLengths(vector<int>(numTrajectories, 0)), isActionProbabilitiesCacheClean(false), learningRate(learningRate), temperature(1.0), numTrajectories(numTrajectories), maxEpisodeLength(maxEpisodeLength), timeVariant(timeVariant), regularizationConstant(regularizationConstant), maxSeenEpisodeLength(0), currTime(0), numEpisodes(0), currTrajectory(0)
    {
        for(int i=0; i < numActions; i++)
        {
            for(int j=0; j < numObservations; j++)
            {
                weights[i][j] = 0.001*(2*((double)rand()/RAND_MAX) - 1);
            }
        }
    }

    void updateTrace(int currAction, double reward, unordered_set<int>& observations)
    {
        assert(currTime < maxEpisodeLength);
        vector<double> actionProbabilities = getActionProbabilities(observations);
        
        for(int i=0; i < numActions; i++)
        {
            for(auto it = observations.begin(); it != observations.end(); it++)
            {
                assert(*it < numObservations);
                Z[currTrajectory][currTime][i][*it] = ((i==currAction)-actionProbabilities[i])/temperature;
            }
        }
        
        cachedObservations[currTrajectory][currTime] = observations;
        rewards[currTrajectory][currTime] = reward;
        currTime++;
    }
    
    void resetTrace()
    {
        Z = vector<vector<vector<vector<double> > > >(numTrajectories, vector<vector<vector<double> > >(maxEpisodeLength, vector<vector<double> >(numActions, vector<double>(numObservations, 0))));
        rewards = vector<vector<double> >(numTrajectories, vector<double>(maxEpisodeLength, 0));
        cachedObservations = vector<vector<unordered_set<int> > >(numTrajectories, vector<unordered_set<int> >(maxEpisodeLength));
        episodeLengths = vector<int>(numTrajectories, 0);
    }
    
    void performTimeVariantGradientStep()
    {
        mat averagePhi = zeros<mat>(numActions*numObservations, maxSeenEpisodeLength);
        vec averageG = zeros<vec>(numActions*numObservations);
        mat averageF = zeros<mat>(numActions*numObservations, numActions*numObservations);
        vec averageR = zeros<vec>(maxSeenEpisodeLength);
        
        for(int i=0; i < numTrajectories; i++)
        {
            vec psi = zeros<vec>(numActions*numObservations);
            vec g = zeros<vec>(numActions*numObservations);
            mat F = zeros<mat>(numActions*numObservations, numActions*numObservations);
            mat phi = zeros<mat>(numActions*numObservations, 0);
            
            for(int j=0; j < episodeLengths[i]; j++)
            {
                vec currPsi = zeros<vec>(numActions*numObservations);
                
                for(int k=0; k < numActions; k++)
                {
                    for(auto it = cachedObservations[i][j].begin(); it != cachedObservations[i][j].end(); it++)
                    {
                        currPsi(k*numObservations + *it) = Z[i][j][k][*it];
                    }
                }
                
                psi = psi + currPsi;
                F = F + psi * currPsi.t();
                g = g + psi * rewards[i][j];
                averageR(j) = averageR(j) + rewards[i][j];
                phi.insert_cols(j, psi);
            }
            
            averagePhi = averagePhi + phi;
            averageG = averageG + g;
            averageF = averageF + F;
        }
        
        averagePhi = averagePhi / numTrajectories;
        averageG = averageG / numTrajectories;
        averageF = averageF / numTrajectories;
        averageR = averageR / numTrajectories;
        
        mat Fi = averageF.i();
        mat T = (numTrajectories*averageF - averagePhi*averagePhi.t());
        mat Q = (eye<mat>(maxSeenEpisodeLength, maxSeenEpisodeLength) + averagePhi.t()*T.i()*averagePhi)/numTrajectories;
        vec b = Q * (averageR - averagePhi.t()*Fi*averageG);
        vec naturalG = Fi * (averageG - averagePhi*b);
        
        for(int i=0; i < numActions; i++)
        {
            for(int j=0; j < numObservations; j++)
            {
                weights[i][j] += learningRate * naturalG(i*numObservations + j);
            }
        }
        
        isActionProbabilitiesCacheClean = false;
    }

    void performGradientStep()
    {
        vec averagePhi = zeros<vec>(numActions*numObservations);
        vec averageG = zeros<vec>(numActions*numObservations);
        mat averageF = zeros<mat>(numActions*numObservations, numActions*numObservations);
        double averageR = 0;
        
        for(int i=0; i < numTrajectories; i++)
        {
            vec phi = zeros<vec>(numActions*numObservations);
            double r = 0;
            
            for(int j=0; j < episodeLengths[i]; j++)
            {
                for(int k=0; k < numActions; k++)
                {
                    for(auto it = cachedObservations[i][j].begin(); it != cachedObservations[i][j].end(); it++)
                    {
                        phi(k*numObservations + *it) += Z[i][j][k][*it];
                    }
                }
                
                r += rewards[i][j];
            }
            
            mat F = phi * phi.t();
            vec g = r * phi;
            averagePhi += phi;
            averageF += F;
            averageG += g;
            averageR += r;
        }
        
        averagePhi /= numTrajectories;
        averageG /= numTrajectories;
        averageF /= numTrajectories;
        averageR /= numTrajectories;
        
        mat Fi = inv(averageF + eye(numActions*numObservations, numActions*numObservations)*regularizationConstant/(det(averageF)+1));
        //cout<<Fi<<"\n";
        mat T = (numTrajectories*averageF - averagePhi*averagePhi.t());
        T = averagePhi.t()*(T.i())*averagePhi;
        //cout<<T<<"\n";
        double Q = (1 + trace(T))/numTrajectories;
        T = averagePhi.t()*Fi*averageG;
        double b = Q * (averageR - trace(T));
        vec naturalG = Fi * (averageG - averagePhi*b);
        //cout<<naturalG<<"\n";
        
        for(int i=0; i < numActions; i++)
        {
            for(int j=0; j < numObservations; j++)
            {
                weights[i][j] += learningRate * naturalG(i*numObservations + j);
            }
        }
        
        isActionProbabilitiesCacheClean = false;
    }
    
    vector<double> getActionProbabilities(unordered_set<int>& observations)
    {
        if(isActionProbabilitiesCacheClean)
        {
            return cachedActionProbabilities;
        }

        vector<double> actionProbabilities(numActions);
        double sumActionProbabilities = 0;

        for(int i=0 ; i < numActions; i++)
        {
            double sum = 0;

            for(auto it = observations.begin(); it != observations.end(); it++)
            {
                assert(*it < numObservations);

                sum += weights[i][*it];
            }

            actionProbabilities[i] = exp(sum/temperature);
            sumActionProbabilities += actionProbabilities[i];
        }

        for(int i=0 ; i < numActions; i++)
        {
            actionProbabilities[i] /= sumActionProbabilities;
        }

        cachedActionProbabilities = actionProbabilities;
        isActionProbabilitiesCacheClean = true;

        return actionProbabilities;
    }

    int getAction(unordered_set<int>& observations)
    {
        isActionProbabilitiesCacheClean = false;
        vector<double> actionProbabilities = getActionProbabilities(observations);
        int action = sample(actionProbabilities);
        return action;
    }

    void processEpisode()
    {
        isActionProbabilitiesCacheClean = false;
        
        if(currTime > maxSeenEpisodeLength)
        {
            maxSeenEpisodeLength = currTime;
        }
        
        numEpisodes++;
        episodeLengths[currTrajectory] = currTime;
        currTrajectory++;
        currTime = 0;
        
        if(currTrajectory == numTrajectories)
        {
            if(timeVariant)
            {
                performTimeVariantGradientStep();
            }
            else
            {
                performGradientStep();
            }
            
            resetTrace();
            maxSeenEpisodeLength = 0;
            currTrajectory = 0;
        }
    }
};



#include <iostream>
#include <vector>
#include <cstdlib>
#include <cassert>
#include <ctime>

#include "Asterix.h"

using namespace std;

int main(int argc, char** argv)
{
    Asterix game(true, 2);
    double totalReward = 0;
    game.reset();
    srand(time(0));
    
    while(game.isActive())
    {
        vector<vector<int> > screen = game.getScreen();
        
        for(int i=0; i < screen.size(); i++)
        {
            for(int j=0; j < screen[i].size(); j++)
            {
                cout<<screen[i][j];
            }
            cout<<"\n";
        }
        
        int action;
        cin>>action;
        totalReward += game.processAction(action);
    }
    
    cout<<"Total Reward: "<<totalReward<<"\n";
    return 0;
}
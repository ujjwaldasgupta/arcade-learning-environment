#ifndef __FEATURES_INCLUDED__
#define __FEATURES_INCLUDED__

#include <unordered_set>

using namespace std;

class Features
{
public:
    virtual unordered_set<int> getFeatures(ALEScreen& screen)
    {
        unordered_set<int> features;
        return features;
    }
    
    virtual int numFeatures()
    {
        return 0;
    }
};

#endif
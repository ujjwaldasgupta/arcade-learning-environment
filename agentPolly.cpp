#include <iostream>
#include <vector>
#include <unordered_set>
#include <ale_interface.hpp>
#include <environment/ale_screen.hpp>

#include "policy_tree.hpp"

#include "relative_basic_features.hpp"
#include "extended_basic_features2.hpp"

int main(int argc, char** argv)
{
	if(argc < 19)
	{
		std::cerr<<"Usage: "<<argv[0]<<" rom_file display runs timeStepsPerAction sizeHistory minActionProbability learningRate maxLeafNodes gradientAverageWindow optimizationSteps convergenceThreshold maxEpisodeLength cosineThreshold useGPOMDP useTotalReward useBaseLine useSoftMax useMultiSplit minX maxX minY maxY agentPixel"<<'\n';
		return 1;
	}

	int display = atoi(argv[2]);
	int runs = atoi(argv[3]);
	int timeStepsPerAction = atoi(argv[4]);
	int sizeHistory = (bool)atoi(argv[5]);
	double minActionProbability = atof(argv[6]);
	double learningRate = atof(argv[7]);
	int maxLeafNodes = atoi(argv[8]);
	int gradientAverageWindow = atoi(argv[9]);
	int optimizationSteps = atoi(argv[10]);
	unsigned int maxEpisodeLength = atoi(argv[11]);
	double cosineThreshold = atof(argv[12]);
	bool useGPOMDP = atoi(argv[13]) ? true : false;
	bool useTotalReward = atoi(argv[14]) ? true : false;
	bool useBaseLine = atoi(argv[15]) ? true : false;
	bool useSoftMax = atoi(argv[16]) ? true : false;
	bool useMultiSplit = atoi(argv[17]) ? true : false;
	int relative = atoi(argv[18]);
	const int numColumns = 16;
	const int numRows = 21;
	const int numColors = 32;
	Features *features;
	unsigned int featureSize;
	
	if(relative == 2)
	{
		if(argc < 24)
		{
			std::cerr<<"Usage: "<<argv[0]<<" rom_file display runs timeStepsPerAction sizeHistory minActionProbability learningRate maxLeafNodes gradientAverageWindow optimizationSteps convergenceThreshold maxEpisodeLength cosineThreshold useGPOMDP useTotalReward useBaseLine useSoftMax useMultiSplit minX maxX minY maxY agentPixel"<<'\n';
			return 1;
		}
		int minX = atoi(argv[19]);
		int maxX = atoi(argv[20]);
		int minY = atoi(argv[21]);
		int maxY = atoi(argv[22]);
		int agentPixel = atoi(argv[23]);
		
		features = new RelativeBASICFeatures(numColors, numRows, numColumns, agentPixel, minX, maxX, minY, maxY);
		featureSize = numColors * (2*numRows - 1) * (2*numColumns - 1);
	}
	else if(relative == 1)
	{
		features = new ExtendedBASICFeatures2(numColors, numRows, numColumns, 3);
		featureSize = features->numFeatures();
	}
	else
	{
		features = new BASICFeatures(numColors, numRows, numColumns);
		featureSize = features->numFeatures();
	}

	ALEInterface ale(display);
	ale.loadROM(argv[1]);
	//ActionVect valid_actions = ale.getLegalActionSet();
	ActionVect valid_actions = ale.getMinimalActionSet();
	ALEScreen screen = ale.getScreen();
	
	PolicyTree policyTree(sizeHistory, featureSize, valid_actions.size(), minActionProbability, learningRate, maxLeafNodes, gradientAverageWindow, optimizationSteps, maxEpisodeLength, cosineThreshold, useGPOMDP, useTotalReward, useBaseLine, useSoftMax, useMultiSplit);
	
	double sumReward = 0;
	
	for(int episode=0; episode < runs; episode++)
	{
		double totalReward = 0;
		int episodeLength = 0;

		while(!ale.game_over() && episodeLength < maxEpisodeLength)
		{
			screen = ale.getScreen();
			std::unordered_set<unsigned int> observations = features->getFeatures(screen);
// 			std::cout<<"Monsters found at ";
// 			BOOST_FOREACH(unsigned int observation, observations)
// 			{
// 				if(observation % 32 == 14)
// 				{
// 					int xBlock = observation / (numRows*numColors);
// 					int yBlock = (observation % (numRows*numColors)) / 32;
// 					std::cout<<"("<<xBlock<<", "<<yBlock<<") ";
// 				}
// 			}
// 			std::cout<<"\n";
			unsigned int action = policyTree.storeObservationsAndGetAction(observations);
			double reward = 0;
			for(unsigned int i=0; i < timeStepsPerAction; i++)
			{
				reward += ale.act(valid_actions[action]);
			}
			policyTree.processAction(action, reward);
			totalReward += reward;
			episodeLength++;
		}
		
		std::cout<<"Episode "<<episode<<" ended with score: "<< totalReward <<'\n';
		policyTree.processEpisode();
		sumReward += totalReward;
		ale.reset_game();
	}

	delete features;
	std::cout << "Average score: " << sumReward/runs << '\n';
}
#ifndef __BASIC_FEATURES_INCLUDED__
#define __BASIC_FEATURES_INCLUDED__

#include <cassert>
#include "features.hpp"

using namespace std;

class BASICFeatures : public Features
{
    public:
    
    /* There will be numRows-1 rows above and below the agent, and similiarly numColumns-1 columns above and below the agent. Effectively, the total number of rows is 2*numRows-1,
     * while the total number of columns is 2*numColumns-1.
     */
    
    int numColors;
    int numRows;
    int numColumns;
    int colorsAssigned;
    vector<int> colorMap;
    int agentPixel;
    int minX;
    int maxX;
    int minY;
    int maxY;
    
    BASICFeatures(int numColors, int numRows, int numColumns) : numColors(numColors), numRows(numRows), numColumns(numColumns), colorsAssigned(0), colorMap(128, -1) 
    {
        assert(numColors > 0);
        assert(numRows > 0);
        assert(numColumns > 0);
    }
    
    int numFeatures()
    {
        return numColors*numRows*numColumns;
    }
    
    unordered_set<int> getFeatures(ALEScreen& screen)
    {
        int screenWidth = screen.width();
        int screenHeight = screen.height();
        int blockWidth = screenWidth / numColumns;
        int blockHeight = screenHeight / numRows;
        int featuresPerBlock = numColors;
        unordered_set<int> features;
        // For each pixel block
        for (int bx = 0; bx < numColumns; bx++) 
        {
            for (int by = 0; by < numRows; by++) 
            {
                vector<bool> hasColor(numColors, false);
                int xo = bx * blockWidth;
                int yo = by * blockHeight;
                // Determine which colors are present
                for (int x = xo; x < xo + blockWidth; x++)
                {
                    for (int y = yo; y < yo + blockHeight; y++)
                    {
                        unsigned char pixel = screen.get(y,x);
                        if(colorMap[pixel >> 1] == -1)
                        {
                            if(colorsAssigned < numColors)
                            {
                                colorMap[pixel >> 1] = colorsAssigned;
                                hasColor[colorsAssigned] = true;
                                colorsAssigned++;
                            }
                        }
                        else hasColor[colorMap[pixel >> 1]] = true;
                    }
                }
                // Add all colors present to our feature set
                for (int c = 0; c < numColors; c++)
                {
                    int blockIndex = (bx*numRows + by)*numColors;
                    if (hasColor[c])
                    {
                        assert(c + blockIndex >= 0);
                        features.insert(c + blockIndex);
                    }
                }
            }
        }
        return features;
    }
};

#endif


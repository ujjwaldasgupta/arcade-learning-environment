#ifndef __EXTENDED_BASIC_FEATURES_INCLUDED__
#define __EXTENDED_BASIC_FEATURES_INCLUDED__

#include <cmath>
#include <cassert>
#include "basic_features.hpp"

using namespace std;

class ExtendedBASICFeatures : public BASICFeatures
{
    public:
    int proximityLimit;
    
    ExtendedBASICFeatures(int numColors, int numRows, int numColumns) : BASICFeatures(numColors, numRows, numColumns), proximityLimit(proximityLimit)
    {
    }
    
    int numFeatures()
    {
        return numColors*numColumns*numRows + 4*(log2(numRows)+1)*(log2(numColumns)+1)*numColors*(numColors+1)/2;
    }
    
    unordered_set<int> getFeatures(ALEScreen& screen)
    {
        unordered_set<int> originalFeatures = BASICFeatures::getFeatures(screen);
        unordered_set<int> features = originalFeatures;
        int numOriginalFeatures = numColors*numRows*numColumns;
        //cout<<"Original Feature Size: "<<originalFeatures.size()<<"\n";
        
        for(auto it1 = originalFeatures.begin(); it1 != originalFeatures.end(); it1++)
        {
            auto it2 = it1;
            for(; it2 != originalFeatures.end(); it2++)
            {
                int color1 = (*it1)%numColors;
                int color2 = (*it2)%numColors;
                int minColor = min(color1, color2);
                int maxColor = max(color1, color2);
                int y1 = ((*it1)%(numColors*numRows))/numColors;
                int y2 = ((*it2)%(numColors*numRows))/numColors;
                int x1 = (*it1)/(numColors*numRows);
                int x2 = (*it2)/(numColors*numRows);
                int quadrant = (y1<y2)*2+(x1<x2);
                int y = abs(y1-y2);
                int x = abs(x1-x2);                
                int logY = 0;
                int logX = 0;
                
                if(y > 0)
                {
                    logY = 1+log2(y);
                }
                
                if(x > 0)
                {
                    logX = 1+log2(x);
                }
                
                int index = numOriginalFeatures + quadrant*(log2(numRows)+1)*(log2(numColumns)+1)*numColors*(numColors+1)/2 + logY*(log2(numColumns)+1)*numColors*(numColors+1)/2 + logX*numColors*(numColors+1)/2 + minColor*numColors - ((minColor-1)*minColor)/2 + maxColor - minColor;
                
                if(features.count(index) == 0)
                {
                    assert(index >= numOriginalFeatures);
                    assert(index < numFeatures());
                    features.insert(index);
                }

            }
        }
        
        
        
        return features;
    }
};

#endif
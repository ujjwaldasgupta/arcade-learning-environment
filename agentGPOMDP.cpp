#include <iostream>
#include <vector>
#include <ale_interface.hpp>
#include <environment/ale_screen.hpp>

#include "policy_gradient_gpomdp.hpp"
#include "extended_basic_features2.hpp"

const int numBitsPerByte = 8;

std::vector<unsigned int> getBASICFeaturesFromScreen(ALEScreen &screen)
{
	int numColumns = 16;
	int numRows = 21;
	int numColors = 128;
	int screenWidth = screen.width();
	int screenHeight = screen.height();
	int blockWidth = screenWidth / numColumns;
	int blockHeight = screenHeight / numRows;
	int featuresPerBlock = numColors;
	std::vector<unsigned int> features;
	int agentLocation = 0;
	int agentBlock = 0;
	// Search for agent in Space Invaders
	for(int y = 180; y < 200; y++)
	{
		for(int x = screenWidth-1; x >= 0; x--)
		{
			unsigned char pixel = screen.get(y,x);
			if(pixel == 196)
			{
				agentLocation = x;
				goto done;
			}
		}
	}
	done: agentBlock = agentLocation / blockWidth;
	int blockIndex = (numColumns-agentBlock-1)*numRows*featuresPerBlock;
	// For each pixel block
	for (int bx = 0; bx < numColumns; bx++) 
	{
		for (int by = 0; by < numRows; by++) 
		{
			vector<bool> hasColor(numColors, false);
			int xo = bx * blockWidth;
			int yo = by * blockHeight;
			// Determine which colors are present
			for (int x = xo; x < xo + blockWidth; x++)
			{
				for (int y = yo; y < yo + blockHeight; y++)
				{
					unsigned char pixel = screen.get(y,x);
					hasColor[(pixel) >> 1] = true;
				}
			}
			// Add all colors present to our feature set
			for (int c = 0; c < numColors; c++)
			{
				if (hasColor[c])
				{
					assert(c + blockIndex >= 0);
					features.push_back(c + blockIndex);
				}
			}
			// Increment the feature offset in the big feature vector
			blockIndex += featuresPerBlock;
		}
	}
	return features;
}

int main(int argc, char** argv)
{
	if(argc < 8)
	{
		std::cerr<<"Usage: "<<argv[0]<<" rom_file display runs timeStepsPerAction expand learningRate temperature"<<'\n';
		return 1;
	}

	int display = atoi(argv[2]);
	int runs = atoi(argv[3]);
	int timeStepsPerAction = atoi(argv[4]);
	bool expand = (bool)atoi(argv[5]);
	double learningRate = atof(argv[6]);
	double temperature = atof(argv[7]);
	int numColors = 32;
	int numColumns = 16;
	int numRows = 21;
	int proximityLimit = 5;
	ExtendedBASICFeatures2 features(numColors, numRows, numColumns, proximityLimit);	
	ALEInterface ale(display);
	ale.loadROM(argv[1]);
	//ActionVect valid_actions = ale.getLegalActionSet();
	ActionVect valid_actions = ale.getMinimalActionSet();
	ALEScreen screen = ale.getScreen();
	
	unsigned int featureSize = features.numFeatures();
	
	PolicyGradientGPOMDP policy(1, featureSize, valid_actions.size(), learningRate, temperature);
	double sumReward = 0;
	
	for(int episode=0; episode < runs; episode++)
	{
		double totalReward = 0;

		while(!ale.game_over())
		{	
			screen = ale.getScreen();
			std::unordered_set<unsigned int> observations =  features.getFeatures(screen);
			unsigned int action = policy.storeObservationsAndGetAction(observations);
			double reward = 0;
			for(unsigned int i=0; i < timeStepsPerAction; i++)
			{
				reward += ale.act(valid_actions[action]);
			}
			policy.updateStoredGradient(action, reward);
			totalReward += reward;
		}

		policy.processEpisode();
		std::cout<<"Episode "<<episode<<" ended with score: "<< totalReward <<'\n';
		sumReward += totalReward;
		ale.reset_game();
	}

	std::cout << "Average score: " << sumReward/runs << '\n';
}